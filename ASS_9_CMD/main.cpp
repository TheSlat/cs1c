/**
 * @brief Assignment #9 - Command Line Parameters
 *
 * This program accepts a first and last name as command line parameters,
 * then prints them to the standard output stream.
 *
 * @author Ethan Slattery
 * @date   19OCT2015
 * @note   CS1C - M/W 1730-2050
 ***********************************************************************/
#include<iostream>
using std::endl;
using std::cout;

int main(int argc, char *argv[])
{
	// OUT - usage suggestions if arguments are not correct
	if((argc < 3) | (argc > 3))
	{
		std::cout << "\nnames: wrong number of arguments\n";
		std::cout << "usage: names <first name> <last name>\n";
	}
	// OUT - output the names and letters required
	else
	{
		cout << "\n\nHello " << argv[1] << " " << argv[2] << "!\n";
		cout << "The second letter of your first name is: "
		     << *(argv[1]+1) << endl;
		cout << "The second letter of your last name is: "
			 << *(argv[2]+1) << endl;
	}
	return 0;
}
