/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "header.h"
/*************************************************************************
 * FUNCTION SaveNumbersToFile
 *________________________________________________________________________
 * 	This function save numbers that are in the array of number objects to
 * 	a file for later use.
 * 	- returns the array of ten number objects, by reference
 *________________________________________________________________________
 * PRE-CONDITIONS
 *  The number class must be defined
 *	filename		: name of the output file
 *	NumberArray		: and array of number objects
 *	AR_SIZE		 	: The size of the number array
 *
 * POST-CONDITIONS
 *	returns the array of numbers by reference
 *************************************************************************/
void SaveNumbersToFile(string filename,		// IN - The output file
					   Number numberAR[],	// IN & OUT - the array of numbers
					   const int AR_SIZE)	// IN - The size of the array
{
	ofstream oFile;
	oFile.open(filename.c_str());
	for(int count = 0; count < AR_SIZE; count++)
	{
		oFile << numberAR[count].GetNumber() << endl;
	}
	oFile.close();
}
