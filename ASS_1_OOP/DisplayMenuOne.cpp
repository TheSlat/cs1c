/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "header.h"
/*************************************************************************
 * FUNCTION DisplayMenuOne
 *________________________________________________________________________
 * 	This function displays the initial menu for the program, and validates the
 * 	user input to make sure a valid menu choice is entered. The option to load
 * 	numbers from a file will only exist if the save file is present.
 * 	- returns a valid MenuOptions to the calling function
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	enum MenuOptions must be defined
 *
 * POST-CONDITIONS
 *	displays menu on the screen and returns MenuOptions to calling function
 *************************************************************************/
MenuOptions DisplayMenuOne(string filename)	// IN - The name of the save file
{
	stringstream   prompt;	//CALC - The menu prompt
	MenuOptions    choice;	//IN   - The users menu choice

	cout << "********************************************************\n"
		 << "*     WELCOME TO THE SUPER SPECIAL NUMBER MACHINE!     *\n"
		 << "********************************************************\n";

	prompt     << "1 - Generate Random Numbers\n";

	//OUT - Only show menu option if save file exists
	if(FileExists(filename))
	{
		prompt << "2 - Load Numbers From File\n";
	}

	prompt 	   << "0 - to Exit\n"
		       << "Enter a command: ";

	// IN - Get and validate the user input as an integer and cast to menu enum
	choice = MenuOptions(GetInput(prompt.str(), EXIT, LOAD));

	return choice;
}
