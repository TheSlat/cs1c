/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "header.h"
/************************************************************************
 * FUNCTION InsertionSort
 * ----------------------------------------------------------------------
 * This function sorts an array of Number objects
 * - returns nothing
 * ----------------------------------------------------------------------
 * PRE-CONDITIONS:
 * 			intAr : array of numbers
 * POST-CONDITIONS:
 * 			sorts numbers in array
 ************************************************************************/
void InsertionSort(Number intAr[], 		// IN & OUT - The array to sort
				   const int AR_SIZE)	// IN - The size of the array
{

	integer checkCount;
	integer j;
	Number temp;

	for(checkCount = 1; checkCount < AR_SIZE; checkCount++)
	{
		temp =  intAr[checkCount];
		j = checkCount;

		while( j > 0 && intAr[j - 1].GetNumber() > temp.GetNumber())
		{

			intAr[j] = intAr[j-1];
			j--;
		}
		intAr[j] = temp;
	}
}
