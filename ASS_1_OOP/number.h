/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#ifndef NUMBER_H_
#define NUMBER_H_
#include <iostream>		//for cin/cout
#include <iomanip>		//for IO manipulators
#include <string>		//strings!
#include <sstream>		//string stream
#include <stdlib.h>     // srand, rand
#include <time.h>       // time
using namespace std;

//Typedef for this class
typedef unsigned short integer;

class Number
{
	public:
		/*******************************
		 ** CONSTRUCTOR & DESTRUCTORS **
		 *******************************/
		Number();
		~Number();

		/**************
		 ** MUTATORS **
		 **************/
		void    Set(integer number);
		void    SetRandom();

		/***************
		 ** ACCESSORS **
		 ***************/
		string  GetPaddedNumber() const;
		integer GetNumber() const;
		integer SumIntDigits() const;
		integer InvertDigits() const;
		integer Tripple() const;

	private:
		void GetDigits(integer &hundreds,
					   integer &tens,
					   integer &ones) const;


	private:
		integer randNum;
};

/***********************************************************************
 * Number Class
 *   This class represents a generic Number object
 *   It manages 1 attribute: The number as an integer (typedef unsigned short)
 ***********************************************************************/

/*******************************
 ** CONSTRUCTOR & DESTRUCTORS **
 *******************************/

/***********************************************************************
 ** Number();
 **   Constructor; Initializes class attributes
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** ~Number();
 **   Destructor; Does not perform any specific function
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** MUTATORS  **
 ***************/

/***********************************************************************
 ** void Set(integer number);
 **
 **   Mutator; This method will set the Number value to a set value
 **---------------------------------------------------------------------
 **   Parameter: the number to assign as an integer (unsigned short)
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** void SetRandom();
 **
 **   Mutator; This method will set the Numbers using a random 3 digit number
 **---------------------------------------------------------------------
 **   Parameter: None
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** ACCESSORS **
 ***************/

/***********************************************************************
 ** string GetPaddedNumber() const;
 **
 **   Accessor; This method returns the number padded with zeros if necessary
 **---------------------------------------------------------------------
 **   Parameter: None
 **---------------------------------------------------------------------
 **   Returns: returns the padded number as a string
 ***********************************************************************/

/***********************************************************************
 ** integer GetNumber() const;
 **
 **   Accessor; This method returns the integer
 **---------------------------------------------------------------------
 **   Parameter: None
 **---------------------------------------------------------------------
 **   Returns: returns the integer as an integer (unsigned short)
 ***********************************************************************/

/***********************************************************************
 ** integer SumIntDigits() const;
 **
 **   Accessor; This method sums the individual digits of the number
 **---------------------------------------------------------------------
 **   Parameter: None
 **---------------------------------------------------------------------
 **   Returns: The sum of the three digits as an integer (unsigned short)
 ***********************************************************************/

/***********************************************************************
 ** integer InvertDigits() const;
 **
 **   Accessor; This method reverses the order of the three digits in
 **   		    the number
 **---------------------------------------------------------------------
 **   Parameter: None
 **---------------------------------------------------------------------
 **   Returns: the inverted digits as an integer number
 ***********************************************************************/

/***********************************************************************
 ** integer Triple() const;
 **
 **   Accessor; This method triples the value of the integer (multiply by 3)
 **---------------------------------------------------------------------
 **   Parameter: None
 **---------------------------------------------------------------------
 **   Returns: The triple value of the integer as an integer
 ***********************************************************************/

#endif	//NUMBER_H_
