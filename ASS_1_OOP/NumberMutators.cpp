/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "number.h"

/**************
 ** MUTATORS **
 **************/

/*************************************************************************
 * Method Set: Class number
 *------------------------------------------------------------------------
 *   This method sets the number to the supplied number. The number should be
 *   3 digits to avoid errors so if the number is not (not between 100 and 999)
 *   then the number is set to the closest valid number.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the sum as an integer
 *************************************************************************/
void Number::Set(integer number)
{
	if(number > 999)
	{
		randNum = 999;
	}
	else if(number < 100)
	{
		randNum = 100;
	}
	else
	{
		randNum = number;
	}
}

/*************************************************************************
 * Method SetRandom: Class number
 *------------------------------------------------------------------------
 *   Sets the number to a random value between 100 and 999 (three digits)
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the sum as an integer
 *************************************************************************/
void Number::SetRandom()
{
	randNum = rand() % 899 + 100;
}
