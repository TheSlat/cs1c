/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "header.h"
/***********************************************************************
 * ASSIGNMENT #1 - This program generates a list of ten random numbers, or if
 * 	previous numbers were saved they can be loaded. The user can then choose to
 * 	generate new numbers, save the current numbers, re-load the saved numbers,
 * 	or do one of three operations on a number in the list. Those operations are
 * 	calculating the sum of the individual digits, reverse the order of the
 * 	digits, or triple the number as a whole.
 * _____________________________________________________________________
 * INPUTS:
 * 		userChoice		: The user menu choice for program options
 * 		numberChoice	: The user options for picking a number from the list
 * 		SAVE_FILE		: Save file, save or load one set of numbers to a file
 *
 * OUTPUTS:
 * 		SAVE_FILE		: Save file, save or load one set of numbers to a file
 ***********************************************************************/
int main()
{
/************************************************************************
 * CONSTANTS
 * -----------------------------------------------------------------
 * USED FOR CLASS HEADING - ALL WILL BE OUTPUT
 * -----------------------------------------------------------------
 * ASS_TYPE		: assignment type (lab or assignment) 'L' or 'A'
 * ASS_NUM		: Assignment Number
 * ASS_NAME		: Title of the Assignment
 * PROG_NAME	: The name of the programmers
 * AR_SIZE		: The size of the number list
 * SAVE_FILE	: The name of the save file, one set of numbers saved at a time
 *
 ************************************************************************/
	const string ASS_NAME	= "Random Numbers";
	const char   ASS_TYPE	= 'A';
	const string ASS_NUM	= "1";
	const string PRO_NAME   = "Ethan Slattery";
	const int    AR_SIZE    = 10;
	const string SAVE_FILE = "test.txt";

	//VARIABLE DECLARATIONS
	Number      randNums[AR_SIZE];	// PROC - The array of numbers for the prog
	MenuOptions userChoice;			// IN - The users menu choice
	integer		numberChoice;		// IN - The users number choice

	//OUTPUT - Class Heading
	PrintHeaderOstream(cout, ASS_NAME, ASS_TYPE, ASS_NUM, PRO_NAME);

	//seed for the random numbers to generate
	srand(time(NULL));

	/***********************************************************************
	 * INPUT: The initial menu gives options for establishing the list of
	 * 		  numbers. They can be generated, and if a saved file exists
	 * 		  then the option to load those saved values will be shown.
	 ***********************************************************************/
	userChoice = DisplayMenuOne(SAVE_FILE);
	if(userChoice != EXIT)
	{
		//PROC - Generate a new set of random numbers to fill the array
		if(userChoice == GENERATE)
		{
			for(int count = 0; count < AR_SIZE; count++)
			{
				randNums[count].SetRandom();
			}
		}
		//PROC - If the save file exists, option is shown and will load numbers
		else
		{
			GetNumbersFromFile(SAVE_FILE, randNums, AR_SIZE);
		}
		//PROC - Sort the array. Saved numbers SHOULD be sorted but.... users
		InsertionSort(randNums, AR_SIZE);
	}
	/***********************************************************************
	 * INPUT: Once numbers are generated or loaded the numbers are shown
	 * 		  and the main menu is displayed with all options. The load
	 * 		  numbers option will only be shown if the save file exists.
	 ***********************************************************************/
	while(userChoice != EXIT)
	{
		cout << endl
			 <<	"*********************************************\n"
			 << "*     THE SUPER SPECIAL NUMBER MACHINE!     *\n"
			 << "*********************************************\n";
		cout << OutputIntArray(randNums, AR_SIZE) << endl;
		userChoice = DisplayMenuTwo(SAVE_FILE);

		switch(userChoice)
		{
		case GENERATE	: for(int count = 0; count < AR_SIZE; count++)
						  {
							  randNums[count].SetRandom();
						  }
						  InsertionSort(randNums, AR_SIZE);
						  break;
		case LOAD		: GetNumbersFromFile(SAVE_FILE, randNums, AR_SIZE);
						  break;
		case SAVE		: SaveNumbersToFile(SAVE_FILE, randNums, AR_SIZE);
						  break;
		case SUM		: numberChoice = GetInput(OutputIntMenu(randNums,
												  AR_SIZE), 1, 10);
						  cout << "SUM OF DIGITS: "
							   << randNums[numberChoice-1].SumIntDigits()
							   << endl;
						  break;
		case TRIPLE		: numberChoice = GetInput(OutputIntMenu(randNums,
												  AR_SIZE), 1, 10);
						  cout << "TRIPLE OF NUMBER: "
							   << randNums[numberChoice-1].Tripple()
							   << endl;
						  break;
		case REVERSE	: numberChoice = GetInput(OutputIntMenu(randNums,
												  AR_SIZE), 1, 10);
						  cout << "REVERSE OF NUMBER: "
							   << randNums[numberChoice-1].InvertDigits()
							   << endl;
						  break;
		case EXIT		: break;
		default			: break;
		};
	}
	return 0;
}
