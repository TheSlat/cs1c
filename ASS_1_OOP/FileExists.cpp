/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "header.h"
/*************************************************************************
 * FUNCTION FileExists
 *________________________________________________________________________
 * 	This function performs a simple test to see if the file name provided
 * 	exists in the working directory
 * 	- returns TRUE if the file exists, FALSE if it doesn't, or is inaccessible
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	filename		: name of the file
 *	defaultFile		: A name of a default file to use if user chooses
 *
 * POST-CONDITIONS
 *	returns filename as a string to the calling function
 *************************************************************************/
bool FileExists(string name)	// IN - The name of the file
{
	bool exists;
	ifstream inputfile;
	exists = false;

	// PROC - test the file to see if it exists by attempting to open
	inputfile.open(name.c_str());
	if(inputfile)
	{
		exists = true;
	}
    return exists;
}
