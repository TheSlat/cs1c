/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "number.h"

/***************
 ** ACCESSORS **
 ***************/

/*************************************************************************
 * Method OutputNum: Class number
 *------------------------------------------------------------------------
 *   Outputs the number, padding the front with zeros if less than 3 digits
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the output as a string
 *************************************************************************/
string Number::GetPaddedNumber() const
{
	ostringstream output;

	if(randNum < 100)
	{
		if(randNum < 10)
		{
			output << 0;
		}
		output << 0;
	}
	output << randNum;

	return output.str();
}

/*************************************************************************
 * Method OutputNum: Class number
 *------------------------------------------------------------------------
 *   Outputs the number, padding the front with zeros if less than 3 digits
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the output as a string
 *************************************************************************/
integer Number::GetNumber() const
{
	return randNum;
}

/*************************************************************************
 * Method SumintegerDigits: Class number
 *------------------------------------------------------------------------
 *   Takes the number and sums the digits, returning the sum as an integer
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the sum as an integer
 *************************************************************************/
integer Number::SumIntDigits() const
{
	integer hundredsPlace;
	integer tensPlace;
	integer onesPlace;

	GetDigits(hundredsPlace, tensPlace, onesPlace);

	return (hundredsPlace + tensPlace + onesPlace);
}

/*************************************************************************
 * Method InvertDigits: Class number
 *------------------------------------------------------------------------
 *   Takes the number and reverses the digits, returning it as an integer
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the inverted number as an integer
 *************************************************************************/
integer Number::InvertDigits() const
{
	integer hundredsPlace;
	integer tensPlace;
	integer onesPlace;

	GetDigits(hundredsPlace, tensPlace, onesPlace);

	tensPlace *= 10;
	onesPlace *= 100;

	return (hundredsPlace + tensPlace + onesPlace);
}

/*************************************************************************
 * Method Triple: Class number
 *------------------------------------------------------------------------
 *   Takes the number and triples the number, returning it as an integer
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the tripled number as an integer
 *************************************************************************/
integer Number::Tripple() const
{
	return randNum * 3;
}

/*************************************************************************
 * Method GetDigits: Class number
 *------------------------------------------------------------------------
 *   Takes the number and sums the digits, returning the sum as an integer
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    returns the sum as an integer
 *************************************************************************/
void Number::GetDigits(integer &hundreds,		//OUT - the hundreds place
					   integer &tens,			//OUT - the tens place
					   integer &ones) const		//OUT - the ones place
{
	ones     = (randNum % 10);
	tens     = (((randNum % 100) - ones) / 10);
	hundreds = ((randNum - (randNum % 100)) / 100);
}
