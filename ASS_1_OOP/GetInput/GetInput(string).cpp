/***********************************************************************
 * AUTHOR		: BEAK & PaperCaked
 * LAB #16		: Inheritance, Overloading, Redefining
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 21APR2015
 ***********************************************************************/
#include "GetInput.h"
/*************************************************************************
 * FUNCTION GetInput: String
 *________________________________________________________________________
 * 	This function receives a prompt and a width for the prompt. If the width
 * 	is greater than 0 it will use setw() to format the prompt. The user will
 * 	then enter a string input and that string wil be returned to the calling
 * 	function.
 * 	- returns the user input as a string
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 	prompt			: The input prompt as a string
 * 	promptWidth		: The width of the input prompt, used if greater than 0
 *
 * POST-CONDITIONS
 * 	returns the user input as a string
 *************************************************************************/
string GetInput(string prompt,		//IN - prompt
		   	    int promptWidth) 	//IN - Width of prompt column if > 0
{
	string strIn;			//IN   - User input

	cout << left;
	//PROC - If a prompt width > 0 was passed in, it sets the prompt width
	if(promptWidth > 0)
	{
		cout << setw(promptWidth);
	}
	cout << prompt;
	cout << right;

	//IN - Get the user input as a string
	getline(cin, strIn);

	//OUT - return the user input to the calling function
	return strIn;
}
