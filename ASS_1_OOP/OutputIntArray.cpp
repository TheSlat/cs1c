/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "header.h"
/*************************************************************************
 * FUNCTION OutputIntArray
 *________________________________________________________________________
 * 	This function outputs the array of number objects for display,
 * 	with 5 numbers to a row. There will be an error if the number of number
 * 	objects is not divisible by 5.
 * 	- returns the array of ten number objects, by reference
 *________________________________________________________________________
 * PRE-CONDITIONS
 *  The number class must be defined
 *	filename		: name of the input file
 *	NumberArray		: and array of number objects
 *	AR_SIZE		 	: The size of the number array
 *
 * POST-CONDITIONS
 *	returns the array of numbers by reference
 *************************************************************************/
string OutputIntArray(Number numberArray[],	// IN - the number array
					  const int AR_SIZE)	// IN - the size of the array
{
	const int ROW_SIZE = 5;	// PROC - number of items per row
	int rowCount;			// PROC - loop counter for rows
	int arCount;			// PROC - loop counter
	ostringstream output;	// OUT  - The output stream

	// OUT - Outputs the array of integers as a menu
	arCount  = 0;
	rowCount = 0;
	while(arCount < AR_SIZE)
	{
		output << numberArray[arCount].GetNumber();
		arCount++;
		rowCount++;
		while(arCount < AR_SIZE && rowCount < ROW_SIZE)
		{
			output << "\t" <<  numberArray[arCount].GetNumber();
			arCount++;
			rowCount++;
		}
		rowCount = 0;
		output << endl;
	}
	return output.str();
}
