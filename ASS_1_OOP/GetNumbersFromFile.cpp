/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#include "header.h"
/*************************************************************************
 * FUNCTION GetNumbersFromFile
 *________________________________________________________________________
 * 	This function gets ten numbers from a file. Extra numbers are ignored
 * 	and new numbers generated if needed. Numbers will be error to 3 digits.
 * 	- returns the array of number objects, by reference
 *________________________________________________________________________
 * PRE-CONDITIONS
 *  The number class must be defined
 *	filename		: name of the input file
 *	NumberArray		: and array of number objects
 *	AR_SIZE		 	: The size of the number array
 *
 * POST-CONDITIONS
 *	returns the array of numbers by reference
 *************************************************************************/
void GetNumbersFromFile(string filename,	// IN - The input file
						Number numberAR[],	// IN & OUT - the array of numbers
						const int AR_SIZE)	// IN - The size of the array
{
	integer tempNum;
	ifstream iFile;

	if(FileExists(filename.c_str()))
	{
		iFile.open(filename.c_str());
		for(int count = 0; count < AR_SIZE; count++)
		{
			if(!iFile.eof())
			{
				iFile >> tempNum;

				if(tempNum < 100)
					tempNum += 100;
				else if(tempNum > 999)
					tempNum = tempNum % 899 + 100;

				numberAR[count].Set(tempNum);
			}
			else
			{
				numberAR[count].Set( rand() % 899 + 100 );
			}
		}
		iFile.close();
	}
}
