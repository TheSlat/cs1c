/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #1: Random Numbers
 * CLASS		: CS1C
 * SECTION		: MW 1930
 * DUE DATE		: 24AUG2015
 ***********************************************************************/
#ifndef header_H_
#define header_H_

//Preprocessor directives go here
#include <iostream>		//for cin/cout
#include <iomanip>		//for IO manipulators
#include <string>		//strings!
#include <sstream>		//string stream
#include <fstream>		//for file use
#include "number.h"		//number class for this assignment
#include "GetInput/GetInput.h"	//My custom input functions
using namespace std;

//Global Constants

//User Defined Types
enum MenuOptions{
	EXIT,
	GENERATE,
	LOAD,
	SAVE,
	SUM,
	TRIPLE,
	REVERSE
};

//Function Prototypes
/*************************************************************************
 * PrintHeaderString
 *  This function receives an ostream variable, assignment name, type,
 * 	number, and programmers name. The function then returns the
 * 	appropriate header via reference through the ostream variable.
 * 	- returns nothing
 *************************************************************************/
void PrintHeaderOstream(ostream &output,	// OUT - output stream
						string  assName,	// IN - Assignment Name
						char    assType,	// IN - assignment Type
			 	 	 						//     (Lab or Assignment)
						string  assNum,		// IN - assignment number
						string  progName);	// IN - Programmers Name

/*************************************************************************
 * DisplayMenuOne
 * 	This function displays the initial menu for the program, and validates the
 * 	user input to make sure a valid menu choice is entered. The option to load
 * 	numbers from a file will only exist if the save file is present.
 * 	- returns a valid MenuOptions to the calling function
 *************************************************************************/
MenuOptions DisplayMenuOne(string filename); // IN - The name of the save file

/*************************************************************************
 * DisplayMenuTwo
 *	This function displays the main menu for the program, and validates the
 * 	user input to make sure a valid menu choice is entered. The option to load
 * 	numbers from a file will only exist if the save file is present.
 * 	- returns a valid MenuOptions to the calling function
 *************************************************************************/
MenuOptions DisplayMenuTwo(string filename); // IN - The name of the save file

/*************************************************************************
 * OutputIntMenu
 * 	This function outputs the array of number objects for use in a menu,
 * 	with 5 numbers to a row. There will be an error if the number of number
 * 	objects is not divisible by 5.
 * 	- returns the array of ten number objects, by reference
 *************************************************************************/
string OutputIntMenu(Number numberArray[],	// IN - the number array
					 const int AR_SIZE);	// IN - the size of the array

/*************************************************************************
 * OutputIntArray
 * 	This function outputs the array of number objects for display,
 * 	with 5 numbers to a row. There will be an error if the number of number
 * 	objects is not divisible by 5.
 * 	- returns the array of ten number objects, by reference
 *************************************************************************/
string OutputIntArray(Number numberArray[],	// IN - the number array
					  const int AR_SIZE);		// IN - the size of the array

/*************************************************************************
 * FileExists
 * 	This function performs a simple test to see if the file name provided
 * 	exists in the working directory
 * 	- returns TRUE if the file exists, FALSE if it doesn't, or is inaccessible
 *************************************************************************/
bool FileExists(string name);	// IN - THe name of the file

/*************************************************************************
 * GetNumbersFromFile
 * 	This function gets ten numbers from a file for use in the random number
 * 	program. Any numbers beyond the bounds of the array will be ignored and if
 * 	there are not enough numbers random ones will be generated.
 * 	- returns the array of ten number objects, by reference
 *************************************************************************/
void GetNumbersFromFile(string filename,	// IN - The input file
						Number numberAR[],	// IN & OUT - the array of numbers
						const int AR_SIZE);		// IN - The size of the array

/*************************************************************************
 * SaveNumbersToFile
 * 	This function save numbers that are in the array of number objects to
 * 	a file for later use.
 * 	- returns the array of ten number objects, by reference
 *************************************************************************/
void SaveNumbersToFile(string filename,		// IN - The output file
					   Number numberAR[],	// IN & OUT - the array of numbers
				       const int AR_SIZE);	// IN - The size of the array

/************************************************************************
 * InsertionSort
 * 	This function sorts the Array
 * 	- returns nothing
 ************************************************************************/
void InsertionSort(Number intAr[], 		// IN & OUT - The array to sort
				   const int AR_SIZE);	// IN - The size of the array

#endif /* header_H_ */
