//
// Created by Ethan on 12/1/2015.
//
#include "sorting.h"

void FillArrays(int ar1[], int ar2[], int ar3[], const int AR_SIZE) {
    for(int i = 0; i < AR_SIZE; i++) {
        // CALC - load the ordered arrays with numbers
        ar1[i] = (i+1);
        ar3[i] = (i+1);
        ar2[AR_SIZE-(i+1)] = (i+1);
    }
    std::random_shuffle(ar3, ar3 +  AR_SIZE);
}

Telemetry BubbleSort(int intAr[], const int AR_SIZE) {
    int temp;
    Telemetry out;

    for(int i = 0; i < AR_SIZE - 1; i++) {
        for(int j = 0; j < AR_SIZE-1-i; j++) {
            out.Compare();
            if(intAr[j] > intAr[j+1]) {
                temp = intAr[j];
                intAr[j] = intAr[j+1];
                intAr[j+1]=temp;
                out.Assign(3);
            }
        }
    }
    out.RecordTime();
    cout << "Elapsed Time: " << out.Elapsed() << endl;
    cout << "Seconds: " << out.GetNanoSec() << endl;

    return out;
}

Telemetry InsertionSort(int intAr[], const int AR_SIZE) {
    int checkCount;
    int j;
    int temp;
    Telemetry out;

    for(checkCount = 1; checkCount < AR_SIZE; checkCount++) {
        temp =  intAr[checkCount];
        j = checkCount;

        while( j > 0 && intAr[j - 1] > temp) {
            intAr[j] = intAr[j-1];
            j--;
        }
        intAr[j] = temp;
    }
    return out;
}

Telemetry SelectionSort(int intAr[], const int AR_SIZE) {
    int larger, index;
    Telemetry out;

    for(int i = AR_SIZE-1; i >= 1; --i) {
        larger = intAr[0];
        index  = 0;
        for(int j = 1; j <= i; j++) {
            if(intAr[j] > larger) {
                larger = intAr[j];
                index  = j;
            }
        }
        intAr[index] = intAr[i];
        intAr[i] = larger;
    }
    return out;
}

string PrintAR(int *ar, int numOut, const int AR_SIZE) {
    ostringstream out;
    int colWidth = std::floor( std::log10( AR_SIZE ) ) + 2;

    for(int i = 0; i < numOut; i++) {
        out << setw(colWidth) << ar[i];
    }
    out << setw(colWidth) << "...";
    for(int i = AR_SIZE-numOut; i < AR_SIZE; i++) {
        out << setw(colWidth) << ar[i];
    }
    out << endl;

    return out.str();
}
