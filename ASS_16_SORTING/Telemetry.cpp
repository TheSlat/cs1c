/*
 * Telemetry.cpp
 *
 *  Created on: Dec 3, 2015
 *      Author: Eslattery1
 */
#include "Telemetry.h"

Telemetry::Telemetry() {
	elapsed     = 0;
	comparisons = 0;
	assignments = 0;

    if(!QueryPerformanceFrequency(&freq)) {
    	cout << "QueryPerformanceFrequency FAILED!\n";
    }

    if(!QueryPerformanceCounter(&startPoint)) {
    	cout << "QueryPerformanceCounter FAILED! " << endl;
    }
}

void Telemetry::Reset() {
	QueryPerformanceCounter(&startPoint);
}

long long Telemetry::Elapsed() const {
	LARGE_INTEGER tempElapsed;
	QueryPerformanceCounter(&tempElapsed);

	return tempElapsed.QuadPart - startPoint.QuadPart;
}

long long Telemetry::RecordTime() {
	LARGE_INTEGER tempElapsed;
	QueryPerformanceCounter(&tempElapsed);
	elapsed = tempElapsed.QuadPart - startPoint.QuadPart;

	return elapsed;
}

void Telemetry::Compare(int num) {
	comparisons += num;
}

void Telemetry::Assign(int num) {
	assignments += num;
}

unsigned long Telemetry::GetCompares() {
	return comparisons;
}

unsigned long Telemetry::GetAssigns() {
	return assignments;
}

double Telemetry::GetNanoSec() {
	return double(elapsed)/freq.QuadPart;
}
