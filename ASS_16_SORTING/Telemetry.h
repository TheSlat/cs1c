/*
 * Telemetry.h
 *
 *  Created on: Dec 3, 2015
 *      Author: Eslattery1
 *      notes: http://www.geisswerks.com/ryan/FAQS/timing.html
 */
#ifndef TELEMETRY_H_
#define TELEMETRY_H_

#include <iomanip>
#include <iostream>
#include <windows.h>	// gross!

using std::cout;
using std::endl;

/// class for returning the sort performance data
/// From gist.github.com/gongzhitaao/7062087
/// stackoverflow.com/questions/275004/
class Telemetry{
public:
    Telemetry();

    /// Mutators
    void      Reset();
    long long Elapsed() const;
    long long RecordTime();
    void      Compare(int num = 1);
    void      Assign (int num = 1);

    /// Accessors
    unsigned long GetCompares();
    unsigned long GetAssigns();
    double        GetNanoSec();

private:

    LARGE_INTEGER freq;		// counter frequency in Hz
    LARGE_INTEGER startPoint;	// counter value of start point
    long long     elapsed;		// ticks from start to end point
    unsigned long comparisons;	// counter for number of comparison operations
    unsigned long assignments;	// counter for number of assignment operations
};

#endif /* TELEMETRY_H_ */
