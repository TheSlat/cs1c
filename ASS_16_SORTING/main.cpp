/**
 * @file	main.cpp
 * @brief	Assignment 16 - Sorting
 *
 * Sort three arrays (list1, list 2, and list3) five times.
 *
 * list1 = {1, 2, 3, ...,98, 99, 100}
 * list2 = {100, 99, 98, ..., 3, 2, 1}
 * list3 = {100, 5, 92, ..., 98, 2, 88}
 *
 * Sort ascending (bubble, insertion, selection,
 * quick (use a random number between 1 and 100 as the pivot),
 * merge the arrays where
 * 1.	list1 is initially in ascending order
 * 2.	list2 is initially in descending order
 * 3.	list3 is initially in random order
 *
 * a.	Print out the first and last 10 array entries of list1, list2,
 *      and list3 before sorting.  Also print out the first and last 10 array
 *      entries after each sort (bubble, insertion, selection, quick, merge).
 *
 * b.	Output the number of comparisons and item assignments made during each
 * 	    sort.  There should be 15 entries.
 *
 * c.	Output the execution time in nanoseconds for each sort. There should
 *      be 15 entries.
 *
 * @author	Ethan Slattery
 * @date	02DEC2015
 */
#include "sorting.h"

int main()
{
	/**
	 * Problems with large arrays due to stock overflows, description:
	 * stackoverflow.com/questions/1847789/
	 * http://stackoverflow.com/questions/79923/
	 */
	const int AR_SIZE   = 100000;
	const int DISP_SIZE = 10;
	int *ar1 = new int[AR_SIZE];
	int *ar2 = new int[AR_SIZE];
	int *ar3 = new int[AR_SIZE];
    Telemetry arOut[5][3];

	FillArrays(ar1, ar2, ar3, AR_SIZE);

    cout << PrintAR(ar1, DISP_SIZE, AR_SIZE);
    cout << PrintAR(ar2, DISP_SIZE, AR_SIZE);
	cout << PrintAR(ar3, DISP_SIZE, AR_SIZE) << endl;

    arOut[1][0] = BubbleSort(ar1, AR_SIZE);
    arOut[1][1] = BubbleSort(ar2, AR_SIZE);
    arOut[1][2] = BubbleSort(ar3, AR_SIZE);

    cout << PrintAR(ar1, DISP_SIZE, AR_SIZE);
    cout << PrintAR(ar2, DISP_SIZE, AR_SIZE);
    cout << PrintAR(ar3, DISP_SIZE, AR_SIZE) << endl;
//
//    cout << "BubbleSort on Ordered Array made " << arOut[1][0].comparisons << " comparisons and "
//         << arOut[1][0].assignments << " assignments in " << arOut[1][0].ticks << " time" << endl;
//
//    cout << "BubbleSort on Reversed Array made " << arOut[1][1].comparisons << " comparisons and "
//         << arOut[1][1].assignments << " assignments in " << arOut[1][1].ticks << " time" << endl;
//
//    cout << "BubbleSort on Random Array made " << arOut[1][2].comparisons << " comparisons and "
//         << arOut[1][2].assignments << " assignments in " << arOut[1][2].ticks << " time" << endl;
	return 0;
}
