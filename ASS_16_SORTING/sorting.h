//
// Created by Ethan on 12/1/2015.
//

#ifndef LAB_16_SORTING_SORTING_H
#define LAB_16_SORTING_SORTING_H

#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>	// shuffle for random list
#include <math.h>       // log10
#include "Telemetry.h"
using namespace std;

/// Function to load the arrays with the initial values
void FillArrays(int ar1[], int ar2[], int ar3[], const int AR_SIZE);

/// Prints the array to STDOUT according to assignment guidelines
string PrintAR(int *ar, int numOut, const int AR_SIZE);

/// Sorts the array using the bubble sort
Telemetry BubbleSort(int intAr[], const int AR_SIZE);

/// Sorts the Array using the insertion sort
Telemetry InsertionSort(int intAr[], const int AR_SIZE);

/// Sorts the array using the Selection Sort
Telemetry SelectionSort(int intAr[], const int AR_SIZE);

#endif //LAB_16_SORTING_SORTING_H
