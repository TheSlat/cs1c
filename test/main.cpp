#include <iostream>
#include <cstring>
#include <iomanip>

using namespace std;

float SalesTax (float numRetail, float numRate);
float TotalSale (float numRetail, float numTax);

int main ()
{
	float retailPrice;
	float salesTaxRate;
	float totalSale;
	float salesTax;

	cout << "Retail Price: ";
	cin >> retailPrice;

	cout << "\nSales Tax Rate: ";
	cin >> salesTaxRate;

	salesTax = SalesTax (retailPrice, salesTaxRate);
	totalSale= TotalSale (retailPrice, salesTax);

	cout << "\nYour sales tax is " << setprecision(2) << fixed << salesTax<< endl;
	cout << "Your total sale is " << setprecision(2) << fixed << totalSale << endl;

return 0;
}
