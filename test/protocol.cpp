
using namespace std;

float SalesTax (float numRetail, float numRate)
{
	return numRetail * numRate;
}

float TotalSale (float numRetail, float numTax)
{
	return numRetail + numTax;
}
