/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Employee.h"
/******************************
 ** Constructor & Destructor **
 ******************************/
/*************************************************************************
 * Constructor: Employee
 *------------------------------------------------------------------------
 *   This constructor initiates deck object to a fresh deck of cards using the
 *   Initialize() method
 *************************************************************************/
Employee::Employee()
{
	SetID(0);
	SetAge(0);
	SetGender(OTHER);
	SetSalary(0.0);
}

/*************************************************************************
 * Constructor: Employee - NON DEFAULT CONSTRUCTOR
 *------------------------------------------------------------------------
 *   This constructor initiates deck object to a fresh deck of cards using the
 *   Initialize() method
 *************************************************************************/
Employee::Employee(string newName,
				   int    newID,
				   string newPhone,
				   unsigned short newAge,
				   char   newGender,
				   string newJob,
				   float  newSalary,
				   unsigned short newMonth,
				   unsigned short newDay,
				   unsigned short newYear)
{
	SetName(newName);
	SetID(newID);
	SetName(newName);
	SetID(newID);
	SetPhone(newPhone);
	SetAge(newAge);
	SetGender(newGender);
	SetJob(newJob);
	SetSalary(newSalary);
	SetHireDate(newMonth, newDay, newYear);
}

/*************************************************************************
 * Destructor: Employee
 *------------------------------------------------------------------------
 *   This destructor does nothing in particular
 *************************************************************************/
Employee::~Employee()
{
}

/**************
 ** MUTATORS **
 **************/
/*************************************************************************
 * Method SetName: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee name
 *************************************************************************/
void Employee::SetName(string newName)	//IN - The new name for the employee
{
	empName = newName;
}

/*************************************************************************
 * Method SetID: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee ID
 *************************************************************************/
void Employee::SetID(int newID)	//IN - The new ID number
{
	empID = newID;
}

/*************************************************************************
 * Method SetPhone: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee Phone Number
 *************************************************************************/
void Employee::SetPhone(string newPhone)	//IN - The new phone number
{
	phoneNum = newPhone;
}

/*************************************************************************
 * Method SetAge: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee age
 *************************************************************************/
void Employee::SetAge(unsigned short newAge)	//IN - The new age
{
	empAge = newAge;
}

/*************************************************************************
 * Method SetGender: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee Gender
 *************************************************************************/
void Employee::SetGender(char newGender)	//IN - The new gender
{
	switch(toupper(newGender))
	{
		case 'M'	: 	empGender = MALE;
						break;
		case 'F'	:	empGender = FEMALE;
						break;
		case 'T'	:	empGender = TRANS;
						break;
		default		:	empGender = OTHER;
	}
}

/*************************************************************************
 * Method SetJob: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee job title
 *************************************************************************/
void Employee::SetJob(string newJob)	//IN - The new job title
{
	jobTitle = newJob;
}

/*************************************************************************
 * Method SetSalary: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee salary
 *************************************************************************/
void Employee::SetSalary(float newSalary)	//IN - The new salary
{
	salary = newSalary;
}

/*************************************************************************
 * Method SetHireDate: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee salary
 *************************************************************************/
void Employee::SetHireDate(unsigned short month,	//IN - The month of hire
						   unsigned short day,		//IN - The day of hire
						   unsigned short year)		//IN - The year of hire
{
	hireDate.SetDate(month, day, year);
}

/*************************************************************************
 * Method SetHireDate: Class Employee
 *------------------------------------------------------------------------
 *   This method Sets the employee salary using a date object
 *************************************************************************/
void Employee::SetHireDate(Date newHireDate)	//IN - Date in
{
	hireDate = newHireDate;
}
