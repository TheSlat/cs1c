/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Programmer.h"
/******************************
 ** Constructor & Destructor **
 ******************************/
/*************************************************************************
 * Constructor: Programmer
 *------------------------------------------------------------------------
 *   This constructor initiates deck object to a fresh deck of cards using the
 *   Initialize() method
 *************************************************************************/
Programmer::Programmer()
{
	SetID(0);
	SetAge(0);
	SetGender(OTHER);
	SetSalary(0.0);
	SetDepartment(0);
	SetPercentRaise(0.0);
	SetCPPKnowledge(false);
	SetJavaKnowledge(false);
}

/*************************************************************************
 * Constructor: Programmer - NON DEFAULT CONSTRUCTOR
 *------------------------------------------------------------------------
 *   This constructor initiates deck object to a fresh deck of cards using the
 *   Initialize() method
 *************************************************************************/
Programmer::Programmer (string newName,
						int    newID,
						string newPhone,
						unsigned short newAge,
						char   newGender,
						string newJob,
						float  newSalary,
						unsigned short newMonth,
						unsigned short newDay,
						unsigned short newYear,
						unsigned int newDepartmentNum,
						string newSupervisorName,
						float  newPercentRaise,
						bool   newCPPKnowledge,
						bool   newJavaKnowledge)
{
	SetName(newName);
	SetID(newID);
	SetName(newName);
	SetID(newID);
	SetPhone(newPhone);
	SetAge(newAge);
	SetGender(newGender);
	SetJob(newJob);
	SetSalary(newSalary);
	SetHireDate(newMonth, newDay, newYear);
	SetDepartment(newDepartmentNum);
	SetSupervisor(newSupervisorName);
	SetPercentRaise(newPercentRaise);
	SetCPPKnowledge(newCPPKnowledge);
	SetJavaKnowledge(newJavaKnowledge);
}

/*************************************************************************
 * Destructor: Programmer
 *------------------------------------------------------------------------
 *   This destructor does nothing in particular
 *************************************************************************/
Programmer::~Programmer()
{
}

/**************
 ** MUTATORS **
 **************/
/*************************************************************************
 * Method SetDepartment: Class Programmer
 *------------------------------------------------------------------------
 *   This method Sets the programmers department
 *************************************************************************/
void Programmer::SetDepartment(unsigned int newDepartmentNum)//IN - dept number
{
	departmentNum = newDepartmentNum;
}

/*************************************************************************
 * Method SetSupervisor: Class Programmer
 *------------------------------------------------------------------------
 *   This method Sets the programmers supervisor
 *************************************************************************/
void Programmer::SetSupervisor(string NewSupervisorName)//IN - supervisor name
{
	supervisorName = NewSupervisorName;
}

/*************************************************************************
 * Method SetPercentRaise: Class Programmer
 *------------------------------------------------------------------------
 *   This method Sets the programmers last raise in percent
 *************************************************************************/
void Programmer::SetPercentRaise(float newPercentRaise)
{
	percentRaise = newPercentRaise;
}

/*************************************************************************
 * Method SetCPPKnowledge: Class Programmer
 *------------------------------------------------------------------------
 *   This method Sets the programmers C++ ability
 *************************************************************************/
void Programmer::SetCPPKnowledge(bool newCPPKnowledge)
{
	cppKnowledge = newCPPKnowledge;
}

/*************************************************************************
 * Method SetJavaKnowledge: Class Programmer
 *------------------------------------------------------------------------
 *   This method Sets the programmers Java ability
 *************************************************************************/
void Programmer::SetJavaKnowledge(bool newJavaKnowledge)
{
	javaKnowledge = newJavaKnowledge;
}

/****************
 ** ACCCESSORS **
 ****************/
/*************************************************************************
 * Method GetDepartment: Class Programmer
 *------------------------------------------------------------------------
 *   This method Gets the programmers department
 *************************************************************************/
unsigned int Programmer::GetDepartment() const
{
	return departmentNum;
}

/*************************************************************************
 * Method GetSupervisor: Class Programmer
 *------------------------------------------------------------------------
 *   This method Gets the programmers Supervisor
 *************************************************************************/
string Programmer::GetSupervisor() const
{
	return supervisorName;
}

/*************************************************************************
 * Method GetPercentRaise: Class Programmer
 *------------------------------------------------------------------------
 *   This method Gets the programmers last % raise
 *************************************************************************/
float Programmer::GetPercentRaise() const
{
	return percentRaise;
}

/*************************************************************************
 * Method GetCPPKnowledge: Class Programmer
 *------------------------------------------------------------------------
 *   This method Gets the programmers C++ ability
 *************************************************************************/
bool Programmer::GetCPPKnowledge() const
{
	return cppKnowledge;
}

/*************************************************************************
 * Method GetJavaKnowledge: Class Programmer
 *------------------------------------------------------------------------
 *   This method Gets the programmers Java ability
 *************************************************************************/
bool Programmer::GetJavaKnowledge() const
{
	return javaKnowledge;
}

/*************************************************************************
 * Method PrintHeader: Class Programmer
 *------------------------------------------------------------------------
 *   This method Print a header for employee info
 *************************************************************************/
string Programmer::PrintHeader() const
{
	ostringstream output;
	output << left
		   << Employee::PrintHeader()
		   << setw(DEPT_COL) << "DEPARTMENT" << "| "
		   << setw(SUP_COL) << "SUPERVISOR" << "| "
		   << setw(RAISE_COL) << "LAST RAISE" << "| "
		   << setw(CPP_COL) << "C++" << "| "
		   << setw(JAVA_COL) << "JAVA" << "| "
		   << right;
	return output.str();
}

/*************************************************************************
 * Method PrintLine: Class Programmer
 *------------------------------------------------------------------------
 *   This method Print a line for use in print output
 *************************************************************************/
string Programmer::PrintLine() const
{
	ostringstream output;
	output << Employee::PrintLine();
	output << setfill('-')
		   << setw(DEPT_COL + SUP_COL + RAISE_COL
				   + CPP_COL + JAVA_COL + 10)
	       << "-";
	return output.str();
}

/*************************************************************************
 * Method Print: Class Programmer
 *------------------------------------------------------------------------
 *   This method Print the employee info
 *************************************************************************/
string Programmer::Print() const
{
	ostringstream output;
	output << Employee::Print();
	output << left
		   << setw(DEPT_COL)    << GetDepartment() << "| "
		   << setw(SUP_COL)     << GetSupervisor() << "| "
		   << setw(RAISE_COL-9) << setprecision(1) << fixed
		   	   	   	   	   	    << GetPercentRaise() << "%        | "
		   << setw(CPP_COL)     << GetCPPKnowledge() << "| "
		   << setw(JAVA_COL)    << GetJavaKnowledge() << "| "
		   << right;
	return output.str();
}
