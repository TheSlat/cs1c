/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#ifndef EMPLOYEECLASSES_PROGRAMMER_H_
#define EMPLOYEECLASSES_PROGRAMMER_H_
#include "Employee.h"

class Programmer : public Employee
{
	public:
	/* CONSTRUCTOR & DESTRUCTOR */
		Programmer();
		~Programmer();
		Programmer(	string newName,
					int    newID,
					string newPhone,
					unsigned short newAge,
					char   newGender,
					string newJob,
					float  newSalary,
					unsigned short newMonth,
					unsigned short newDay,
					unsigned short newYear,
					unsigned int newDepartmentNum,
					string newSupervisorName,
					float  newPercentRaise,
					bool   newCPPKnowledge,
					bool   newJavaKnowledge);
	/* MUTATORS*/
		void SetDepartment(unsigned int newDepartmentNum);
		void SetSupervisor(string newSupervisorName);
		void SetPercentRaise(float newPercentRaise);
		void SetCPPKnowledge(bool newCPPKnowledge);
		void SetJavaKnowledge(bool newJavaKnowledge);
	/* ACCCESSORS */
		unsigned int GetDepartment() const;
		string GetSupervisor() const;
		float  GetPercentRaise() const;
		bool   GetCPPKnowledge() const;
		bool   GetJavaKnowledge() const;
		string PrintHeader() const;
		string PrintLine() const;
		string Print() const;

	private:
		unsigned int departmentNum;
		string		 supervisorName;
		float		 percentRaise;
		bool		 cppKnowledge;
		bool		 javaKnowledge;
		static const int DEPT_COL  = 15;
		static const int SUP_COL   = 15;
		static const int RAISE_COL = 12;
		static const int CPP_COL   = 7;
		static const int JAVA_COL  = 7;
};
#endif /* EMPLOYEECLASSES_PROGRAMMER_H_ */
