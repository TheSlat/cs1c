/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#ifndef EMPLOYEECLASSES_ARCHITECT_H_
#define EMPLOYEECLASSES_ARCHITECT_H_
#include "Employee.h"

class Architect : public Employee
{
	public:
	/* CONSTRUCTOR & DESTRUCTOR */
		Architect();
		~Architect();
		Architect(string newName,
				  int    newID,
				  string newPhone,
				  unsigned short newAge,
				  char   newGender,
				  string newJob,
				  float  newSalary,
				  unsigned short newMonth,
				  unsigned short newDay,
				  unsigned short newYear,
				  unsigned int newDepartmentNum,
				  string newSupervisorName,
				  float  newPercentRaise,
				  unsigned int newYearsExp);
	/* MUTATORS*/
		void SetDepartment(unsigned int newDepartmentNum);
		void SetSupervisor(string newSupervisorName);
		void SetPercentRaise(float newPercentRaise);
		void SetYearsExp(unsigned int newYearsExp);
	/* ACCCESSORS */
		unsigned int GetDepartment() const;
		string GetSupervisor() const;
		float  GetPercentRaise() const;
		unsigned int GetYearsExp() const;
		string PrintHeader() const;
		string PrintLine() const;
		string Print() const;

	private:
		unsigned int departmentNum;
		string		 supervisorName;
		float		 percentRaise;
		unsigned int yearsExp;
		static const int DEPT_COL  = 15;
		static const int SUP_COL   = 15;
		static const int RAISE_COL = 12;
		static const int YEARS_COL = 10;
};
#endif /* EMPLOYEECLASSES_ARCHITECT_H_ */
