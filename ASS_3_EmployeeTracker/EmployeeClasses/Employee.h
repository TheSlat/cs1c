/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#ifndef EMPLOYEECLASSES_EMPLOYEE_H_
#define EMPLOYEECLASSES_EMPLOYEE_H_

#include <iomanip>
#include <string.h>
#include <sstream>
#include "..\Date\date.h"

class Employee
{
	public:
		/* CONSTRUCTOR & DESTRUCTOR */
		Employee();
		~Employee();
		Employee(string newName,
			     int    newID,
			     string newPhone,
			     unsigned short newAge,
			     char   newGender,
			     string newJob,
			     float  newSalary,
				 unsigned short newMonth,
		  	     unsigned short newDay,
			     unsigned short newYear);

		/* MUTATORS*/
		void SetName(string newName);
		void SetID(int newID);
		void SetPhone(string newPhone);
		void SetAge(unsigned short newAge);
		void SetGender(char newGender);
		void SetJob(string newJob);
		void SetSalary(float newSalary);
		void SetHireDate(Date newHireDate);
		void SetHireDate(unsigned short month,
						 unsigned short day,
						 unsigned short year);

		/* ACCCESSORS */
		string GetName() const;
		int    GetID() const;
		string GetPhone() const;
		unsigned int  GetAge() const;
		string GetGender() const;
		string GetJob() const;
		float  GetSalary() const;
		Date   GetHireDate() const;
		string PrintHeader() const;
		string PrintLine() const;
		string Print() const;

	protected:
		enum Gender
		{
			MALE,
			FEMALE,
			TRANS,
			OTHER
		};

	private:
		string empName;
		int    empID;
		string phoneNum;
		unsigned short  empAge;
		Gender empGender;
		string jobTitle;
		float  salary;
		Date   hireDate;
		static const int NAME_COL = 15;
		static const int ID_COL = 9;
		static const int PHONE_COL = 13;
		static const int AGE_COL = 3;
		static const int GENDER_COL = 7;
		static const int JOB_COL = 15;
		static const int SAL_COL = 12;
		static const int DATE_COL = 12;
};
#endif /* EMPLOYEECLASSES_EMPLOYEE_H_ */
