/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "header.h"
/*************************************************************************
 * FUNCTION SetValues
 *________________________________________________________________________
 * This function sets the values of an employee object to the passed values
 * 	- returns nothing
 *************************************************************************/
void SetValues(Employee &employee,
			  string newName,
			  int    newID,
	 	      string newPhone,
	 	      unsigned short newAge,
	 	      char   newGender,
			  string newJob,
			  float  newSalary,
			  unsigned short newMonth,
			  unsigned short newDay,
			  unsigned short newYear)
{
	employee.SetName(newName);
	employee.SetID(newID);
	employee.SetName(newName);
	employee.SetID(newID);
	employee.SetPhone(newPhone);
	employee.SetAge(newAge);
	employee.SetGender(newGender);
	employee.SetJob(newJob);
	employee.SetSalary(newSalary);
	employee.SetHireDate(newMonth, newDay, newYear);
}

/*************************************************************************
 * FUNCTION SetValues
 *________________________________________________________________________
 * This function sets the values of an programmer object to the passed values
 * 	- returns nothing
 *************************************************************************/
void SetValues(Programmer &programmer,
			  string newName,
			  int    newID,
	 	      string newPhone,
	 	      unsigned short newAge,
	 	      char   newGender,
			  string newJob,
			  float  newSalary,
			  unsigned short newMonth,
			  unsigned short newDay,
			  unsigned short newYear,
			  unsigned int   newDepartmentNum,
			  string newSupervisorName,
			  float  newPercentRaise,
			  bool   newCPPKnowledge,
			  bool   newJavaKnowledge)
{
	programmer.SetName(newName);
	programmer.SetID(newID);
	programmer.SetName(newName);
	programmer.SetID(newID);
	programmer.SetPhone(newPhone);
	programmer.SetAge(newAge);
	programmer.SetGender(newGender);
	programmer.SetJob(newJob);
	programmer.SetSalary(newSalary);
	programmer.SetHireDate(newMonth, newDay, newYear);
	programmer.SetDepartment(newDepartmentNum);
	programmer.SetSupervisor(newSupervisorName);
	programmer.SetPercentRaise(newPercentRaise);
	programmer.SetCPPKnowledge(newCPPKnowledge);
	programmer.SetJavaKnowledge(newJavaKnowledge);
}

/*************************************************************************
 * FUNCTION SetValues
 *________________________________________________________________________
 * This function sets the values of an Architect object to the passed values
 * 	- returns nothing
 *************************************************************************/
void SetValues(Architect &arch,
			  string newName,
			  int    newID,
			  string newPhone,
			  unsigned short newAge,
			  char   newGender,
			  string newJob,
			  float  newSalary,
			  unsigned short newMonth,
			  unsigned short newDay,
			  unsigned short newYear,
			  unsigned int newDepartmentNum,
			  string newSupervisorName,
			  float  newPercentRaise,
			  unsigned int newYearsExp)
{
	arch.SetName(newName);
	arch.SetID(newID);
	arch.SetName(newName);
	arch.SetID(newID);
	arch.SetPhone(newPhone);
	arch.SetAge(newAge);
	arch.SetGender(newGender);
	arch.SetJob(newJob);
	arch.SetSalary(newSalary);
	arch.SetHireDate(newMonth, newDay, newYear);
	arch.SetDepartment(newDepartmentNum);
	arch.SetSupervisor(newSupervisorName);
	arch.SetPercentRaise(newPercentRaise);
	arch.SetYearsExp(newYearsExp);
}
