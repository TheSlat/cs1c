/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#ifndef HEADER_H_
#define HEADER_H_

#include "Date\Date.h"
#include "EmployeeClasses\Employee.h"
#include "EmployeeClasses\Programmer.h"
#include "EmployeeClasses\Architect.h"
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

//Function Prototypes
/*************************************************************************
 * PrintHeaderString
 *  This function receives an ostream variable, assignment name, type,
 * 	number, and programmers name. The function then returns the
 * 	appropriate header via reference through the ostream variable.
 * 	- returns nothing
 *************************************************************************/
void PrintHeaderOstream(ostream &output,	// OUT - output stream
						string  assName,	// IN - Assignment Name
						char    assType,	// IN - assignment Type
			 	 	 						//     (Lab or Assignment)
						string  assNum,		// IN - assignment number
						string  progName);	// IN - Programmers Name

/*************************************************************************
 * FUNCTION SetValues
 *________________________________________________________________________
 * This function sets the values of an employee object to the passed values
 * 	- returns nothing
 *************************************************************************/
void SetValues(Employee &employee,
			  string newName,
			  int    newID,
	 	      string newPhone,
	 	      unsigned short newAge,
	 	      char   newGender,
			  string newJob,
			  float  newSalary,
			  unsigned short newMonth,
			  unsigned short newDay,
			  unsigned short newYear);

/*************************************************************************
 * FUNCTION SetValues
 *________________________________________________________________________
 * This function sets the values of an programmer object to the passed values
 * 	- returns nothing
 *************************************************************************/
void SetValues(Programmer &programmer,
			  string newName,
			  int    newID,
	 	      string newPhone,
	 	      unsigned short newAge,
	 	      char   newGender,
			  string newJob,
			  float  newSalary,
			  unsigned short newMonth,
			  unsigned short newDay,
			  unsigned short newYear,
			  unsigned int   newDepartmentNum,
			  string newSupervisorName,
			  float  newPercentRaise,
			  bool   newCPPKnowledge,
			  bool   newJavaKnowledge);

/*************************************************************************
 * FUNCTION SetValues
 *________________________________________________________________________
 * This function sets the values of an Architect object to the passed values
 * 	- returns nothing
 *************************************************************************/
void SetValues(Architect &arch,
			  string newName,
			  int    newID,
			  string newPhone,
			  unsigned short newAge,
			  char   newGender,
			  string newJob,
			  float  newSalary,
			  unsigned short newMonth,
			  unsigned short newDay,
			  unsigned short newYear,
			  unsigned int newDepartmentNum,
			  string newSupervisorName,
			  float  newPercentRaise,
			  unsigned int newYearsExp);

#endif /* HEADER_H_ */
