/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "header.h"
/***********************************************************************
 * ASSIGNMENT #3 - This program manages several classes of employees.
 * 				   The basic employee class and two derived calsses,
 * 				   the programmer and the architect. The client creates
 * 				   different employees and then tests them by changing the
 * 				   attributes one at a time.
 ***********************************************************************/
int main()
{
	/* CONSTANTS FOR THE CLASS HEADER */
	const string ASS_NAME	= "Employee Manager";
	const char   ASS_TYPE	= 'A';
	const string ASS_NUM	= "3";
	const string PRO_NAME   = "Ethan Slattery";

	/* OUTPUT - CLASS HEADING */
	PrintHeaderOstream(cout, ASS_NAME, ASS_TYPE, ASS_NUM, PRO_NAME);

	/* VARIABLE DECLARATIONS */
	//Create Employees with NON-DEFAULT constructor
	Employee jimmy("Jimmy Fallon", 12345, "949-555-1234", 40,
				   'M', "Comedian", 100000.00, 8, 31, 2014);
	Employee stephen("Stephan Colbert", 12346, "310-555-5555", 51, 'M',
					 "Comedian", 70123, 5, 8, 2015);
	Employee james("James Corden", 87654, "703-703-1234", 37, 'M',
			       "Talk Show Host", 900000, 12, 25, 2014);

	//Create Employees with DEFAULT constructor
	Employee katie;

	//Create Programmer with NON-DEFAULT constructor
	Programmer sam("Sam Software", 54321, "819-123-4567", 21, 'M',
			   	   "Programmer", 223000.00, 12, 24, 2011, 5432122,
				   "Joe Boss", 4.0, true, false);
	//Create Programmers with DEFAULT constructor
	Programmer mary;

	//Create Architect with NON-DEFAULT constructor
	Architect alex("Alex Arch", 88888, "819-123-4444", 31, 'M',
			       "Architect", 323000.00, 12, 24, 2009, 5434222,
				   "Big Boss", 5.0, 4);
	//Create Architects with DEFAULT constructor
	Architect sally;

	cout << "/********************************\n"
		 << " *  TESTING THE EMPLOYEE CLASS  *\n"
		 << " ********************************/\n";
	cout << ":: EMPLOYEES ::\n";
	cout << jimmy.PrintLine() << endl;
	cout << jimmy.PrintHeader() << endl;
	cout << jimmy.PrintLine() << endl;
	cout << jimmy.Print() << endl;
	cout << stephen.Print()<< endl;
	cout << james.Print() << endl;
	cout << katie.Print() << endl << katie.PrintLine();

	cout << "\nKatie Couric was created with default values."
			"\nValues will now be set using the methods\n\n";

	/* Set values using the employee SET methods */
	SetValues(katie, "Katie Couric", 77777, "203-555-6789", 58, 'F',
			  "News Reporter", 500500, 3, 1, 2005);

	cout << ":: EMPLOYEES - UPDATED ::\n";
	cout << jimmy.PrintLine() << endl;
	cout << jimmy.PrintHeader() << endl;
	cout << jimmy.PrintLine() << endl;
	cout << jimmy.Print() << endl;
	cout << stephen.Print()<< endl;
	cout << james.Print() << endl;
	cout << katie.Print() << endl << katie.PrintLine();
	cout << endl << endl;

	cout << "/**********************************\n"
		 << " *  TESTING THE PROGRAMMER CLASS  *\n"
		 << " **********************************/\n";
	cout << ":: PROGRAMMERS ::\n";
	cout << sam.PrintLine() << endl;
	cout << sam.PrintHeader() << endl;
	cout << sam.PrintLine() << endl;
	cout << sam.Print() << endl;
	cout << mary.Print()<< endl;
	cout << sam.PrintLine() << endl << endl;

	cout << "\Mary Coder was created with default values."
			"\nValues will now be set using the methods\n\n";

	/* Set values using the employee SET methods */
	SetValues(mary, "Mary Coder", 6543222, "310-555-5555", 28, 'F',
			  "Programmer", 770123.00, 2, 8, 2010, 6543222, "Mary Leader",
			  7.0, true, true);

	cout << ":: PROGRAMMERS - UPDATED::\n";
	cout << sam.PrintLine() << endl;
	cout << sam.PrintHeader() << endl;
	cout << sam.PrintLine() << endl;
	cout << sam.Print() << endl;
	cout << mary.Print()<< endl;
	cout << sam.PrintLine() << endl << endl;

	cout << "/*********************************\n"
		 << " *  TESTING THE ARCHITECT CLASS  *\n"
		 << " *********************************/\n";
	cout << ":: ARCHITECTS ::\n";
	cout << alex.PrintLine() << endl;
	cout << alex.PrintHeader() << endl;
	cout << alex.PrintLine() << endl;
	cout << alex.Print() << endl;
	cout << sally.Print()<< endl;
	cout << alex.PrintLine() << endl << endl;

	cout << "\nSally Designer was created with default values."
			"\nValues will now be set using the methods\n\n";

	/* Set values using the employee SET methods */
	SetValues(sally, "Sally Designer", 87878, "310-555-8888", 38, 'F',
			  "Architect", 870123.00, 2, 8, 2003, 6543422, "Big Boss",
			  8.0, 11);

	cout << ":: ARCHITECTS - UPDATED::\n";
	cout << alex.PrintLine() << endl;
	cout << alex.PrintHeader() << endl;
	cout << alex.PrintLine() << endl;
	cout << alex.Print() << endl;
	cout << sally.Print()<< endl;
	cout << alex.PrintLine() << endl << endl;

	return 0;
}
