/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Date.h"
/*************************************************************************
 * Method GetDate: Class Date
 *------------------------------------------------------------------------
 *   Gets and returns the day, month, and year by reference to variables
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    month: month of the date
 *    day  : day of the date
 *    year : year of the date
 *
 *  POST-CONDITIONS:
 *    The Date is returned back to main through reference
 *************************************************************************/
void Date::GetDate(unsigned short &month,
			       unsigned short &day,
			       unsigned short &year) const
{
	month = dateMonth;
	day   = dateDay;
	year  = dateYear;
}

/*************************************************************************
 * Method IsThisTheFuture: Class Date
 *------------------------------------------------------------------------
 *   checks to see if the passed date is AFTER the date object
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    Returns TRUE if the passed date is in the future
 *************************************************************************/
bool Date::IsThisTheFuture(Date checkDate) const
{
	bool thisIsTheFuture;
	unsigned short checkMonth;
	unsigned short checkDay;
	unsigned short checkYear;
	thisIsTheFuture = false;

	checkDate.GetDate(checkMonth, checkDay, checkYear);
	if(checkYear >= dateYear)
	{
		if(checkYear == dateYear)
		{
			if(checkMonth == dateMonth)
			{
				if(checkDay > dateDay)
				{
					thisIsTheFuture = true;
				}
				else
				{
					thisIsTheFuture = false;
				}
			}
			else if(checkMonth > dateMonth)
			{
				thisIsTheFuture = true;
			}
		}
		else
		{
			thisIsTheFuture = true;
		}
	}
	return thisIsTheFuture;
}

/*************************************************************************
 * Method IsThisThePast: Class Date
 *------------------------------------------------------------------------
 *   checks to see if the passed date is AFTER the date object
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    Returns TRUE if the passed date is in the future
 *************************************************************************/
bool Date::IsThisThePast(Date checkDate) const
{
	bool thisIsThePast;
	unsigned short checkMonth;
	unsigned short checkDay;
	unsigned short checkYear;
	thisIsThePast = false;

	checkDate.GetDate(checkMonth, checkDay, checkYear);
	if(checkYear <= dateYear)
	{
		if(checkYear == dateYear)
		{
			if(checkMonth == dateMonth)
			{
				if(checkDay < dateDay)
				{
					thisIsThePast = true;
				}
				else
				{
					thisIsThePast = false;
				}
			}
			else
			{
				thisIsThePast = true;
			}
		}
		else
		{
			thisIsThePast = true;
		}
	}

	return thisIsThePast;
}

/*************************************************************************
 * Method IsThisTheSame: Class Date
 *------------------------------------------------------------------------
 *   checks to see if the passed date is the same as the date object
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    Returns TRUE if the passed date is today. Carpe Diem!
 *************************************************************************/
bool Date::IsThisTheSame(Date checkDate) const
{
	unsigned short checkMonth;
	unsigned short checkDay;
	unsigned short checkYear;
	checkDate.GetDate(checkMonth, checkDay, checkYear);

	return (checkMonth == dateMonth &&
			checkDay   == dateDay &&
			checkYear  == dateYear);
}

/*************************************************************************
 * Method GetYear: Class Date
 *------------------------------------------------------------------------
 *   Gets and returns the year by reference back to main
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    The Year is returned back to main through reference
 *************************************************************************/
unsigned short Date::GetYear()  const
{
	return dateYear;
}

/*************************************************************************
 * Method GetMonth: Class Date
 *------------------------------------------------------------------------
 *   Gets and returns the month by reference back to main
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    The Month is returned back to main through reference
 *************************************************************************/
unsigned short Date::GetMonth() const
{
	return dateMonth;
}

/*************************************************************************
 * Method SetDate: Class Date
 *------------------------------------------------------------------------
 *   Gets and returns the day by reference back to main
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    The day is returned back to main through reference
 *************************************************************************/
unsigned short Date::GetDay()   const
{
	return dateYear;
}

/*************************************************************************
 * Method DisplayDate: Class Date
 *------------------------------------------------------------------------
 *   Displays the date in a MM/DD/YYYY format
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    Returns the date as a string back to main.
 *************************************************************************/
string Date::DisplayDate() const
{
	ostringstream output;

	output << (dateMonth < 10 ? "0" : "") << dateMonth << "/"
		   << (dateDay < 10 ? "0" : "") << dateDay << "/"
		   << dateYear;

	return output.str();
}
