/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "date.h"
/**********************************
 ** CONSTRUCTORS AND DESTRUCTORS **
 **********************************/

/*************************************************************************
 * Constructor: Class Date
 *------------------------------------------------------------------------
 *   Default constructor.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    sets the attributes to default values
 *************************************************************************/
Date::Date()
{
	dateMonth = 0;
	dateDay   = 0;
	dateYear  = 0;
}

/*************************************************************************
 * Constructor: Class Date - Non default #1
 *------------------------------------------------------------------------
 *   Non-Default constructor, allows assignment of values
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    sets the attributes to default values
 *************************************************************************/
Date::Date(unsigned short month,
		   unsigned short day,
	       unsigned short year)
{
	SetDate(month, day, year);
}

/*************************************************************************
 * Constructor: Class Date - Non default #2
 *------------------------------------------------------------------------
 *   Non-Default constructor, allows assignment of values
 *   	USES A STRING IN MM/DD/YYY format
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    Date as string in MM/DD/YYYY format, separation characters don't matter
 *
 *  POST-CONDITIONS:
 *    sets the attributes to default values
 *************************************************************************/
Date::Date(string newDate)
{
	SetDate(newDate);
}

/*************************************************************************
 * Destructor: Class Date
 *------------------------------------------------------------------------
 *   Default destructor, destructs.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
Date::~Date()
{}
