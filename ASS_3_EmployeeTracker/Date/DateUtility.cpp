/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Date.h"
/**********************************
 **        UTILITY METHODS       **
 **********************************/
/*************************************************************************
 * Method GetDaysInMonth: Class Date
 *------------------------------------------------------------------------
 *   Finds the number of days in a given month,
 *   taking into account leap years
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    month: month to check for number of days
 *
 *  POST-CONDITIONS:
 *    The number of days in the given month is returned
 *************************************************************************/
unsigned short Date::GetDaysInMonth(unsigned short month,		//IN - Month
		  	  	  	  	  	  	    unsigned short year) const  //IN - year
{
	unsigned short days;

	if(month == 2)
	{
		if(IsLeapYear(year))
		{
			days = 29;
		}
		else
		{
			days = 28;
		}
	}
	else if(month == 1 || month == 3 || month == 5 || month == 7 ||
			month == 8 || month == 10 || month == 12)
	{
		days = 31;
	}
	else
	{
		days = 30;
	}

	return days;
}

/*************************************************************************
 * Method IsLeapYear: Class Date
 *------------------------------------------------------------------------
 *   Finds out if the given year is the leap year
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    year:  The year to check
 *
 *  POST-CONDITIONS:
 *    Returns a bool, true if the year is a leap year
 *************************************************************************/
bool Date::IsLeapYear(unsigned short year) const
{
	return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
}

/**********************
 ** VALIDATE METHODS **
 **********************/

/*************************************************************************
 * Method ValidateMonth: Class Date
 *------------------------------------------------------------------------
 *   Validates if the input for the month is valid.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    month: month to check for valid input
 *
 *  POST-CONDITIONS:
 *    returns a bool if valid month has been entered.
 *************************************************************************/
bool Date::ValidateMonth(unsigned short month) const
{
	return (month <= 12 && month >= 1);
}

/*************************************************************************
 * Method ValidateYear: Class Date
 *------------------------------------------------------------------------
 *   Checks if a valid year has been entered.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    year: input to check for bounds
 *
 *  POST-CONDITIONS:
 *    returns a bool if valid year has been entered.
 *************************************************************************/
bool Date::ValidateYear(unsigned short year) const
{
	time_t now;
	tm *currentTime;

	now = time(NULL);
	currentTime = localtime(&now);

	unsigned short currentYear;

	currentYear = 1900 + currentTime -> tm_year;

	return (year <= currentYear && year >= 1900);
}

/*************************************************************************
 * Method ValidateDays: Class Date
 *------------------------------------------------------------------------
 *  Checks if a valid days has been entered.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    days: to be checked if correct number of days has been input
 *
 *  POST-CONDITIONS:
 *    returns a bool if the correct number of days has been input
 *************************************************************************/
bool Date::ValidateDay(unsigned short month,
				       unsigned short day,
				       unsigned short year) const
{
	unsigned short correctDays;
	correctDays = GetDaysInMonth(month, year);
	return (day <= correctDays && day >= 1);
}

/*************************************************************************
 * Method ValidateDate: Class Date
 *------------------------------------------------------------------------
 *  Checks if the correct date has been entered overall.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    month: month of the date
 *    day: day of the date
 *    year: year of the date
 *
 *  POST-CONDITIONS:
 *    returns a bool if the correct date overall has been input
 *************************************************************************/
bool Date::ValidateDate(unsigned short  month,
				        unsigned short  day,
				        unsigned short  year) const
{
	bool validDate;
	time_t now;
	tm *currentTime;
	unsigned short currentYear;
	unsigned short currentMonth;
	unsigned short currentDay;

	now = time(NULL);
	currentTime  = localtime(&now);
	currentYear  = 1900 + currentTime -> tm_year;
	currentMonth = currentTime->tm_mon + 1;
	currentDay   = currentTime->tm_mday;

	if(ValidateYear(year))
	{
		if(year == currentYear && month == currentMonth)
		{
			validDate = (day <= currentDay);
		}
		else if(year == currentYear)
		{
			validDate = (month < currentMonth &&
					     ValidateDay(day, month, year));
		}
		else
		{
			validDate = (ValidateMonth(month) &&
						 ValidateDay(month, day, year));
		}
	}
	else
	{
		validDate = false;
	}

	return validDate;
}
