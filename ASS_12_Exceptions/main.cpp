/**
 * @file	main.cpp
 * @brief	Assignment 12 - Exceptions
 *
 * Using the queue from assignment #11, add exceptions for errors and
 * use an assert macro somewhere. I changed the number of dequeues on the
 * final step to six so that the empty queue exception would get tested
 *
 * @author	Ethan Slattery
 * @date	29SEP2015
 */
#include "queue.h"
#include <iostream>
#include <cassert>

int main()
{
	ASS11::queue<int>    iQ;
	ASS11::queue<double> dQ;
	ASS11::queue<string> sQ;
	int input;

	/******************************/
	/*    USE THE ASSERT MACRO    */
	/******************************/
	std::cout << "I ASSERT that you will enter the number 5!\n"
				 "Please enter the number 5 to prevent an exit: ";
	std::cin >> input;
	assert(input == 5);
	std:: cout << "\nThank you for your cooperation!\n\n";

	/*****************************/
	/* TESTING THE INTEGER QUEUE */
	/*****************************/

	try	{
		std::cout << "**** ADDING TO THE INTEGER QUEUE 6 TIMES ****\n";
		for(int i = 1; i <= 6; i++)
		{
			iQ.enqueue(i);
			std::cout << iQ.print();
		}
	}
	catch(ASS11::queue<int>::Full) {
		std::cout << "Stack is FULL!!\n";
	}

	std::cout << "\n**** TESTING COPY CONSTRUCTOR BY MAKING A COPY ****\n";
	ASS11::queue<int> iQ2 = iQ;
	int size = iQ2.size();
	for(int i = 0; i < size; i++) {
		std::cout << "Removing " << iQ2.dequeue() << " from copy" << std::endl;
	}
	for(int j = 10; j < 15; j++) {
		iQ2.enqueue(j);
		std::cout << "Adding " << j << " to copy" << std::endl;
	}

	std::cout << "**** Origional and Copy are unique! ****\n";
	std::cout << "Origional Queue: " << iQ.print()
			  << "Copy of Queue: "   << iQ2.print();

	try {
		std::cout << "\n**** REMOVING FROM THE INTEGER QUEUE 4 TIMES ****\n";
		for(int i = 1; i <= 4; i++)
		{
			std::cout << "Removing " << iQ.dequeue() << " from the front\n";
					std::cout << iQ.print();
		}
	}
	catch(ASS11::queue<int>::Empty) {
		std::cout << "Stack is EMPTY!!\n";
	}

	try {
		std::cout << "\n**** ADDING TO THE INTEGER QUEUE 5 TIMES ****\n";
		for(int i = 7; i <= 11; i++)
		{
			iQ.enqueue(i);
			std::cout << iQ.print();
		}
	}
	catch(ASS11::queue<int>::Full) {
		std::cout << "Stack is FULL!!\n";
	}

	try {
		std::cout << "\n**** REMOVING FROM THE INTEGER QUEUE 6 TIMES ****\n";
		for(int i = 1; i <= 6; i++)
		{
			std::cout << "Removing " << iQ.dequeue() << " from the front\n";
			std::cout << iQ.print();
		}
	}
	catch(ASS11::queue<int>::Empty) {
		std::cout << "Stack is EMPTY!!\n";
	}

	/****************************/
	/* TESTING THE DOUBLE QUEUE */
	/****************************/

	try {
		std::cout << "\n**** ADDING TO THE DOUBLE QUEUE 6 TIMES ****\n";
		for(double i = 1.1; i <= 6.1; i++)
		{
			dQ.enqueue(i);
			std::cout << dQ.print();
		}
	}
	catch(ASS11::queue<double>::Full) {
		std::cout << "Stack is FULL!!\n";
	}

	try {
		std::cout << "\n**** REMOVING FROM THE DOUBLE QUEUE 4 TIMES ****\n";
		for(int i = 1; i <= 4; i++)
		{
			std::cout << "Removing " << dQ.dequeue() << " from the front\n";
			std::cout << dQ.print();
		}
	}
	catch(ASS11::queue<double>::Empty) {
		std::cout << "Stack is EMPTY!!\n";
	}

	try {
		std::cout << "\n**** ADDING TO THE DOUBLE QUEUE 5 TIMES ****\n";
		for(double i = 7.1; i <= 11.1; i++)
		{
			dQ.enqueue(i);
			std::cout << dQ.print();
		}
	}
	catch(ASS11::queue<double>::Full) {
		std::cout << "Stack is FULL!!\n";
	}

	try{
		std::cout << "\n**** REMOVING FROM THE DOUBLE QUEUE 6 TIMES ****\n";
		for(int i = 1; i <= 6; i++)
		{
			std::cout << "Removing " << dQ.dequeue() << " from the front\n";
			std::cout << dQ.print();
		}
	}
	catch(ASS11::queue<double>::Empty) {
		std::cout << "Stack is EMPTY!!\n";
	}

	/****************************/
	/* TESTING THE STRING QUEUE */
	/****************************/

	try {
		std::cout << "\n**** ADDING TO THE STRING QUEUE 6 TIMES ****\n";
		sQ.enqueue("one");
		std::cout << sQ.print();
		sQ.enqueue("two");
		std::cout << sQ.print();
		sQ.enqueue("three");
		std::cout << sQ.print();
		sQ.enqueue("four");
		std::cout << sQ.print();
		sQ.enqueue("five");
		std::cout << sQ.print();
		sQ.enqueue("six");
		std::cout << sQ.print();
	}
	catch(ASS11::queue<string>::Full) {
		std::cout << "Stack is FULL!!\n";
	}

	try {
		std::cout << "\n**** REMOVING FROM THE STRING QUEUE 4 TIMES ****\n";
		for(int i = 0; i < 4; i++)
		{
			std::cout << "Removing " << sQ.dequeue() << " from the front\n";
			std::cout << sQ.print();
		}
	}
	catch(ASS11::queue<string>::Empty) {
		std::cout << "Stack is EMPTY!!\n";
	}

	try {
		std::cout << "\n**** ADDING TO THE STRING QUEUE 5 TIMES ****\n";
		sQ.enqueue("seven");
		std::cout << sQ.print();
		sQ.enqueue("eight");
		std::cout << sQ.print();
		sQ.enqueue("nine");
		std::cout << sQ.print();
		sQ.enqueue("ten");
		std::cout << sQ.print();
		sQ.enqueue("eleven");
		std::cout << sQ.print();
	}
	catch(ASS11::queue<string>::Full) {
		std::cout << "Stack is FULL!!\n";
	}

	try {
		std::cout << "\n**** REMOVING FROM THE STRING QUEUE 6 TIMES ****\n";
		for(int i = 0; i < 6; i++)
		{
			std::cout << "Removing " << sQ.dequeue() << " from the front\n";
			std::cout << sQ.print();
		}
	}
	catch(ASS11::queue<string>::Empty) {
		std::cout << "Stack is EMPTY!!\n";
	}
	return 0;
}
