I ASSERT that you will enter the number 5!
Please enter the number 5 to prevent an exit: 5

Thank you for your cooperation!

**** ADDING TO THE INTEGER QUEUE 6 TIMES ****
front --> 1 <-- tail
front --> 1 :: 2 <-- tail
front --> 1 :: 2 :: 3 <-- tail
front --> 1 :: 2 :: 3 :: 4 <-- tail
front --> 1 :: 2 :: 3 :: 4 :: 5 <-- tail
Stack is FULL!!

**** TESTING COPY CONSTRUCTOR BY MAKING A COPY ****
Removing 1 from copy
Removing 2 from copy
Removing 3 from copy
Removing 4 from copy
Removing 5 from copy
Adding 10 to copy
Adding 11 to copy
Adding 12 to copy
Adding 13 to copy
Adding 14 to copy
**** Origional and Copy are unique! ****
Origional Queue: front --> 1 :: 2 :: 3 :: 4 :: 5 <-- tail
Copy of Queue: front --> 10 :: 11 :: 12 :: 13 :: 14 <-- tail

**** REMOVING FROM THE INTEGER QUEUE 4 TIMES ****
Removing 1 from the front
front --> 2 :: 3 :: 4 :: 5 <-- tail
Removing 2 from the front
front --> 3 :: 4 :: 5 <-- tail
Removing 3 from the front
front --> 4 :: 5 <-- tail
Removing 4 from the front
front --> 5 <-- tail

**** ADDING TO THE INTEGER QUEUE 5 TIMES ****
front --> 5 :: 7 <-- tail
front --> 5 :: 7 :: 8 <-- tail
front --> 5 :: 7 :: 8 :: 9 <-- tail
front --> 5 :: 7 :: 8 :: 9 :: 10 <-- tail
Stack is FULL!!

**** REMOVING FROM THE INTEGER QUEUE 6 TIMES ****
Removing 5 from the front
front --> 7 :: 8 :: 9 :: 10 <-- tail
Removing 7 from the front
front --> 8 :: 9 :: 10 <-- tail
Removing 8 from the front
front --> 9 :: 10 <-- tail
Removing 9 from the front
front --> 10 <-- tail
Removing 10 from the front
front -->  <-- tail
Stack is EMPTY!!

**** ADDING TO THE DOUBLE QUEUE 6 TIMES ****
front --> 1.10 <-- tail
front --> 1.10 :: 2.10 <-- tail
front --> 1.10 :: 2.10 :: 3.10 <-- tail
front --> 1.10 :: 2.10 :: 3.10 :: 4.10 <-- tail
front --> 1.10 :: 2.10 :: 3.10 :: 4.10 :: 5.10 <-- tail
Stack is FULL!!

**** REMOVING FROM THE DOUBLE QUEUE 4 TIMES ****
Removing 1.1 from the front
front --> 2.10 :: 3.10 :: 4.10 :: 5.10 <-- tail
Removing 2.1 from the front
front --> 3.10 :: 4.10 :: 5.10 <-- tail
Removing 3.1 from the front
front --> 4.10 :: 5.10 <-- tail
Removing 4.1 from the front
front --> 5.10 <-- tail

**** ADDING TO THE DOUBLE QUEUE 5 TIMES ****
front --> 5.10 :: 7.10 <-- tail
front --> 5.10 :: 7.10 :: 8.10 <-- tail
front --> 5.10 :: 7.10 :: 8.10 :: 9.10 <-- tail
front --> 5.10 :: 7.10 :: 8.10 :: 9.10 :: 10.10 <-- tail
Stack is FULL!!

**** REMOVING FROM THE DOUBLE QUEUE 6 TIMES ****
Removing 5.1 from the front
front --> 7.10 :: 8.10 :: 9.10 :: 10.10 <-- tail
Removing 7.1 from the front
front --> 8.10 :: 9.10 :: 10.10 <-- tail
Removing 8.1 from the front
front --> 9.10 :: 10.10 <-- tail
Removing 9.1 from the front
front --> 10.10 <-- tail
Removing 10.1 from the front
front -->  <-- tail
Stack is EMPTY!!

**** ADDING TO THE STRING QUEUE 6 TIMES ****
front --> one <-- tail
front --> one :: two <-- tail
front --> one :: two :: three <-- tail
front --> one :: two :: three :: four <-- tail
front --> one :: two :: three :: four :: five <-- tail
Stack is FULL!!

**** REMOVING FROM THE STRING QUEUE 4 TIMES ****
Removing one from the front
front --> two :: three :: four :: five <-- tail
Removing two from the front
front --> three :: four :: five <-- tail
Removing three from the front
front --> four :: five <-- tail
Removing four from the front
front --> five <-- tail

**** ADDING TO THE STRING QUEUE 5 TIMES ****
front --> five :: seven <-- tail
front --> five :: seven :: eight <-- tail
front --> five :: seven :: eight :: nine <-- tail
front --> five :: seven :: eight :: nine :: ten <-- tail
Stack is FULL!!

**** REMOVING FROM THE STRING QUEUE 6 TIMES ****
Removing five from the front
front --> seven :: eight :: nine :: ten <-- tail
Removing seven from the front
front --> eight :: nine :: ten <-- tail
Removing eight from the front
front --> nine :: ten <-- tail
Removing nine from the front
front --> ten <-- tail
Removing ten from the front
front -->  <-- tail
Stack is EMPTY!!
