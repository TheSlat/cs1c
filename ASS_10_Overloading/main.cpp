/***********************************************************************
 * AUTHOR		 : Ethan Slattery
 * ASSIGNMENT #10: Overloading
 * CLASS		 : CS1C
 * SECTION		 : MW 1930 - 2050
 * DUE DATE		 : 26OCT2015
 ***********************************************************************/
#include "header.h"
/***********************************************************************
 * ASSIGNMENT #10 - This program manages several classes of employees.
 * 				    And several operators are overloaded for use with the
 * 				    employee class
 ***********************************************************************/
int main()
{
	/* CONSTANTS FOR THE CLASS HEADER */
	const string ASS_NAME	= "Overloading & Friends";
	const char   ASS_TYPE	= 'l';
	const string ASS_NUM	= "10";
	const string PRO_NAME   = "Ethan Slattery";

	/* OUTPUT - CLASS HEADING */
	PrintHeaderOstream(cout, ASS_NAME, ASS_TYPE, ASS_NUM, PRO_NAME);

	/* VARIABLE DECLARATIONS */
	//Create Employees with NON-DEFAULT constructor
	Employee jimmy("Jimmy Fallon", 12345, "949-555-1234", 40,
				   'M', "Comedian", 100000.00, 8, 31, 2014);
	Employee stephen("Stephan Colbert", 12346, "310-555-5555", 51, 'M',
					 "Comedian", 70123, 5, 8, 2015);
	Employee james("James Corden", 87654, "703-703-1234", 37, 'M',
			       "Talk Show Host", 900000, 12, 25, 2014);

	//Create Employees with DEFAULT constructor
	Employee katie("Katie Couric", 77777, "203-555-6789", 58, 'F',
			  	   "News Reporter", 500500, 3, 1, 2005);

	//Create Programmer with NON-DEFAULT constructor
	Programmer sam("Sam Software", 54321, "819-123-4567", 21, 'M',
			   	   "Programmer", 223000.00, 12, 24, 2011, 5432122,
				   "Joe Boss", 4.0, true, false);

	//Create Programmers with DEFAULT constructor
	Programmer mary("Mary Coder", 6543222, "310-555-5555", 28, 'F',
			        "Programmer", 770123.00, 2, 8, 2010, 6543222, "Mary Leader",
			         7.0, true, true);

	//Create Architect with NON-DEFAULT constructor
	Architect alex("Alex Arch", 88888, "819-123-4444", 31, 'M',
			       "Architect", 323000.00, 12, 24, 2009, 5434222,
				   "Big Boss", 5.0, 4);

	cout << "/************************************************\n"
		 << " *  THE EMPLOYEES - PRINTED WITH OVERLOADED <<  *\n"
		 << " ************************************************/\n";
	cout << jimmy.PrintLine() << endl;
	cout << jimmy.PrintHeader() << endl;
	cout << jimmy.PrintLine() << endl;
	cout << jimmy << endl;
	cout << stephen << endl
	     << james << endl
	     << katie << endl
	     << sam << endl
	     << mary << endl
		 << alex << endl
	     << katie.PrintLine();

	cout << "\n\n *** INPUT A NEW EMPLOYEE WITH OVERLOADED >> ***\n";
	Employee newEmp;
	cin >> newEmp;
	cout << "\n\n *** SHOW THE NEW EMPLOYEE ***\n";
	cout << newEmp.PrintLine() << endl
		 << newEmp.PrintHeader() << endl
		 << newEmp.PrintLine() << endl;
	cout << newEmp << endl
		 << newEmp.PrintLine();

	cout << "\n\n/****************************************************\n"
		 << " *  PHONE NUMBER COMPARISON - USING FRIENDS AND ==  *\n"
		 << " ****************************************************/\n";
	cout << "Katie Curic's phone number changed to match Stephen Colbert's\n";
	stephen.SetPhone(katie.GetPhone());
	cout << jimmy.PrintLine() << endl;
	cout << jimmy.PrintHeader() << endl;
	cout << jimmy.PrintLine() << endl;
	cout << jimmy << endl;
	cout << stephen << endl
		 << james << endl
		 << katie << endl
		 << sam << endl
		 << mary << endl
		 << alex << endl
		 << newEmp << endl
		 << newEmp.PrintLine();

	cout << "\n Using the friend function " << stephen.GetName() << "'s and "
		 << mary.GetName() << "'s Phone numbers are "
		 << ( isPhoneSame(&stephen, &mary) ? "THE SAME!" : "DIFFERENT!" );

	cout << "\n Using the friend function " << stephen.GetName() << "'s and "
		 << katie.GetName() << "'s Phone numbers are "
		 << ( isPhoneSame(&stephen, &katie) ? "THE SAME!" : "DIFFERENT!" );

	cout << "\n Using the overloaded == " << stephen.GetName() << "'s and "
			 << katie.GetName() << "'s Phone numbers are "
			 << ( stephen==katie ? "THE SAME!" : "DIFFERENT!" );

	cout << "\n Using the overloaded == " << stephen.GetName() << "'s and "
				 << mary.GetName() << "'s Phone numbers are "
				 << ( stephen==mary ? "THE SAME!" : "DIFFERENT!" );

	cout << "\n\n/***********************************************\n"
			 << " *  ADD TO THE AGE - USING FRIENDS, +, and +=  *\n"
			 << " ***********************************************/\n";
	cout << jimmy.GetName() << " was " << jimmy.GetAge() << "..." << endl;
	cout << jimmy.GetName() << " is now " << jimmy.AddAge(3)
		 << "(used the member function)" << endl;

	jimmy = jimmy + 3;
	cout << jimmy.GetName() << " is now " << jimmy.GetAge()
		 << " (used the overloaded +)" << endl;

	jimmy += 3;
	cout << jimmy.GetName() << " is now " << jimmy.GetAge()
		 << " (used the overloaded +=)" << endl;

	return 0;
}
