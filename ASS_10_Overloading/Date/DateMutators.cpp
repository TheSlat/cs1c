/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Date.h"
/**********************************
 **          MUTATORS            **
 **********************************/

/*************************************************************************
 * Method SetDate: Class Date
 *------------------------------------------------------------------------
 *   Calls a Validation method and then sets a valid Date.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    month: month of the date
 *    day  : day of the date
 *    year : year of the date
 *
 *  POST-CONDITIONS:
 *    The Date is set to a valid date, otherwise return FALSE
 *************************************************************************/
bool Date::SetDate(unsigned short month, //month of the date
			       unsigned short day,         //day of the date
			       unsigned short year)        //year of the date
{
	bool success;
	success = false;
	//Will set the date if valid date has been entered.
	if(ValidateDate(month, day, year))
	{
		dateMonth = month;
		dateDay   = day;
		dateYear  = year;
		success   = true;
	}
	return success;
}

/*************************************************************************
 * Method SetDate: Class Date
 *------------------------------------------------------------------------
 *   Calls a Validation method and then sets a valid Date.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    month: month of the date
 *    day  : day of the date
 *    year : year of the date
 *
 *  POST-CONDITIONS:
 *    The Date is set to a valid date, otherwise return FALSE
 *************************************************************************/
bool Date::SetDate(string newDate)	//IN - Date as a string with MM/DD/YYYY
{
	bool success;
	success = false;
	unsigned short month;
	unsigned short day;
	unsigned short year;

	month = atoi(newDate.substr(0,2).c_str());
	day   = atoi(newDate.substr(3,2).c_str());
	year  = atoi(newDate.substr(6,4).c_str());

	//Will set the date if valid date has been entered.
	if(ValidateDate(month, day, year))
	{
		dateMonth = month;
		dateDay   = day;
		dateYear  = year;
		success   = true;
	}
	return success;
}

/*************************************************************************
 * Method SetToday: Class Date
 *------------------------------------------------------------------------
 *   Sets the date to today
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *		NONE
 *
 *  POST-CONDITIONS:
 *    The Date is set to a valid date, otherwise return FALSE
 *************************************************************************/
void Date::SetToday()
{
	time_t now;
	tm *currentTime;

	now = time(NULL);
	currentTime  = localtime(&now);
	dateYear  = 1900 + currentTime -> tm_year;
	dateMonth = currentTime->tm_mon + 1;
	dateDay   = currentTime->tm_mday;
}
