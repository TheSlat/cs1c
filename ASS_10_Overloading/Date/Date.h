/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#ifndef DATE_H_
#define DATE_H_

#include <string>
#include <ctime>
#include <sstream>
#include <iostream>
#include <stdlib.h>
using namespace std;

class Date
{
	public:
		/*  CONSTRUCTOR & DESTRUCTORS  */
		Date();
		Date(string newDate);
		Date(unsigned short month,
			 unsigned short day,
			 unsigned short year);
		~Date();

		/*  MUTATORS  */
		bool SetDate(unsigned short month,
					 unsigned short day,
					 unsigned short year);
		bool SetDate(string newDate);
		void SetToday();

		/*  ACCESSORS  */
		void GetDate(unsigned short &month,
					 unsigned short &day,
					 unsigned short &year) const;
		bool IsThisTheFuture(Date checkDate) const;
		bool IsThisThePast(Date checkDate) const;
		bool IsThisTheSame(Date checkDate) const;
		unsigned short GetYear()  const;
		unsigned short GetMonth() const;
		unsigned short GetDay()   const;

		//Displays in MM/DD/YYYY format
		string DisplayDate() const;

	private:
		/*  UTILITY METHODS  */
		unsigned short GetDaysInMonth(unsigned short month,
									  unsigned short year) const;
		bool IsLeapYear(unsigned short year) const;

		/*  VALIDATE METHODS  */
		//Validates the month
		bool ValidateMonth(unsigned short month) const;

		//checks if a day is valid taking leap years into account
		bool ValidateDay(unsigned short month,
						 unsigned short day,
						 unsigned short year) const;

		//validates that the year is between 1900 and the current year
		bool ValidateYear(unsigned short year) const;

		//Uses the other validate methods to validate a given date
		//checks against today!
		bool ValidateDate(unsigned short month,
						  unsigned short day,
						  unsigned short year) const;

		/*  ATTRIBUTES  */
		unsigned short dateMonth;
		unsigned short dateDay;
		unsigned short dateYear;
};
#endif /* DATE_H_ */
