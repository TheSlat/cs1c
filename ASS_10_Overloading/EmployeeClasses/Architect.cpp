/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Architect.h"
/******************************
 ** Constructor & Destructor **
 ******************************/
/*************************************************************************
 * Constructor: Architect
 *------------------------------------------------------------------------
 *   This constructor initiates deck object to a fresh deck of cards using the
 *   Initialize() method
 *************************************************************************/
Architect::Architect()
{
	SetID(0);
	SetAge(0);
	SetGender(OTHER);
	SetSalary(0.0);
	SetDepartment(0);
	SetPercentRaise(0.0);
	SetYearsExp(0);
}

/*************************************************************************
 * Constructor: Architect - NON DEFAULT CONSTRUCTOR
 *------------------------------------------------------------------------
 *   This constructor initiates deck object to a fresh deck of cards using the
 *   Initialize() method
 *************************************************************************/
Architect::Architect (string newName,
						int    newID,
						string newPhone,
						unsigned short newAge,
						char   newGender,
						string newJob,
						float  newSalary,
						unsigned short newMonth,
						unsigned short newDay,
						unsigned short newYear,
						unsigned int newDepartmentNum,
						string newSupervisorName,
						float  newPercentRaise,
						unsigned int newYearsExp)
{
	SetName(newName);
	SetID(newID);
	SetName(newName);
	SetID(newID);
	SetPhone(newPhone);
	SetAge(newAge);
	SetGender(newGender);
	SetJob(newJob);
	SetSalary(newSalary);
	SetHireDate(newMonth, newDay, newYear);
	SetDepartment(newDepartmentNum);
	SetSupervisor(newSupervisorName);
	SetPercentRaise(newPercentRaise);
	SetYearsExp(newYearsExp);
}

/*************************************************************************
 * Destructor: Architect
 *------------------------------------------------------------------------
 *   This destructor does nothing in particular
 *************************************************************************/
Architect::~Architect()
{
}

/**************
 ** MUTATORS **
 **************/
/*************************************************************************
 * Method SetDepartment: Class Architect
 *------------------------------------------------------------------------
 *   This method Sets the Architects department
 *************************************************************************/
void Architect::SetDepartment(unsigned int newDepartmentNum)//IN - dept number
{
	departmentNum = newDepartmentNum;
}

/*************************************************************************
 * Method SetSupervisor: Class Architect
 *------------------------------------------------------------------------
 *   This method Sets the Architects supervisor
 *************************************************************************/
void Architect::SetSupervisor(string NewSupervisorName)//IN - supervisor name
{
	supervisorName = NewSupervisorName;
}

/*************************************************************************
 * Method SetPercentRaise: Class Architect
 *------------------------------------------------------------------------
 *   This method Sets the Architects department
 *************************************************************************/
void Architect::SetPercentRaise(float newPercentRaise)
{
	percentRaise = newPercentRaise;
}

/*************************************************************************
 * Method SetCPPKnowledge: Class Architect
 *------------------------------------------------------------------------
 *   This method Sets the Architects Years of Experience
 *************************************************************************/
void Architect::SetYearsExp(unsigned int newYearsExp)
{
	yearsExp = newYearsExp;
}

/****************
 ** ACCCESSORS **
 ****************/
/*************************************************************************
 * Method GetDepartment: Class Architect
 *------------------------------------------------------------------------
 *   This method Gets the Architects department
 *************************************************************************/
unsigned int Architect::GetDepartment() const
{
	return departmentNum;
}

/*************************************************************************
 * Method GetSupervisor: Class Architect
 *------------------------------------------------------------------------
 *   This method Gets the Architects supervisor
 *************************************************************************/
string Architect::GetSupervisor() const
{
	return supervisorName;
}

/*************************************************************************
 * Method GetPercentRaise: Class Architect
 *------------------------------------------------------------------------
 *   This method Gets the Architects last raise
 *************************************************************************/
float Architect::GetPercentRaise() const
{
	return percentRaise;
}

/*************************************************************************
 * Method GetYearsExp: Class Architect
 *------------------------------------------------------------------------
 *   This method Gets the Architects years of experience
 *************************************************************************/
unsigned int Architect::GetYearsExp() const
{
	return yearsExp;
}

/*************************************************************************
 * Method PrintHeader: Class Architect
 *------------------------------------------------------------------------
 *   This method Print a header for employee info
 *------------------------------------------------------------------------
 *************************************************************************/
string Architect::PrintHeader() const
{
	ostringstream output;
	output << left
		   << Employee::PrintHeader()
		   << setw(DEPT_COL) << "DEPARTMENT" << "| "
		   << setw(SUP_COL) << "SUPERVISOR" << "| "
		   << setw(RAISE_COL) << "LAST RAISE" << "| "
		   << setw(YEARS_COL) << "YEARS EXP." << "| "
		   << right;
	return output.str();
}

/*************************************************************************
 * Method PrintLine: Class Architect
 *------------------------------------------------------------------------
 *   This method Print a line for use in print output
 *************************************************************************/
string Architect::PrintLine() const
{
	ostringstream output;
	output << Employee::PrintLine();
	output << setfill('-')
		   << setw(DEPT_COL + SUP_COL + RAISE_COL
				   + YEARS_COL + 8)
	       << "-";
	return output.str();
}

/*************************************************************************
 * Method Print: Class Architect
 *------------------------------------------------------------------------
 *   This method Print the employee info
 *************************************************************************/
string Architect::Print() const
{
	ostringstream output;
	output << Employee::Print();
	output << left
		   << setw(DEPT_COL)  << GetDepartment() << "| "
		   << setw(SUP_COL)   << GetSupervisor() << "| "
		   << setw(RAISE_COL-9) << setprecision(1) << fixed
		   		   	   	   	   	<< GetPercentRaise() << "%        | "
		   << setw(YEARS_COL) << GetYearsExp() << "| "
		   << right;
	return output.str();
}
