/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Employee.h"
/****************
 ** ACCCESSORS **
 ****************/
/*************************************************************************
 * Method GetName: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee name
 *************************************************************************/
string Employee::GetName() const
{
	return empName;
}

/*************************************************************************
 * Method GetID: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee ID
 *************************************************************************/
int Employee::GetID() const
{
	return empID;
}

/*************************************************************************
 * Method GetPhone: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee Phone Number
 *************************************************************************/
string Employee::GetPhone() const
{
	return phoneNum;
}

/*************************************************************************
 * Method GetAge: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee age
 *************************************************************************/
unsigned int Employee::GetAge() const
{
return empAge;
}

/*************************************************************************
 * Method GetGender: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee Gender
 *************************************************************************/
string Employee::GetGender() const
{
	string output;
	switch(empGender)
	{
		case MALE	: 	output = "Male";
						break;
		case FEMALE	:	output = "Female";
						break;
		case TRANS	:	output = "Trans";
						break;
		case OTHER	:	output = "Other";
	}
	return output;
}

/*************************************************************************
 * Method GetJob: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee job title
 *************************************************************************/
string Employee::GetJob() const
{
	return jobTitle;
}

/*************************************************************************
 * Method GetSalary: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee salary
 *************************************************************************/
float Employee::GetSalary() const
{
	return salary;
}

/*************************************************************************
 * Method GetHireDate: Class Employee
 *------------------------------------------------------------------------
 *   This method Gets the employee salary
 *************************************************************************/
Date Employee::GetHireDate() const
{
	return hireDate;
}

/*************************************************************************
 * Method PrintHeader: Class Employee
 *------------------------------------------------------------------------
 *   This method Print a header for employee info
 *************************************************************************/
string Employee::PrintHeader() const
{
	ostringstream output;
	output << left
		   << "| "
		   << setw(NAME_COL) << "NAME" << "| "
		   << setw(ID_COL) << "ID #" << "| "
		   << setw(PHONE_COL) << "PHONE NUMBER" << "| "
		   << setw(AGE_COL) << "AGE" << "| "
		   << setw(GENDER_COL) << "GENDER" << "| "
		   << setw(JOB_COL) << "JOB TITLE" << "| "
		   << setw(SAL_COL) << "SALARY" << "| "
		   << setw(DATE_COL) << "HIRE DATE" << "| "
		   << right;
	return output.str();
}

/*************************************************************************
 * Method PrintLine: Class Employee
 *------------------------------------------------------------------------
 *   This method Print a line for use in print output
 *************************************************************************/
string Employee::PrintLine() const
{
	ostringstream output;
	output << setfill('-')
		   << setw(NAME_COL + ID_COL + PHONE_COL
				   + AGE_COL + GENDER_COL + JOB_COL
				   + SAL_COL + DATE_COL + 17)
	       << "-";
	return output.str();
}

/*************************************************************************
 * Method Print: Class Employee
 *------------------------------------------------------------------------
 *   This method Print the employee info
 *************************************************************************/
string Employee::Print() const
{
	ostringstream output;
	output << left
		   << "| "
		   << setw(NAME_COL) << GetName() << "| "
		   << setw(ID_COL) << GetID() << "| "
		   << setw(PHONE_COL) << GetPhone() << "| "
		   << setw(AGE_COL) << GetAge() << "| "
		   << setw(GENDER_COL) << GetGender() << "| "
		   << setw(JOB_COL) << GetJob() << "| "
		   << "$" << setw(SAL_COL-1) << setprecision(2)
		   	   	  << fixed << GetSalary() << "| "
		   << setw(DATE_COL) << hireDate.DisplayDate() << "| "
		   << right;
	return output.str();
}
