/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #3: Employee Tracker
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 10SEP2015
 ***********************************************************************/
#include "Employee.h"

/// Overload the + operator for the employee class
Employee Employee::operator+(int add)
{
	Employee addEmployee = *this;
	addEmployee.SetAge(addEmployee.GetAge() + add);
	return addEmployee;
}

/// Overloads the += operator for the employee class
Employee Employee::operator+=(int add)
{
	*this = *this + add;
	return *this;
}

/// overloads the == operator for the employee class
bool Employee::operator==(const Employee &compareEmp)
{
	return this->GetPhone() == compareEmp.GetPhone();
}

/// friend function to the employee class to compare phone numbers
bool isPhoneSame(Employee *emp1, Employee *emp2)
{
	return emp1->GetPhone() == emp2->GetPhone();
}

/// Member method to add to the employee age
int Employee::AddAge(int num)
{
	empAge += num;
	return this->GetAge();
}

/// overloads the insertion operator for the employee class
ostream& operator<<(ostream& output, const Employee& outEmp)
{
	cout << outEmp.Print();
	return output;
}

/// overloads the extraction operator for the employee class
istream& operator>>(istream& input, Employee& inputEmp)
{
	char gender;
	string date;

	cout << "\nEnter Name: ";
	getline(input, inputEmp.empName);

	cout << "\nEnter Employee ID#: ";
	input >> inputEmp.empID;

	cout << "\nEnter age: ";
	input >> inputEmp.empAge;
	input.ignore(10000, '\n');

	cout << "\nEnter gender (M/F/T/O): ";
	input.get(gender);
	inputEmp.SetGender(gender);
	input.ignore(100000, '\n');

	cout << "\nEnter job title: ";
	getline(input, inputEmp.jobTitle);

	cout << "\nEnter phone number: ";
	getline(input, inputEmp.phoneNum);

	cout << "\nEnter Hire Date (MM/DD/YYY): ";
	getline(input, date);
	inputEmp.SetHireDate(date);

	cout << "\nEnter salary: ";
	input >> inputEmp.salary;
	input.ignore(10000, '\n');

	return input;
}
