/**
 * @file	GetChar2.cpp
 * @brief	function declaration for GetChar
 *
 * @author	Ethan Slattery
 * @date	12SEP2015
 */
#include "header.h"
/** @brief Gets one of two possible characters.
 *
 *  This function gets a valid char input out of two possible valid choices
 *
 *  @param [in] prompt The input prompt
 *  @param [in] valid1 First valid character input
 *  @param [in] valid2 Second valid character input
 *  @param [in] promptWidth Width of the prompt column, optional
 *  @returns The valid character, capitalized
 *  @bug     no known bugs at present
 */
char GetChar(string prompt, char valid1, char valid2, int promptWidth)
{
	bool invalid;	//CALC - true while input is invalid
	char input;		//IN   - character input by the user
	valid1 = toupper(valid1);
	valid2 = toupper(valid2);
	do
	{
		cout << setw(promptWidth) << prompt;
		cin.get(input);
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		input = toupper(input);

		if(input == valid1 || input == valid2)
		{
			invalid = false;
		}
		else
		{
			cout << "\n**** please input " << valid1 << " or "
										   << valid2 << " ****\n\n";
			invalid = true;
		}
	}while(invalid);
	return input;
}
