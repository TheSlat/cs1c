/**
 * @file	ReverseString.cpp
 * @brief	function declaration for ReverseString
 *
 * @author	Ethan Slattery
 * @date	12SEP2015
 */
#include "header.h"

/** @brief	Reverses string elements.
 *
 * 	Function to reverse a section of a given string based on the passed indexes
 *
 * 	@param [in] TARGET The string to work reversal magic on
 *  @param [in] start  the letter to start reversal at (starting from 1)
 *  @param [in] end    the letter to end reversal at (starting from 1)
 *  @param [in] count  the LCV for the recursive function, passed by reference
 * 	@returns Nothing
 *  @bug	 no known bugs at present
 */
void ReverseString(const string TARGET, unsigned int start, unsigned int end,
		           unsigned int &count)
{
	// OUT - Print the part of the string before the reversal
	while(count < start-1)
	{
		cout << TARGET[count];
		count++;
 	}

	// OUT - Print out the reversed part of the string RECURSIVE!
	if(count >= start-1 && count <= end-1)
	{
		cout << static_cast<char>(toupper(TARGET[(end-1)-(count-(start-1))]));
		count++;
		ReverseString(TARGET, start, end, count);
	}

	// OUT - Print the part of the string after the reversal
	while(count > end-1 && count < TARGET.length())
	{
		cout << TARGET[count];
		count++;
	}
}
