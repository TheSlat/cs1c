/**
 * @file	main.cpp
 * @brief	Assignment #4 - Recursion
 *
 * @author	Ethan Slattery
 * @date	16SEP2015
 */
#include "header.h"

/** @brief	Assignment #4 - Recursion
 *
 * 	This program uses a recursive function to reverse a specified portion
 * 	of the alphabet.
 *
 * 	@returns integer of 0 when complete
 *  @bug	 no known bugs at present
 *
 */
int main()
{
	//CONSTANT DECLARATION
	const string ALPHABET = "abcdefghijklmnopqrstuvwxyz"; /**< The Alphabet */

	//VARIABLE DECLARATION
	unsigned int count;
	unsigned int start;
	unsigned int end;
	bool         valid;

	PrintHeaderOstream(cout, "Recursion", 'A', "4", "Ethan Slattery");
	do
	{
		count = 0;
		cout << "\n**** PLEASE INPUT THE RANGE OF REVERSAL! ****\n";
		start = GetInput("Enter the index to begin reversal: ", 1, 26, 35);
		end   = GetInput("Enter the index to end reversal: ", start, 26, 35);

		cout << endl;
		ReverseString(ALPHABET, start, end, count);
		cout << endl << endl;

		valid = (GetChar("Again? (Y/N) ", 'Y', 'N')) == 'Y';
	}while(valid);
	return 0;
}
