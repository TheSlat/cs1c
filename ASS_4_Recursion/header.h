/**
 * @file	header.h
 * @brief	function prototypes and pre-processor directives for assignment 4
 *
 * 	This header contains all the function prototypes and pre-processor
 * 	directives necessary for the recursion assignment (assignment #4)
 *
 * @author	Ethan Slattery
 * @date	16SEP2015
 */
#ifndef HEADER_H_
#define HEADER_H_

/* PRE-PROCESSOR INCLUDES */
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string.h>
#include <ios>
#include <limits>
using namespace	std;

/* PrintHeaderOstream: Prints the class header */
void PrintHeaderOstream(ostream &output, string assName, char assType,
						string assNum, string progName);

/* GetInput: gets a valid integer in a range of numbers */
int GetInput(string prompt, int lower, int upper, int promptWidth = 0);

/* GetChar: Gets a valid char from two choices */
char GetChar(string prompt, char valid1, char valid2, int promptWidth = 0);

/* ReverseString: Prints a string with a specified range in reverse */
void ReverseString(const string  TARGET, unsigned int  start,
				   unsigned int  end, 	 unsigned int &count);

#endif /* HEADER_H_ */
