/**
 * @file	PrintHeaderOstream.cpp
 * @brief	function declaration for PrintHeaderOstream
 *
 * @author	Ethan Slattery
 * @date	12SEP2015
 */
#include "header.h"
/** @brief Prints class header to supplied stream
 *
 *  This function receives an ostream variable and returns the
 * 	appropriate header via reference through the ostream variable
 *
 * 	@param [out] output  	output stream to output the header
 *  @param [in]  assName 	Assignment Name
 *  @param [in]  assType	Type of assignment [L]ab or [A]ssignment
 *  @param [in]  assNum		Assignment number
 *  @param [in]  progName	Programmers name
 * 	@returns Valid integer input number
 * 	@bug		no known bugs at present
 */
void PrintHeaderOstream(ostream &output, string  assName, char assType,
					    string assNum, string progName)
{
    output << left;
    output << "*****************************************************";
    output << "\n*  PROGRAMMED BY  : " << progName;
    output << "\n*  " << setw(15) << "CLASS" << ": CS1C";
    output << "\n*  " << setw(15) << "SECTION" << ": MW: 1930 - 2050";
    output << "\n*  ";

    //	OUTPUT - If assignment type is 'L', output LAB #, anything else
    // 			 outputs "ASSIGNMENT #".  toupper() makes this case-insensitive.
    if (toupper(assType) == 'L')
    {
	    output << "LAB #" << setw(10);
    }
    else
    {
	    output << "ASSIGNMENT #" << setw(3);
    }
    output << assNum << ": " << assName;
    output << "\n*****************************************************\n\n";
    output << right;
}
