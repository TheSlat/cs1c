/**
 * @file	GetInput.cpp
 * @brief	function declaration for GetInput, integer version
 *
 * @author	Ethan Slattery
 * @date	12SEP2015
 */
#include "header.h"
/** @brief Integer input and validation
 *
 * 	This function gets an integer with an upper and lower limit.
 * 	The promptWidth parameter will set a width for the input prompt IF set to
 * 	an integer > 0.
 *
 * 	@param [in] prompt      The input prompt
 *  @param [in] lower       Lower limit of valid input (inclusive)
 *  @param [in] upper 	    Upper limit of valid input (inclusive)
 *  @param [in] promptWidth Width of the prompt column, optional
 * 	@returns Valid integer  input number
 * 	@bug     no known bugs at present
 */
int GetInput(string prompt, int lower, int upper, int promptWidth)
{
	int  errorWidth;		// CALC - Possible width of error message
	int  integer;			// IN   - User input
	bool invalid;			// CALC - invalid if input is outside range
	ostringstream error1;	// CALC - The first error message
	ostringstream error2;	// CALC - The second error message

	cout << left;
	do
	{
		//PROC - If a prompt width > 0 was passed in, it sets the prompt width
		if(promptWidth > 0)
		{
			cout << setw(promptWidth);
		}
		cout << prompt;

		if(!(cin >> integer))
		{
			cout << "\n**** Please input a NUMBER between "
				 << lower << " and " << upper << " ****\n";
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			invalid = true;
		}
		//PROC - Checks to make sure it's within bounds
		else if(integer < lower || integer > upper)
		{
			error1 << "The number " << integer << " is an invalid entry";
			error2 << "Please input a number between " << lower
													   << " and " << upper;

			//PROC - Sets the error width to the length of the longest string
			if(error1.str().length() > error2.str().length())
			{
				errorWidth = error1.str().length();
			}
			else
			{
				errorWidth = error2.str().length();
			}

			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << endl;
			cout << "**** "
				 << setw(errorWidth) << error1.str()
				 << " ****\n";
			cout << "**** "
				 << setw(errorWidth) << error2.str()
				 <<" ****\n\n";
			invalid = true;
		}
		else
		{
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			invalid = false;
		}
	}while(invalid);
	cout << right;

	return integer;
}
