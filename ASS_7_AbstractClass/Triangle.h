/**
 * @file	Triangle.h
 * @brief	Triangle class declaration
 *
 * @author	Ethan Slattery
 * @date	28SEP2015
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_
#include "shape.h"
#include <cmath>

class Triangle : public shape
{
	public:
		/* CONSTRUCTORS & DESTRUCTORS */
		 Triangle();
		 Triangle(float num1, float num2, float num3);
		~Triangle(){};

		/* VIRTUAL METHODS */
		float calcPerimeter();
		float calcArea();

		/* MUTATORS */
		void SetSides(float num1, float num2, float num3);

	private:
		float side1; ///< first side of triangle
		float side2; ///< second side of triangle
		float side3; ///< third side of triangle
};

#endif /* TRIANGLE_H_ */
