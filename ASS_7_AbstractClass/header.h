/**
 * @file	header.h
 * @brief	Assignment #6 - Abstract Class
 * @author	Ethan Slattery
 * @date	28SEP2015
 */

#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <iomanip>
#include <string>
#include "Triangle.h"
#include "Rectangle.h"
#include "shape.h"

/* PrintHeaderOstream: Prints the class header */
void PrintHeaderOstream(std::ostream &output, std::string assName, char assType,
						std::string assNum, std::string progName);

/* printPerimeter: Prints the shapes perimeter to stdout */
void printPerimeter(shape *shapeOut);

/* printArea: Prints the shapes area to stdout */
void printArea(shape *shapeOut);

#endif /* HEADER_H_ */
