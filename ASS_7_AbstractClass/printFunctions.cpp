/**
 * @file	PrintFunctions.cpp
 * @brief	Assignment #6 - Abstract Class
 *
 * @author	Ethan Slattery
 * @date	28SEP2015
 */
#include "header.h"

/**
 * @brief   Prints the parameter to stdout
 * @param   [in] shapeOut pointer to a class derived from shape
 * @returns Nothing
 */
void printPerimeter(shape *shapeOut)
{
	std::cout << "Perimeter: " << std::setprecision(2) << std::fixed
			  << shapeOut->calcPerimeter() << std::endl;
}

/**
 * @brief   Prints the area to stdout
 * @param   [in] shapeOut pointer to a class derived from shape
 * @returns Nothing
 */
void printArea(shape *shapeOut)
{
	std::cout<< "Area: " << std::setprecision(2) << std::fixed
			 << shapeOut->calcArea() << std::endl;
}
