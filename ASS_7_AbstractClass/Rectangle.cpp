/**
 * @file	Rectangle.h
 * @brief	Rectangle class definition
 *
 * @author	Ethan Slattery
 * @date	28SEP2015
 */
#include "Rectangle.h"

/* CREATES THE RECTANGLE OBJECT */
Rectangle::Rectangle()
{
	length = 0;
	width  = 0;
}

/* NON-DEFAULT CONSTRUCTOR FOR THE RECTANGLE */
Rectangle::Rectangle(float newLength, float newWidth)
{
	length = newLength;
	width  = newWidth;
}

/* VIRTUAL - CALCULATES PERIMETER - RETURNS FLOAT */
float Rectangle::calcPerimeter()
{
	return (2*length) + (2*width);
}

/* VIRTUAL - CALCULATES AREA - RETURNS FLOAT */
float Rectangle::calcArea()
{
	return length * width;
}

/* SETS VALUE OF THE RECTANGLE DIMENSIONS */
void Rectangle::SetSides(float newLength, float newWidth)
{
	length = newLength;
	width  = newWidth;
}
