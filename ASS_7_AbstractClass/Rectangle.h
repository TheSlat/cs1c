/**
 * @file	Rectangle.h
 * @brief	Rectangle class declaration
 * @author	Ethan Slattery
 * @date	28SEP2015
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_
#include "shape.h"

class Rectangle : public shape
{
	public:
		/* CONSTRUCTORS & DESTRUCTORS */
		 Rectangle();
		 Rectangle(float newLength, float newWidth);
		~Rectangle(){};

		/* VIRTUAL METHODS */
		float calcPerimeter();
		float calcArea();

		/* MUTATORS */
		void SetSides(float newLength, float newWidth);

	private:
		float length; ///< length of the rectangle
		float width;  ///< width of the rectangle
};

#endif /* RECTANGLE_H_ */
