/**
 * @file	shape.h
 * @brief	Abstract shape class declaration
 *
 * @author	Ethan Slattery
 * @date	28SEP2015
 */

#ifndef SHAPE_H_
#define SHAPE_H_

class shape
{
	public:
		/* CONSTRUCTOR & DESTRUCTOR */
		virtual ~shape(){};

		/* VIRTUAL METHODS */
		virtual float calcPerimeter() = 0;
		virtual float calcArea() = 0;
};

#endif /* SHAPE_H_ */
