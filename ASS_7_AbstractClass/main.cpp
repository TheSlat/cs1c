/**
 * @file	main.cpp
 * @brief	Assignment #6 - Abstract Class
 * @author	Ethan Slattery
 * @date	28SEP2015
 */
#include "header.h"
/** @brief	Assignment #6 - Abstract Class
 *
 * 	This program implements abstract classes and virtual functions
 *
 * 	@returns integer of 0 when complete
 *  @bug	 no known bugs at present
 */
int main()
{
	/* DECLARE VARIABLES */
	using namespace std;
	Rectangle *rect;
	Triangle  *tri;
	float length, width;
	float side1, side2, side3;

	PrintHeaderOstream(cout, "Abstract Class & Virtual Functions",
			                 'A', "4", "Ethan Slattery");

	rect = new Rectangle;
	tri  = new Triangle;

	/* GET THE RECTANGLE INFORMATION */
	cout << "RECTANGLE:\n----------\n";
	cout << "Enter LENGTH: ";
	cin  >> length;
	cout << "Enter WIDTH: ";
	cin  >> width;
	cout << endl;

	/* SET THE RECTANLGE DIMENSIONS & OUTPUT WITH FUNCTIONS */
	rect->SetSides(length, width);
	printArea(rect);
	printPerimeter(rect);

	/* GET THE TRIANGLE INFORMATION */
	cout << "\nTRIANGLE:\n----------\n";
	cout << "Enter THREE sides (separated by spaces): ";
	cin  >> side1 >> side2 >> side3;
	cout << endl;

	/* SET THE TRIANGLE DIMENSIONS & OUTPUT WITH FUNCTIONS */
	tri->SetSides(side1, side2, side3);
	printArea(tri);
	printPerimeter(tri);

	return 0;
}
