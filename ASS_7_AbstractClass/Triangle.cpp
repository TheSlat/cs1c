/**
 * @file	Triangle.h
 * @brief	Triangle class definition
 * @author	Ethan Slattery
 * @date	28SEP2015
 */
#include "Triangle.h"

/* INITIATE THE TRIANGLE OBJECT */
Triangle::Triangle()
{
	side1 = 0;
	side2 = 0;
	side3 = 0;
}

/* NON-DEFUALT CONSTRUCTOR FOR TRIANGLE */
Triangle::Triangle(float num1, float num2, float num3)
{
	side1 = num1;
	side2 = num2;
	side3 = num3;
}

/* VIRTUAL - CALCULATES PERIMETER - RETURNS FLOAT */
float Triangle::calcPerimeter()
{
	return side1 + side2 + side3;
}

/* VIRTUAL - CALCULATES AREA - RETURNS FLOAT */
float Triangle::calcArea()
{
	float p = Triangle::calcPerimeter()/2;
	return sqrt( p*(p-side1)*(p-side2)*(p-side3) );
}

/* SET THE LENGTHS OF THE TRIANGLE SIDES */
void Triangle::SetSides(float num1, float num2, float num3)
{
	side1 = num1;
	side2 = num2;
	side3 = num3;
}
