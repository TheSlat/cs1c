/**
 * @file	main.cpp
 * @brief	Assignment 15 - ASCII bits
 *
 * Print out the number of bits turned on for every capital letter,
 * every lowercase letter, and the ASCII numbers between 0 and 9.
 *
 * @author	Ethan Slattery
 * @date	30NOV2015
 */
#include<iostream>
#include<iomanip>
#include<string>
#include<sstream>
#include<bitset>
using namespace std;

int HowManyOn(char letter);
string Print(char letter);

int main()
{
	// OUTPUT VALUES FOR ASCII NUMBERS
	for(char ltr = '0'; ltr <= '9'; ltr++){
		cout << Print(ltr);
	}

	// OUTPUT VALUES FOR ASCII UPPER CASE
	for(char ltr = 'A'; ltr <= 'Z'; ltr++){
		cout << Print(ltr);
	}

	// OUTPUT VALUES FOR ASCII LOWER CASE
	for(char ltr = 'a'; ltr <= 'z'; ltr++){
		cout << Print(ltr);
	}
	return 0;
}

int HowManyOn(char letter){
	bitset<8> binary(letter);	// CALC : 8-bit binary of the letter passed in
	bitset<8> mask;				// CALC : 8-bit mask
	int count = 0;				// OUT  : counter variable

	for(unsigned int i = 0; i < binary.size(); i++){
		// Create a mask for the appropriate bit
		mask   = 1<<i;
		// add 1 to the counter if there is a bit in the current position
		count += ((mask&binary)>>i).to_ulong();
	}
	return count;
}

string Print(char letter){
	ostringstream out;	// OUT : output variable to return
	out << left
		<< setw(3)   << letter
		<< setw(4)   << hex << int(letter)
	    << setw(10)  << bitset<8>(letter)
	    << setw(3)   << HowManyOn(letter)
	    << endl;
	return out.str();
}
