/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #2: Playing Card Shuffle
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 31AUG2015
 ***********************************************************************/
#include "Deck.h"
/******************************
 ** Constructor & Destructor **
 ******************************/
/*************************************************************************
 * Constructor: Deck
 *------------------------------------------------------------------------
 *   This constructor initiates deck object to a fresh deck of cards using the
 *   Initialize() method
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    none
 *
 *  POST-CONDITIONS:
 *    none
 *************************************************************************/
Deck::Deck()
{
	deckSize = 52;
	Initialize();
}

/*************************************************************************
 * Destructor: Deck
 *------------------------------------------------------------------------
 *   This destructor does nothing in particular
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    none
 *
 *  POST-CONDITIONS:
 *    none
 *************************************************************************/
Deck::~Deck()
{
}

/**************
 ** MUTATORS **
 **************/
/*************************************************************************
 * Method Initialize: Class Deck
 *------------------------------------------------------------------------
 *   This method initiates deck object to a fresh deck of cards in the
 *   "new deck" state.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    None
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
void Deck::Initialize()
{
	for(int count = 0; count < deckSize; count++)
	{
		for(int suit = 1; suit <= 4; suit++)
		{
			for(int card = 1; card <= 13; card++)
			{
				thisDeck[count].value = card;
				thisDeck[count].suit = suit;
				count++;
			}
		}
	}
}

/*************************************************************************
 * Method Shuffle: Class Deck
 *------------------------------------------------------------------------
 *   This method performs a "perfect shuffle" on the deck object.
 *   	In a perfect shuffle, the deck is broken exactly in half and rearranged
 *   	so that the first card is followed by the 27th card, followed by the
 *   	second card, followed by the 28th card, and so on.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    Card struct must be defined
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
void Deck::Shuffle()
{
	Card tempLoCards[deckSize/2];	// PROC - to store bottom half for swapping
	Card tempHiCards[deckSize/2];	// PROC - to store top half for swapping
	int  mainARIndex;				// PROC - to loop through main AR
	int  tempARIndex;				// PROC - to loop through temp arrays

	for(int count = 0; count < (deckSize/2); count++)
	{
		tempLoCards[count] = thisDeck[count];
	}
	for(int count = (deckSize/2); count < deckSize; count++)
	{
		tempHiCards[count-(deckSize/2)] = thisDeck[count];
	}

	mainARIndex = 0;
	tempARIndex = 0;
	while(mainARIndex < deckSize && tempARIndex < (deckSize/2))
	{
		thisDeck[mainARIndex]   = tempLoCards[tempARIndex];
		mainARIndex++;
		thisDeck[mainARIndex] = tempHiCards[tempARIndex];
		mainARIndex++;
		tempARIndex++;
	}
}

/***************
 ** ACCESSORS **
 ***************/
/*************************************************************************
 * Method CompareDeck: Class Deck
 *------------------------------------------------------------------------
 *   This method compares this deck to another deck to see if they are the same.
 *   - Returns TRUE if the decks are the same.
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    Card struct must be defined
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
bool Deck::CompareDeck(Deck otherDeck) const	// other deck to compare against
{
	bool sameDeck;	// PROC & OUT - The bool value for loop and return
	int  count;		// PROC - Loop variable for cycling through the arrays
	sameDeck = true;
	count    = 0;

	while(sameDeck && count < deckSize)
	{
		sameDeck = (GetCardVal(count) == otherDeck.GetCardVal(count));
		count++;
	}
	return sameDeck;
}

/*************************************************************************
 * Method CompareDeck: Class Deck
 *------------------------------------------------------------------------
 *   This method grabs the card value from the specified index in the deck
 *   - Returns the card value and suit as a formatted string
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    Card struct must be defined
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
string Deck::GetCardVal(int cardNum) const	// IN - Card in the deck to return
{
	string output; // OUT - the return string for card value

	if(cardNum >= 0 && cardNum <= deckSize)
	{
		output = GetSym(thisDeck[cardNum]);
	}
	return output;
}

/*************************************************************************
 * Method PrintDeck: Class Deck
 *------------------------------------------------------------------------
 *   This method prints out the whole deck in order for displaying to user
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    Card struct must be defined
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
string Deck::PrintDeck() const
{
	ostringstream output;
	const short COL_NUM = 13;

	for(int card = 0; card < deckSize; card += COL_NUM)
	{
		for(int col = 0; col < COL_NUM; col++)
			output << "┌-----┐";
		output << endl;
		for(int col = 0; col < COL_NUM; col++)
			output << "│" << (thisDeck[card+col].value == 10 ?
					          GetSym(thisDeck[card+col]):
  	   	   	   	        	  GetSym(thisDeck[card+col])+" ") << "  │";
		output << endl;
		for(int col = 0; col < COL_NUM; col++)
			output << "│     │";
		output << endl;
		for(int col = 0; col < COL_NUM; col++)
			output << "│  " << (thisDeck[card+col].value == 10 ?
					            GetSym(thisDeck[card+col]):
								" "+GetSym(thisDeck[card+col])) << "│";
		output << endl;
		for(int col = 0; col < COL_NUM; col++)
			output << "└-----┘";
		output << endl;
	}
	return output.str();
}

/*************************************************************************
 * Method PrintCard: Class Deck
 *------------------------------------------------------------------------
 *   This method prints out a single card. Helper Function used in PrintDeck
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    Card struct must be defined
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
string Deck::PrintCard(Card outCard) const	// IN - the card to output
{
	ostringstream output;

	output << "┌─────┐\n"
		   << "│" << (outCard.value == 10 ? GetSym(outCard) :
				   	   	   	   	   	        GetSym(outCard)+" ") << "  │\n"
		   << "│     │\n"
		   << "│  " << (outCard.value == 10 ? GetSym(outCard) :
				                              " "+GetSym(outCard)) << "│\n"
		   << "└─────┘\n";

	return output.str();
}

/*************************************************************************
 * Method GetSym: Class Deck
 *------------------------------------------------------------------------
 *   This method prints out the card symbol, suit and value. A helper function.
 *   1 = ♠ (\u2660)		2 = ♦ (\u2666)		3 = ♣ (\u2663)		4 = ♥ (\u2665)
 *------------------------------------------------------------------------
 *  PRE-CONDITIONS:
 *    Card struct must be defined
 *
 *  POST-CONDITIONS:
 *    None
 *************************************************************************/
string Deck::GetSym(Card outCard) const	// IN - the card symbol to output
{
	ostringstream output;

	if(outCard.value <= 10)
	{
		if(outCard.value == 1)
			output << "A";
		else
			output << outCard.value;
	}
	else
	{
		switch(outCard.value)
		{
			case 11 : output << "J";
					  break;
			case 12 : output << "Q";
					  break;
			case 13 : output << "K";
					  break;
		}
	}

	switch(outCard.suit)
	{
	case 1	: output << "♠";
			  break;
	case 2	: output << "♦";
			  break;
	case 3	: output << "♣";
			  break;
	case 4	: output << "♥";
			  break;
	}
	return output.str();
}
