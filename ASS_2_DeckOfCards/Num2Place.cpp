/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #2: Playing Card Shuffle
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 31AUG2015
 ***********************************************************************/
#include "header.h"
/*************************************************************************
 * FUNCTION Num2Place
 *________________________________________________________________________
 *  This function receives a number and returns a formatted string of that
 *  number with the appropriate suffix attached (1st 2nd, 3rd, 4th.. etc)
 * 	- returns string of the number with suffix
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		StringStream included in header
 *
 * POST-CONDITIONS
 * 		This function will return the heading to the calling function
 * 		via reference through the ostream variable
 *************************************************************************/
string Num2Place(int num)	// IN - The number to format
{
	ostringstream output;
	output << num;

	switch(num)
	{
		case 0	: break;
		case 1	: output << "st";
				  break;
		case 2	: output << "nd";
						  break;
		case 3	: output << "rd";
						  break;
		default	: output << "th";
						  break;
	}
	return output.str();
}
