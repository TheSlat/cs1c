/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #2: Playing Card Shuffle
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 31AUG2015
 ***********************************************************************/

#ifndef DECK_H_
#define DECK_H_

//Preprocessor directives go here
#include <iostream>		//for cin/cout
#include <iomanip>		//for IO manipulators
#include <string>		//strings!
#include <sstream>		//string streams
using namespace std;

//Global Constants

//User Defined Types
struct Card
{
	unsigned short value;
	unsigned short suit;
};

class Deck
{
	public:
		/*******************************
		 ** CONSTRUCTOR & DESTRUCTORS **
		 *******************************/
		Deck();
		~Deck();

		/**************
		 ** MUTATORS **
		 **************/
		void Initialize();
		void Shuffle();

		/***************
		 ** ACCESSORS **
		 ***************/
		bool   CompareDeck(Deck otherDeck) const;
		string GetCardVal(int cardNum) const;
		string PrintDeck() const;

	protected:
		string PrintCard(Card outCard) const;
		string GetSym(Card outCard) const;

	private:
		int  deckSize;
		Card thisDeck[52];
};

#endif /* DECK_H_ */
