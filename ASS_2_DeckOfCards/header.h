/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #2: Playing Card Shuffle
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 31AUG2015
 ***********************************************************************/
#ifndef header_H_
#define header_H_

//Preprocessor directives go here
#include <iostream>		//for cin/cout
#include <iomanip>		//for IO manipulators
#include <string>		//strings!
#include <sstream>		//string stream
#include "Deck.h"
using namespace std;

//Global Constants

//User Defined Types

//Function Prototypes
/*************************************************************************
 * PrintHeaderString
 *  This function receives an ostream variable, assignment name, type,
 * 	number, and programmers name. The function then returns the
 * 	appropriate header via reference through the ostream variable.
 * 	- returns nothing
 *************************************************************************/
void PrintHeaderOstream(ostream &output,	// OUT - output stream
						string  assName,	// IN - Assignment Name
						char    assType,	// IN - assignment Type
			 	 	 						//     (Lab or Assignment)
						string  assNum,		// IN - assignment number
						string  progName);	// IN - Programmers Name

/*************************************************************************
 * Num2Place
 * 	This function receives a number and returns a formatted string of that
 * 	number with the appropriate suffix attached (1st 2nd, 3rd, 4th.. etc)
 * 	- returns string of the number with suffix
 *************************************************************************/
string Num2Place(int num);	// IN - The number to format

#endif /* header_H_ */
