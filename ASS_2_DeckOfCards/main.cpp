/***********************************************************************
 * AUTHOR		: Ethan Slattery
 * ASSIGNMENT #2: Playing Card Shuffle
 * CLASS		: CS1C
 * SECTION		: MW 1930 - 2050
 * DUE DATE		: 31AUG2015
 ***********************************************************************/
#include "header.h"
/***********************************************************************
 * ASSIGNMENT #2 - This program initializes a deck of cards in "new deck order"
 * 		and then performs a series of "perfect shuffles" on that deck. The
 * 		program runs until the deck is back in the "new deck order".
 * 		The program will print out the initial deck state, the deck after the
 * 		first shuffle and the final state. It will also print out the number of
 * 		shuffles required to get back to a new deck.
 * _____________________________________________________________________
 * INPUTS:
 * 		NONE
 *
 * OUTPUTS:
 * 		shuffleCount	: Tracks the number of shuffles
 ***********************************************************************/
int main()
{
/************************************************************************
 * CONSTANTS
 * -----------------------------------------------------------------
 * USED FOR CLASS HEADING - ALL WILL BE OUTPUT
 * -----------------------------------------------------------------
 * ASS_TYPE		: assignment type (lab or assignment) 'L' or 'A'
 * ASS_NUM		: Assignment Number
 * ASS_NAME		: Title of the Assignment
 * PROG_NAME	: The name of the programmers
 * AR_SIZE		: The size of the number list
 * SAVE_FILE	: The name of the save file, one set of numbers saved at a time
 *
 ************************************************************************/
	const string ASS_NAME	= "Playing Card Shuffle";
	const char   ASS_TYPE	= 'A';
	const string ASS_NUM	= "2";
	const string PRO_NAME   = "Ethan Slattery";

	//VARIABLE DECLARATIONS
	Deck myDeck;	// PROC - The working deck
	Deck refDeck;	// PROC - untouched deck to compare to
	unsigned int shuffleCount;	// PROC & OUT - Tracks number of shuffles

	//OUTPUT - Class Heading
	PrintHeaderOstream(cout, ASS_NAME, ASS_TYPE, ASS_NUM, PRO_NAME);

	cout << endl
		 <<	"**********************************************\n"
		 << "*          THE CARD SHUFFLING GAME!          *\n"
		 << "*         \"the house always wins...\"         *\n"
		 << "**********************************************\n";

	cout << "A new deck, fresh from the factory:" << endl;
	shuffleCount = 0;
	do
	{
		if(shuffleCount < 2)
			cout << myDeck.PrintDeck() << endl;
		shuffleCount++;
		cout << "Shuffling the deck for the " << Num2Place(shuffleCount)
		     << " time..." << endl << endl;
		myDeck.Shuffle();
	}while(!myDeck.CompareDeck(refDeck));

	cout << myDeck.PrintDeck() << endl;
	cout << "the deck is now back to the original state!" << endl << endl
	     << "The deck was Shuffled a total of " << shuffleCount << " times!";

	return 0;
}
