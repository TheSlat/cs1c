/**
 * @file	main.cpp
 * @brief	Assignment #5 header file *
 * @author	Ethan Slattery
 * @date	24SEP2015
 */

#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <iomanip>
#include <string>
#include "item.h"
#include "store.h"
using namespace std;

#endif /* HEADER_H_ */
