/**
 * @file	item.cpp
 * @brief	definitions for item class
 *
 * @author	Ethan Slattery
 * @date	24SEP2015
 */
#include "item.h"

/// Create an Item class
store::Item::Item()
{
	iName  = new string;
	iPrice = new float;
	iStock = new int;

	if(iName != NULL && iPrice != NULL && iStock != NULL)
	{
		*iName  = "NULL PRODUCT";
		*iPrice = 0.00;
		*iStock = 0;
	}
}

/**
 *  @brief	Non-standard constructor for Item Class
 * 	@param [in] newName  name of the new Item
 * 	@param [in] newPrice price of the new Item
 * 	@param [in] newStock how many of the new Item to place in stock
 */
store::Item::Item(string newName, float newPrice, int newStock)
{
	iName  = new string;
	iPrice = new float;
	iStock = new int;

	if(iName != NULL && iPrice != NULL && iStock != NULL)
	{
		*iName  = newName;
		*iPrice = newPrice;
		*iStock = newStock;
	}
}

/// Destroy the Item Class
store::Item::~Item()
{
	delete iName;
	delete iPrice;
	delete iStock;
}

/**
 *  @brief	Copy Constructr for Item Class
 * 	@param [in] otherItem the other Item object to copy from
 */
store::Item::Item(const Item& otherItem)
{
	iName  = new string;
	iPrice = new float;
	iStock = new int;

	if(iName != NULL && iPrice != NULL && iStock != NULL)
	{
		*iName  = otherItem.GetName();
		*iPrice = otherItem.GetPrice();
		*iStock = otherItem.GetStock();
	}
}

/**
 *  @brief	Assignment Operator for Item Class
 * 	@param [in] otherItem the other Item object to copy from
 * 	@returns the Item
 */
store::Item& store::Item::operator =(const Item& otherItem)
{
	iName  = new string;
	iPrice = new float;
	iStock = new int;

	if(iName != NULL && iPrice != NULL && iStock != NULL)
	{
		*iName  = otherItem.GetName();
		*iPrice = otherItem.GetPrice();
		*iStock = otherItem.GetStock();
	}
	return *this;
}

/**
 *  @brief	Sets the value of all members in Item class
 * 	@param [in] newName  name of the new Item
 * 	@param [in] newPrice price of the new Item
 * 	@param [in] newStock how many of the new Item to place in stock
 * 	@returns TRUE if successful (ready for future error testing)
 */
bool store::Item::SetValues(string newName, float newPrice, int newStock)
{
	*iName  = newName;
	*iPrice = newPrice;
	*iStock = newStock;
	return true;
}

/**
 *  @brief	Sets the value of Item name in the class
 * 	@param [in] newName  new Item name
 * 	@returns TRUE if successful (ready for future error testing)
 */
bool store::Item::SetName(string newName)
{
	*iName = newName;
	return true;
}

/**
 *  @brief	Sets the value of Item price in the class
 * 	@param [in] newPrice  new Item price
 * 	@returns TRUE if successful (ready for future error testing)
 */
bool store::Item::SetPrice(float newPrice)
{
	bool success = false;
	*iPrice = newPrice;
	success = true;
	return success;
}

/**
 *  @brief	reduces the number of Items in stock
 * 	@param [in] sold number of items to reduce inventory by
 * 	@returns new total number of Items in stock as an int
 */
int store::Item::ReduceStock(int sold)
{
	*iStock -= sold;
	return *iStock;
}

/**
 *  @brief	increases the number of Items in stock
 * 	@param [in] new number of items to increase inventory by
 * 	@returns new total number of Items in stock as an int
 */
int store::Item::AddStock(int added)
{
	*iStock += added;
	return *iStock;
}

/**
 *  @brief	 Gets the Item name
 * 	@returns name of the Item as a string
 */
std::string store::Item::GetName() const
{
	return *iName;
}

/**
 *  @brief	 Gets the Item Price
 * 	@returns price of the Item as a float
 */
float store::Item::GetPrice() const
{
	return *iPrice;
}

/**
 *  @brief	 Gets the number of Items in stock
 * 	@returns number of Items in stock as int
 */
int store::Item::GetStock() const
{
	return *iStock;
}

/**
 *  @brief	 Prints the formatted price
 * 	@returns the price of the Item as a formatted string with $
 */
std::string store::Item::PrintPrice() const
{
	std::ostringstream output;

	output << "$" << std::setprecision(2) << std::fixed << GetPrice();

	return output.str();
}

/**
 *  @brief	 Prints the formatted Item
 * 	@returns All Item attributes as a formatted string with set-widths
 */
std::string store::Item::PrintItem() const
{
	std::ostringstream output;

	output << std::setw(25) << std::left << GetName() << std::right
		   << std::setw(10) << PrintPrice()
		   << std::setw(5)  << GetStock();

	return output.str();
}
