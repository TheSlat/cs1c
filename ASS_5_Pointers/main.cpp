/**
 * @file	main.cpp
 * @brief	Assignment #5 - Pointers
 * @author	Ethan Slattery
 * @date	24SEP2015
 */
#include "header.h"
using store::Item;
using store::Store;
/** @brief	Assignment #5 - Pointers
 *
 *  The inventory of the store is displayed and the user can add items
 * 	by name and amount to a shopping cart. When checking out a receipt is
 * 	displayed along with the updated inventory.
 */
int main()
{
	// VARIABLES
	Store  *strList = NULL;
	string *itemName;
	int	   *numIn;

	//Create the store and load the inventory
	strList = new Store;
	strList->AddItem(Item("Nike Basketball Shoes", 145.99, 22));
	strList->AddItem(Item("Under Armour T-Shirt", 29.99, 33));
	strList->AddItem(Item("Brooks Running Shoes", 111.44, 11));
	strList->AddItem(Item("Asics Running Shoes", 165.88, 20));
	strList->AddItem(Item("Nike Shorts", 45.77, 77));

	do{
		//Display the stock of the store
		cout << strList->PrintStock() << endl;
		// INPUT - Get user input
		itemName = new string;
		numIn = new int;
		cout << "Enter an item to add to your cart (X to checkout): ";
		std::getline(cin, *itemName);

		if(*itemName != "X" && *itemName != "x"){
			cout << "How many would you like to buy? ";
			cin  >> *numIn;
			cin.ignore(1000, '\n');

			// PROCESSING - Add items to card and reduce inventory
			if(!strList->Add2Cart(*itemName, *numIn)){
				cout << "\n*** ITEM NOT FOUND PLEASE TRY AGAIN ***\n";
			}
		}
	}while(*itemName != "X" && *itemName != "x");

	// OUT - Receipt and final inventory
	cout << strList->PrintReceipt();
	cout << strList->PrintStock();
	return 0;
}
