/**
 * @file	store.cpp
 * @brief	definitions for store class
 * @author	Ethan Slattery
 * @date	24SEP2015
 */
#include "store.h"
using std::string;
using store::Item;

/// Create a Store class
store::Store::Store()
{
	inventory = NULL;
	cart = NULL;
	invSize  = 0;
	cartSize = 0;
	taxRate  = 0.0825;
}

/// Destroy the Store Class
store::Store::~Store()
{
	delete inventory;
	delete cart;
}

/**
 *  @brief	Add an item to the list
 * 	@param [in] NewItem The new Item Object to add to the list
 * 	@returns TRUE if successful
 */
bool store::Store::AddItem(Item newItem)
{
	bool success = false;
	Item *tempList = NULL;

	if(invSize > 0)
	{
		tempList  = inventory;
		inventory = new Item[invSize + 1];
		if(inventory != NULL)
		{
			for(int count = 0; count < invSize; count++)
			{
				inventory[count] = tempList[count];
			}
			inventory[invSize] = newItem;
			delete tempList;
			invSize++;
			success = true;
		}
	}
	else
	{
		inventory = new Item[invSize + 1];
		if(inventory != NULL)
		{
			inventory[invSize] = newItem;
			invSize++;
			success = true;
		}
	}
	return success;
}

/**
 *  @brief	Add an item to the shopping cart
 * 	@param [in] ItemName The name of the item to add to the shopping cart
 * 	@param [in] numChange the number of items to add to the cart
 * 	@returns TRUE if successful, FALSE if not found or other problem
 */
bool store::Store::Add2Cart(string ItemName, int numChange)
{
	int index = 0;
	bool success = false;
	Item *tempCart;

	while(index < invSize && inventory[index].GetName().compare(ItemName))
	{
		index++;
	}

	if(index < invSize)
	{
		if(cartSize > 0)
		{
			tempCart  = cart;
			cart = new Item[cartSize + 1];
			if(cart != NULL)
			{
				for(int count = 0; count < cartSize; count++)
				{
					cart[count] = tempCart[count];
				}
				cart[cartSize] = Item(ItemName, inventory[index].GetPrice(),
									  numChange);
				delete tempCart;
				cartSize++;
				success = true;
			}
		}
		else
		{
			cart = new Item[cartSize + 1];
			if(cart != NULL)
			{
				cart[cartSize] = Item(ItemName, inventory[index].GetPrice(),
									  numChange);
				cartSize++;
				success = true;
			}
		}

		if(success)
		{
			inventory[index].ReduceStock(numChange);
		}
	}
	return success;
}

/**
 *  @brief increase the stock of an item
 * 	@param [in] ItemName The name of the item to add to inventory
 * 	@param [in] numChange the number of items to add to inventory
 * 	@returns the new number of items in stock as an int
 */
int store::Store::StockItem(string ItemName, int numChange)
{
	int index;
	while(!inventory[index].GetName().compare(ItemName))
	{
		index++;
	}
	inventory[index].AddStock(numChange);

	return inventory[index].GetStock();
}

/**
 *  @brief   outputs the stock of all items in a table
 * 	@returns the total stock and value in a table as a formatted string
 */
std::string store::Store::PrintStock() const
{
	std::ostringstream output;
	float totalCost = 0.0;

	if(invSize > 0)
	{
		output << std::endl;
		output << "-= INVENTORY =-\n"
			   << "----------------------------------------\n"
			   << std::setw(25) << std::left << "ITEM NAME" << std::right
			   << std::setw(10) << "PRICE"
			   << std::setw(5)  << "QTY" << std::endl;
		output << "----------------------------------------\n";
		for(int count = 0; count < invSize; count++)
		{
			output << inventory[count].PrintItem() << std::endl;
			totalCost +=inventory[count].GetPrice() * inventory[count].GetStock();

		}
		output << "----------------------------------------\n";
		output << "TOTAL VALUE: $"<< std::setprecision(2)
								 << std::fixed << totalCost << std::endl;
	}
	else
	{
		output << "There is no inventory!!\n";
	}
	return output.str();
}

/**
 *  @brief   outputs the shopping cart in a table
 * 	@returns the shopping cart and price before and after tax in a string
 */
std::string store::Store::PrintReceipt() const
{
	std::ostringstream output;
	float totalCost = 0.0;

	if(cartSize > 0)
	{
		output << std::endl;
		output << "-= RECEIPT =-\n"
			   << "----------------------------------------\n";
		output << std::setw(25) << std::left << "ITEM NAME" << std::right
			   << std::setw(10) << "PRICE"
			   << std::setw(5)  << "QTY" << std::endl;
		output << "----------------------------------------\n";
		for(int count = 0; count < cartSize; count++)
		{
			output << cart[count].PrintItem() << std::endl;
			totalCost +=cart[count].GetPrice()*cart[count].GetStock();
		}
		output << "----------------------------------------\n";
		output << "TOTAL PRICE: $"<< std::setprecision(2)
								 << std::fixed << totalCost << std::endl;
		output << "TOTAL PRICE: $"<< std::setprecision(2)
								 << std::fixed << totalCost+(totalCost*taxRate)
								 << std::endl;
	}
	else
	{
		output << "Your cart is empty!" << std::endl
			   << "Please let an employee know if you" << std::endl
			   << "couldn't find what you need :)\n";
	}
	return output.str();
}
