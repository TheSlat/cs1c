/**
 * @file	item.h
 * @brief	declarations for item class
 * @author	Ethan Slattery
 * @date	24SEP2015
 */
#ifndef ITEM_H_
#define ITEM_H_
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>

namespace store
{
	using std::string;
	class Item
	{
		public:
			/* CONSTRUCTORS */
			Item();
			Item(string newName, float newPrice, int newStock);  // NON-STANDARD
 			Item(const Item& otherItem);  // COPY CONSTRUCTOR
			/* ASSIGNMENT OPERATOR */
			Item& operator=(const Item& otherItem);
			/* DESTRUCTOR */
			~Item();

			/* MUTATORS*/
			bool SetValues(string name, float price, int stock);
			bool SetName(string newName);
			bool SetPrice(float newPrice);
			int  ReduceStock(int sold);
			int  AddStock(int added);

			/* ACCCESSORS */
			string GetName() const;
			float  GetPrice() const;
			string PrintPrice() const;
			int    GetStock() const;
			string PrintItem() const;

		private:
			string *iName;	//< name of the item
			float  *iPrice; //< Price of the item
			int    *iStock; //< Number fo the item in stock
	};
}
#endif /* ITEM_H_ */
