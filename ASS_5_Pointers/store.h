/**
 * @file	store.h
 * @brief	declarations for store class
 * @author	Ethan Slattery
 * @date	24SEP2015
 */
#ifndef STORE_H_
#define STORE_H_
#include "item.h"

namespace store
{
	using std::string;

	class Store
	{
		public:
			/* CONSTRUCTORS & DESTRUCTOR*/
			Store();
			~Store();

			/* MUTATORS */
			bool AddItem(Item newItem);
			bool Add2Cart(string ItemName, int numChange);
			int  StockItem(string ItemName, int numChange);

			/* ACCESSORS */
			string PrintStock() const;
			string PrintReceipt() const;

		private:
			Item  *inventory;	//< list of the current inventory
			Item  *cart;		//< list of items in the cart
			int   invSize;		//< size of the inventory list
			int   cartSize;		//< size of the shopping cart
			float taxRate;		//< current tax rate
	};
}
#endif /* STORE_H_ */
