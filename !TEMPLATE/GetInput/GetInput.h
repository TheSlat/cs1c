/***********************************************************************
 * AUTHOR		: BEAK & PaperCaked
 * LAB #16		: Inheritance, Overloading, Redefining
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 21APR2015
 ***********************************************************************/
#ifndef GETINPUT_H_
#define GETINPUT_H_

#include <string>		//for strings
#include <sstream>		//for sstream output
#include <iomanip>		//for setw
#include <iostream>		//for setw
#include <ios>			//For Clearning Iput buffer
#include <limits>		//For Clearning Iput buffer

using namespace std;

/*************************************************************************
 * GetInput: Integer
 * 	This function gets an integer with an upper and lower limit.
 * 	The promptWidth parameter will set a width for the input prompt IF set to
 * 	an integer > 0.
 * 	- RETURNS: valid integer input number
 *************************************************************************/
int GetInput(string prompt,	  //IN - prompt
		     int promptWidth, //IN - Width of prompt column if > 0
		     int lower,		  //IN - lower limit of valid range (inclusive)
		     int upper);      //IN - upper limit of valid range (inclusive)

/*************************************************************************
 * GetInput: Double
 *   This function gets a double and checks to make sure it is within a
 *   lower and upper limit. The promptWidth parameter will set a width for
 *   the input prompt IF set to an integer > 0.
 *   - RETURNS: valid double input number
 *************************************************************************/
double GetInput(string prompt,	    //IN - prompt
		        int    promptWidth, //IN - Width of prompt column if > 0
		        double lower,		//IN - lowest valid number
		        double upper);		//IN - highest valid number

/*************************************************************************
 * GetInput: String
 * 	 This function receives a prompt and a width for the prompt. If the width
 * 	 is greater than 0 it will use setw() to format the prompt. The user will
 * 	 then enter a string input and that string will be returned to the calling
 * 	 function.
 * 	 - returns the user input as a string
 *************************************************************************/
string GetInput(string prompt,		//IN - prompt
		   	    int promptWidth); 	//IN - Width of prompt column if > 0

/*************************************************************************
 * Int2String
 * 	This function receives an integer number and converts it to a string
 * 	using the stringstream method.
 * 	- returns the number as a string
 *************************************************************************/
string Int2String(int number);		//IN - number to convert to a string

#endif /* GETINPUT_H_ */
