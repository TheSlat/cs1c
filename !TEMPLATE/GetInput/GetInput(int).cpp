/***********************************************************************
 * AUTHOR		: BEAK & PaperCaked
 * LAB #16		: Inheritance, Overloading, Redefining
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 21APR2015
 ***********************************************************************/
#include "GetInput.h"
/*************************************************************************
 * FUNCTION GetInput: integer
 *________________________________________________________________________
 * 	This function gets an integer with an upper and lower limit.
 * 	The promptWidth parameter will set a width for the input prompt IF set to
 * 	an integer > 0.
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		prompt		: The string prompt
 *		promptWidth	: Width of the prompt column, use a # <= 0 to ignore this
 * 		lower		: The lower limit (inclusive) as an int
 * 		upper		: The upper limit (inclusive) as an int
 *
 * POST-CONDITIONS
 * 		returns the valid integer input.
 *************************************************************************/
int GetInput(string prompt,	  //IN - prompt
		     int promptWidth, //IN - Width of prompt column if > 0
		     int lower,		  //IN - lower limit of valid range (inclusive)
		     int upper)		  //IN - upper limit of valid range (inclusive)
{
	int  errorWidth;		//CALC - Possible width of error message
	int  integer;			//IN   - User input
	bool invalid;			//CALC - invalid of user input is outside range
	ostringstream error1;			//CALC - The first error message
	ostringstream error2;			//CALC - The second error message

	cout << left;
	do
	{
		//PROC - If a prompt width > 0 was passed in, it sets the prompt width
		if(promptWidth > 0)
		{
			cout << setw(promptWidth);
		}
		cout << prompt;

		if(!(cin >> integer))
		{
			cout << "\n**** Please input a NUMBER between "
				 << lower << " and " << upper << " ****\n";
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			invalid = true;
		}
		//PROC - Checks to make sure it's within bounds
		else if(integer < lower || integer > upper)
		{
			error1 << "The number " << integer << " is an invalid entry";
			error2 << "Please input a number between " << lower
													   << " and " << upper;

			//PROC - Sets the error width to the length of the longest string
			if(error1.str().length() > error2.str().length())
			{
				errorWidth = error1.str().length();
			}
			else
			{
				errorWidth = error2.str().length();
			}

			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << endl;
			cout << "**** "
				 << setw(errorWidth) << error1.str()
				 << " ****\n";
			cout << "**** "
				 << setw(errorWidth) << error2.str()
				 <<" ****\n\n";
			invalid = true;
		}
		else
		{
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			invalid = false;
		}
	}while(invalid);
	cout << right;

	return integer;
}
