/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #X: xxxxx
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: DDMMMYYYY
 ***********************************************************************/
#include "header.h"
/***********************************************************************
 * Assignment #5 - This program allows the user to
 * _____________________________________________________________________
 * INPUTS:
 * 		xxxxxxxx		: xxxxxxxx
 *
 * OUTPUTS:
 * 		xxxxxxxx		: xxxxxxxx
 ***********************************************************************/

int main()
{
/************************************************************************
 * CONSTANTS
 * -----------------------------------------------------------------
 * USED FOR CLASS HEADING - ALL WILL BE OUTPUT
 * -----------------------------------------------------------------
 * ASS_TYPE		: assignment type (lab or assignment) 'L' or 'A'
 * ASS_NUM		: Assignment Number
 * ASS_NAME		: Title of the Assignment
 * PROG_NAME	: The name of the programmers
 *
 * -----------------------------------------------------------------
 * BOUNDS FOR INPUT FUNCTIONS
 * -----------------------------------------------------------------
 * MAX_YEAR		: Maxium year that is searchable
 * MIN_YEAR		: Minimum year that is searchable
 * MAX_RATE		: Maximum rating possible
 * MIN_RATE		: Minimum rating possible
 ************************************************************************/
	const string ASS_NAME	= "Searching linked lists";
	const char   ASS_TYPE	= 'a';
	const string ASS_NUM	= "5";
	const string PROG_NAME  = "BEAK";

	//VARIABLE DECLARATIONS

	//VARIABLE INITIALIZATIONS

	//OUTPUT - Class Heading
	PrintHeaderOstream(cout,ASS_NAME, ASS_TYPE, ASS_NUM, PROG_NAME);

/***************************************************************************
 * INPUT -
 ***************************************************************************/

/***************************************************************************
 * PROCESSING -
 ***************************************************************************/

/***************************************************************************
 * OUTPUT -
 ***************************************************************************/

	return 0;
}
