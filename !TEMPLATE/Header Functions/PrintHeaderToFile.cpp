/***********************************************************************
 * AUTHOR		  : BEAK
 * STUDENT ID	  : 381330
 * LAB #1 		  : Eclipse Lab
 * CLASS		  : CS1B
 * SECTION		  : TTh 0800
 * DUE DATE		  : 21JAN2015
 ***********************************************************************/
#include <iomanip>		// setw() and right/left
#include <string>		// strings
#include <fstream>		// to use I/O files
using namespace std;
/*************************************************************************
 * FUNCTION printHeaderToFile
 *________________________________________________________________________
 * This function receives an output file, assignment name, type, and number
 * 		as well as the programmers name(s) and then outputs the appropriate
 * 		class header to a file.
 * 		- returns nothing -> this will output the class heading
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		oFile	 : An output file stream by reference
 * 		assName  : Assignment Name has to be previously defined
 * 		assType  : Assignment Type has to be previously defined
 * 		assName  : Assignment Number has to be previously defined
 * 		progName : Programmer Name has to be previously defined
 * 		
 * POST-CONDITIONS
 * 		This function will output the class heading to the file given
 *************************************************************************/
void printHeaderToFile(ofstream &oFile,			// IN/OUT - output file
				 	   string assName,			// IN - Assignment Name
				 	   char   assType,			// IN - assignment Type
				 	 	 	 	 	 	 		//     (Lab or Assignment)
				 	   int    assNum,			// IN - assignment number
				 	   string progName)			// IN - Programmers Name
{
	oFile << left;
	oFile << "*****************************************************";
	oFile << "\n*  PROGRAMMED BY : " << progName;
	oFile << "\n*  " << setw(14) << "CLASS" << ": CS1B";
	oFile << "\n*  " << setw(14) << "SECTION" << ": TTh: 0800 - 1150";
	oFile << "\n*  ";
	if (toupper(assType) == 'L')
	{
		oFile << "LAB #" << setw(9);
	}
	else
	{
		oFile << "ASSIGNMENT #" << setw(2);
	}
	oFile << assNum << ": " << assName;
	oFile << "\n*****************************************************\n\n";
	oFile << right;
}
