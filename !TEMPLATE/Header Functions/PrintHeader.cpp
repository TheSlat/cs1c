/***********************************************************************
 * AUTHOR		  : BEAK
 * STUDENT ID	  : 381330
 * LAB #1 		  : Eclipse Lab
 * CLASS		  : CS1B
 * SECTION		  : TTh 0800
 * DUE DATE		  : 21JAN2015
 ***********************************************************************/
#include <iostream>		// cin, cout
#include <iomanip>		// setw() and right/left
#include <string>		// strings
using namespace std;
/*************************************************************************
 * FUNCTION printHeader
 *________________________________________________________________________
 * This function receives an assignment name, type, and number as well as 
 * 		the programmers name(s) and then outputs the appropriate class 
 * 		header.
 * 		- returns nothing -> this will output the class heading
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		assName  : Assignment Name has to be previously defined
 * 		assType  : Assignment Type has to be previously defined
 * 		assName  : Assignment Number has to be previously defined
 * 		progName : Programmer Name has to be previously defined
 * 		
 * POST-CONDITIONS
 * 		This function will output the class heading
 *************************************************************************/
void printHeader(string assName,			// IN - Assignment Name
				 char   assType,			// IN - assignment Type
				 	 	 	 	 	 	 	//     (Lab or Assignment)
				 int    assNum,				// IN - assignment number
				 string progName)			// IN - Programmers Name
{
	cout << left;
	cout << "*****************************************************";
	cout << "\n*  PROGRAMMED BY : " << progName;
	cout << "\n*  " << setw(14) << "CLASS" << ": CS1B";
	cout << "\n*  " << setw(14) << "SECTION" << ": MW: 0800 - 1120";
	cout << "\n*  ";
	if (toupper(assType) == 'L')
	{
		cout << "LAB #" << setw(9);
	}
	else
	{
		cout << "ASSIGNMENT #" << setw(2);
	}
	cout << assNum << ": " << assName;
	cout << "\n*****************************************************\n\n";
	cout << right;
}
