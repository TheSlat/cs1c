/***********************************************************************
 * AUTHOR		: BEAK & CingarBear
 * LAB #17		: Polymorphism
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 30APR2015
 ***********************************************************************/
#ifndef GETCHAR_H_
#define GETCHAR_H_

#include <string>		//for strings
#include <iomanip>		//for setw
#include <iostream>		//for setw
#include <ios>			//For Clearning Iput buffer
#include <limits>		//For Clearning Iput buffer

using namespace std;

/*************************************************************************
 * GetChar (2 valid characters)
 *   This function gets a valid char input when the possible inputs are
 *   2 characters
 *
 * RETURNS: the valid character, capitalized
 *************************************************************************/
char GetChar(string prompt,	  // IN - Input prompt
			 int promptWidth, // IN - Width of prompt column
			 char valid1,	  // IN - First valid character input
			 char valid2); 	  // IN - second valid character input

/*************************************************************************
 * GetChar (3 valid characters)
 *   This function gets a valid char input when the possible inputs are
 *   3 characters
 *
 * RETURNS: the valid character, capitalized
 *************************************************************************/
char GetChar(string prompt,	  // IN - Input prompt
			 int promptWidth, // IN - Width of prompt column
			 char valid1,	  // IN - First valid character input
			 char valid2, 	  // IN - second valid character input
			 char valid3);	  // IN - third valid character input

/*************************************************************************
 * GetChar (4 valid characters)
 *   This function gets a valid char input when the possible inputs are
 *   4 characters
 *
 * RETURNS: the valid character, capitalized
 *************************************************************************/
char GetChar(string prompt,	  // IN - Input prompt
			 int promptWidth, // IN - Width of prompt column
			 char valid1,	  // IN - First valid character input
			 char valid2, 	  // IN - second valid character input
			 char valid3,	  // IN - third valid character input
			 char valid4);	  // IN - fourth valid character input

/*************************************************************************
 * GetChar (5 valid characters)
 *   This function gets a valid char input when the possible inputs are
 *   5 characters
 *
 * RETURNS: the valid character, capitalized
 *************************************************************************/
char GetChar(string prompt,	  // IN - Input prompt
			 int promptWidth, // IN - Width of prompt column
			 char valid1,	  // IN - First valid character input
			 char valid2, 	  // IN - second valid character input
			 char valid3,	  // IN - third valid character input
			 char valid4,	  // IN - fourth valid character input
			 char valid5);	  // IN - fifth valid character input

#endif /* GETCHAR_H_ */
