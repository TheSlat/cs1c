/***********************************************************************
 * AUTHOR		: BEAK & CingarBear
 * LAB #17		: Polymorphism
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 30APR2015
 ***********************************************************************/
#include "GetChar.h"
/*************************************************************************
 * FUNCTION GetChar (4 valid characters)
 *________________________________________________________________________
 * 		This function gets a valid char input when the possible inputs are
 * 		4 characters
 *
 * RETURNS: the valid character, capitalized
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		prompt		: the input prompt as a string
 * 		promptWidth	: Width of the prompt column
 * 		valid1		: The first valid character
 * 		valid2		: The second valid character
 * 		valid3		: The third valid character
 * 		valid4		: The fourth valid character
 *
 * POST-CONDITIONS
 * 		returns a valid character to the calling function
 *************************************************************************/
char GetChar(string prompt,	  // IN - Input prompt
			 int promptWidth, // IN - Width of prompt column
			 char valid1,	  // IN - First valid character input
			 char valid2, 	  // IN - second valid character input
			 char valid3,	  // IN - third valid character input
			 char valid4)	  // IN - fourth valid character input
{
	bool invalid;	//CALC - true while input is invalid
	char input;		//IN   - character input by the user

	valid1 = toupper(valid1);
	valid2 = toupper(valid2);
	valid3 = toupper(valid3);
	valid4 = toupper(valid4);
	do
	{
		cout << setw(promptWidth) << prompt;
		cin.get(input);
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		input = toupper(input);

		if(input == valid1 || input == valid2 || input == valid3
						   || input == valid4)
		{
			invalid = false;
		}
		else
		{
			cout << "\n**** please input " 	<< valid1 << " or "
											<< valid2 << " or "
											<< valid3 << " or "
										    << valid4 << " ****\n\n";
			invalid = true;
		}
	}while(invalid);

	return input;
}
