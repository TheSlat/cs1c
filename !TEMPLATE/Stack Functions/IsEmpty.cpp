/***********************************************************************
 * AUTHOR		: BEAK & Flash Gordon
 * LAB #11		: Implementing a Stack
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 10MAR2015
 ***********************************************************************/
#include "LAB_11.h"
/*************************************************************************
 * FUNCTION IsEmpty
 *________________________________________________________________________
 * 	This function checks if the stack is empty and returns a true or false
 * 	- returns a bool value, True if the stack is empty
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the stack
 *
 * POST-CONDITIONS
 *	returns a bool value
 *************************************************************************/
bool IsEmpty(Person *head)
{
	return head == NULL;
}
