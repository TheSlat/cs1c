/***********************************************************************
 * AUTHOR		: BEAK & Flash Gordon
 * LAB #11		: Implementing a Stack
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 10MAR2015
 ***********************************************************************/
#include "LAB_11.h"
/*************************************************************************
 * FUNCTION Push
 *________________________________________________________________________
 * 	This function adds a person element to the top of the stack
 * 	- returns the head of the stack
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the stack
 *	*newPerson		: pointer to a person struct containing new node data
 *	struct person must be defined
 *
 * POST-CONDITIONS
 *	new person node is added to the top of the list.
 *************************************************************************/
Person* Push(Person *head, 		//IN - the head of the stack
			 Person *newPerson)	//IN - the new person node to be inserted
{
	newPerson->next = head;
	head = newPerson;
	newPerson = NULL;

	return head;
}
