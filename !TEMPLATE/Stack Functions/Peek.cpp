/***********************************************************************
 * AUTHOR		: BEAK & Flash Gordon
 * LAB #11		: Implementing a Stack
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 10MAR2015
 ***********************************************************************/
#include "LAB_11.h"
/*************************************************************************
 * FUNCTION Pop
 *________________________________________________________________________
 * 	This function adds a person element to the top of the stack
 * 	- returns the head of the stack
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the stack
 *	*newPerson		: pointer to a person struct containing new node data
 *	struct person must be defined
 *
 * POST-CONDITIONS
 *	new person node is added to the top of the list.
 *************************************************************************/
void Peek(Person *head)	//IN - the head fo the stack
{
	const int COL_WIDTH = 8;

	if(head != NULL)
	{
		cout << left;
		cout << setw(COL_WIDTH) << "Name:" << head->name << endl;
		cout << setw(COL_WIDTH) << "Gender:" << head->gender << endl;
		cout << setw(COL_WIDTH) << "Age:" << head->age;
		cout << right;
	}
	else
	{
		cout << "There is nobody to PEEK at!!";
	}
}
