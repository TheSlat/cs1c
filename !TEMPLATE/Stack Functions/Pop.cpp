/***********************************************************************
 * AUTHOR		: BEAK & Flash Gordon
 * LAB #11		: Implementing a Stack
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 10MAR2015
 ***********************************************************************/
#include "LAB_11.h"
/*************************************************************************
 * FUNCTION Pop
 *________________________________________________________________________
 * 	This function removes a person element to the top of the stack
 * 	- returns the head of the stack
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the stack
 *	struct person must be defined
 *
 * POST-CONDITIONS
 *	The top node on the stack is removed.
 *************************************************************************/
Person* Pop(Person *head)	//IN - the head of the stack
{
	//PROC - Checks to make sure the stack isn't empty
	if(head != NULL)
	{
		Person *perPtr;
		perPtr = head;
		head = head->next;
		delete perPtr;
		perPtr = NULL;
	}
	else
	{
		cout << "Can't POP from an empty stack!";
	}
	return head;
}
