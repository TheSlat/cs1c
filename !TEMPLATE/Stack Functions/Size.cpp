/***********************************************************************
 * AUTHOR		: BEAK & Flash Gordon
 * LAB #11		: Implementing a Stack
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 10MAR2015
 ***********************************************************************/
#include "LAB_11.h"
/*************************************************************************
 * FUNCTION Size
 *________________________________________________________________________
 * 	This function returns the number of nodes in the stack as an integer
 * 	- returns the stack size
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the stack
 *
 * POST-CONDITIONS
 *	returns the stack size
 *************************************************************************/
int Size(Person *head) 		//IN - the head of the stack
{
	Person *perPtr;	//CALC - for moving through he stack
	int     count;	//CALC - counter for counting the stack nodes
	count  = 0;
	perPtr = head;

	while(perPtr != NULL)
	{
		count++;
		perPtr = perPtr->next;
	}
	return count;
}
