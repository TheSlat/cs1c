/***********************************************************************
 * AUTHOR		: BEAK & Blue
 * LAB #12		: Implementing a Queue
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 12MAR2015
 ***********************************************************************/
#include "LAB_12.h"
/*************************************************************************
 * FUNCTION Enqueue
 *________________________________________________________________________
 * 	This function adds a person element to the end of the queue
 * 	- returns the head and tail by reference
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the stack
 *	*tail			: the tail of the queue
 *	*newPerson		: pointer to a person struct containing new node data
 *	struct person must be defined
 *
 * POST-CONDITIONS
 *	new person node is added to the end of the list.
 *************************************************************************/
void Enqueue(Person *&head, 	//IN - the head of the stack
			 Person *&tail,		//IN - the tail of the queue
			 Person *newPerson)	//IN - the new person node to be inserted
{
	if(!IsEmpty(head))
	{
		tail->next = newPerson;
		tail = newPerson;
		newPerson = NULL;
	}
	else
	{
		head = newPerson;
		tail = newPerson;
		newPerson =  NULL;
	}
}
