/***********************************************************************
 * AUTHOR		: BEAK & Blue
 * LAB #12		: Implementing a Queue
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 12MAR2015
 ***********************************************************************/
#include "LAB_12.h"
/*************************************************************************
 * FUNCTION Front
 *________________________________________________________________________
 * 	This function looks at an outputs the top element in the queue
 * 	- returns nothing
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the queue
 *	struct person must be defined
 *
 * POST-CONDITIONS
 *	displays the top element to the console
 *************************************************************************/
void Front(Person *head)	//IN - the head of the queue
{
	const int COL_WIDTH = 8;

	if(!IsEmpty(head))
	{
		cout << left;
		cout << setw(COL_WIDTH) << "Name:" << head->name << endl;
		cout << setw(COL_WIDTH) << "Gender:" << head->gender << endl;
		cout << setw(COL_WIDTH) << "Age:" << head->age;
		cout << right;
	}
	else
	{
		cout << "Nobody in FRONT, the queue is empty!!";
	}
}
