/***********************************************************************
 * AUTHOR		: BEAK & Blue
 * LAB #12		: Implementing a Queue
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 12MAR2015
 ***********************************************************************/
#include "LAB_12.h"
/*************************************************************************
 * FUNCTION ClearQueue
 *________________________________________________________________________
 * 	This function clears all members from a queue.
 * 	- returns the head and tail by reference
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the queue
 *	*tail			: pointer to the tail of the queue
 *	struct person must be defined
 *
 * POST-CONDITIONS
 *	returns the queue size
 *************************************************************************/
void ClearQueue(Person *&head, 		//IN - the head of the queue
				Person *&tail) 		//IN - the tail of the queue
{
	if(!IsEmpty(head))
	{
		cout << "CLEARING...\n";

		while(!IsEmpty(head))
		{
			cout << head -> name
				 << endl;
			Dequeue(head, tail);
		}

		cout << endl
			 << "The QUEUE has been CLEARED!"
			 << endl;
	}
	else
	{
		cout << "The QUEUE is ALREADY clear!"
			 << endl;
	}
}
