/***********************************************************************
 * AUTHOR		: BEAK & Blue
 * LAB #12		: Implementing a Queue
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 12MAR2015
 ***********************************************************************/
#include "LAB_12.h"
/*************************************************************************
 * FUNCTION Dequeue
 *________________________________________________________________________
 * 	This function removes a person element to the top of the queue
 * 	- returns the head and tail of the queue by reference
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the queue
 *	*tail			: pointer to the tail of the queue
 *	struct person must be defined
 *
 * POST-CONDITIONS
 *	The top node on the queue is removed.
 *************************************************************************/
void Dequeue(Person *&head,	//IN - the head of the queue
			 Person *&tail)	//IN - the tail of the queue
{
	//PROC - Checks to make sure the stack isn't empty
	if(!IsEmpty(head))
	{
		Person *perPtr;
		perPtr = head;
		head = head->next;
		delete perPtr;
		perPtr = NULL;
		if(head == NULL)
		{
			tail = NULL;
		}
	}
	else
	{
		cout << "Can't DEQUEUE from an empty list!";
	}
}
