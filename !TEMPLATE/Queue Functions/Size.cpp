/***********************************************************************
 * AUTHOR		: BEAK & Blue
 * LAB #12		: Implementing a Queue
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 12MAR2015
 ***********************************************************************/
#include "LAB_12.h"
/*************************************************************************
 * FUNCTION Size
 *________________________________________________________________________
 * 	This function returns the number of nodes in the queue as an integer
 * 	- returns the queue size
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the queue
 *
 * POST-CONDITIONS
 *	returns the queue size
 *************************************************************************/
int Size(Person *head) 		//IN - the head of the queue
{
	Person *perPtr;	//CALC - for moving through he queue
	int     count;	//CALC - counter for counting the queue nodes
	count  = 0;
	perPtr = head;

	while(perPtr != NULL)
	{
		count++;
		perPtr = perPtr->next;
	}
	return count;
}
