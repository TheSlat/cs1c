/***********************************************************************
 * AUTHOR		: BEAK & Blue
 * LAB #12		: Implementing a Queue
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 12MAR2015
 ***********************************************************************/
#include "LAB_12.h"
/*************************************************************************
 * FUNCTION IsEmpty
 *________________________________________________________________________
 * 	This function checks if the queue is empty and returns a true or false
 * 	- returns a bool value, True if the queue is empty
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	*head			: pointer to the top of the queue
 *
 * POST-CONDITIONS
 *	returns a bool value
 *************************************************************************/
bool IsEmpty(Person *head)
{
	return head == NULL;
}
