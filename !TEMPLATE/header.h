/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #X: xxxxx
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: DDMMMYYYY
 ***********************************************************************/
#ifndef header_H_
#define header_H_

//Preprocessor directives go here
#include <iostream>		//for cin/cout
#include <iomanip>		//for IO manipulators
#include <string>		//for strings
#include <fstream>		//for file use
#include <limits>		//limits
#include <ios>			//streamsize
#include <sstream>		//for sstream output
using namespace std;

//Global Constants

//User Defined Types

//Function Prototypes
/*************************************************************************
 * PrintHeaderString
 *  This function receives an ostream variable, assignment name, type,
 * 	number, and programmers name. The function then returns the
 * 	appropriate header via reference through the ostream variable.
 * 	- returns nothing
 *************************************************************************/
void PrintHeaderOstream(ostream &output,	// OUT - output stream
						string  assName,	// IN - Assignment Name
						char    assType,	// IN - assignment Type
			 	 	 						//     (Lab or Assignment)
						string  assNum,		// IN - assignment number
						string  progName);	// IN - Programmers Name

#endif /* header_H_ */
