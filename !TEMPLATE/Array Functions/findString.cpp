/***********************************************************************
 * AUTHOR		  : BEAK
 * STUDENT ID	  : 381330
 * ASSIGNMENT #10 : Parallel Arrays
 * CLASS		  : CS1B
 * SECTION		  : MW 0800
 * DUE DATE		  : 8DEC2014
 ***********************************************************************/
#include "ASS10_HEADER.h"
/*************************************************************************
 * FUNCTION findString
 *________________________________________________________________________
 * This function receives an array of strings, the size of that array as an
 * 		integer, and a string to search for. The function will search the
 * 		array and return the index of the string as an integer. If the
 * 		string is not found then the function will return LIST_SIZE.
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		WORDS[]  	: An array of strings
 * 		LIST_SIZE	: the size of the array as an integer
 * 		WORD		: the string to search for
 *
 * POST-CONDITIONS
 * 		This function will return the index of the first instance of the
 * 		search string.
 *************************************************************************/
int findString(const string WORDS[],	//IN - An array of strings to search
			   const int    LIST_SIZE,	//IN - Size of the search array
			         string word)		//IN - The string to search for
{
	int index;
	index = 0;
	word = stringToUpper(word);
	while(index < LIST_SIZE && stringToUpper(WORDS[index]) != word)
	{
		index++;
	}
	return index;
}
