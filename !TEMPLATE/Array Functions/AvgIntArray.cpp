/***********************************************************************
 * AUTHOR		  : BEAK
 * ASSIGNMENT #XX : xx
 * CLASS		  : CS1B
 * SECTION		  : TTH 0800
 * DUE DATE		  : 2015
 ***********************************************************************/
#include "_HEADER.h"
/*************************************************************************
 * FUNCTION AvgIntArray
 *________________________________________________________________________
 * 		This function receives an array of integers, the size of that array
 * 		as an integer, and the initialization value as an integer.
 * 		The function will then loop through and add up the values in the
 * 		the array until it seems the initialization value, and then return
 * 		the average of the values.
 *
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		INT_AR[]	: Array of ints, should be initialized to INIT_VALUE
 * 		AR_SIZE[]	: The size of INT_AR[] as an integer
 * 		INIT_VALUE	:
 *
 * POST-CONDITIONS
 * 		This function will return the average of the integers in the array.
 *************************************************************************/
float AvgIntArray(const int INT_AR[],	//IN - An array of integers
				  const int AR_SIZE,	//IN - Size of INT_AR[] array
				  const int INIT_VALUE) //IN - init value of INT_AR[]
{
	int index;
	int total;
	index = 0;
	total = 0;

	while(index < AR_SIZE && INT_AR[index] != INIT_VALUE)
	{
		total += INT_AR[index];
		index++;
	}
	return total / float(index);
}
