/***********************************************************************
 * AUTHOR		  : BEAK
 * STUDENT ID	  : 381330
 * ASSIGNMENT #10 : Parallel Arrays
 * CLASS		  : CS1B
 * SECTION		  : MW 0800
 * DUE DATE		  : 8DEC2014
 ***********************************************************************/
#include "ASS10_HEADER.h"
/*************************************************************************
 * FUNCTION fillArraysFromFile
 *________________________________________________________________________
 * This function receives a names array, and account number array, and a
 * 		balances array by reference along with a input file name and a size
 * 		for all the arrays. The function will read in the input file
 * 		filling the arrays with the proper information assuming the input
 * 		file is formatted with a name on its own line, then the account
 * 		number and balance on the next line separated by a space, and no
 * 		blank lines between entries.
 *
 * 		Jean Rousseau
 *		1001 15.50
 *		Steve Woolston
 *	 	1002 1423.20
 *		Michele Rousseau
 *			1005 52.75
 *
 *		etc...
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		inputFile		: The name of the input file as a string
 * 		nameArray		: The array of strings to store names in
 * 		accountArray	: The array of integers to store account #'s in
 * 		balanceArray	: The array of floats to store balances in
 * 		arraySize		: The size of the arrays as an integer
 * 		
 * POST-CONDITIONS
 * 		This function will fill the given arrays with account info from the
 * 		specified input file
 *************************************************************************/
void fillArraysFromFile(string inputFile,		// IN - The input file
						string nameArray[],		// IN/OUT - array for names
						int    accountArray[],	// IN/OUT - array for
												//          account numbers
						float  balanceArray[],	// IN/OUT - array for
												//          account balances
						int    arraySize)		// IN - size of the arrays
{
	int index;

	index = 0;
	ifstream inFile;
	inFile.open(inputFile.c_str());

	while(index < arraySize)
	{
		if(inFile)
		{
			getline(inFile, nameArray[index]);
			inFile >> accountArray[index];
			inFile >> balanceArray[index];
			inFile.ignore(1000, '\n');
		}
		else
		{
			nameArray[index] = "NO NAME";
			accountArray[index] = 0;
			balanceArray[index] = 0;
		}
		index++;
	}
	inFile.close();
}
