/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #5: Searching linked lists
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 19MAR2015
 ***********************************************************************/
#include "ASS_5.h"
/*************************************************************************
 * FUNCTION GetIOFile
 *________________________________________________________________________
 * 	This function displays the menu for the movie program, and validates the
 * 	user input to make sure a valid menu choice is entered.
 * 	- returns integer representing the menu choice to the calling function
 *________________________________________________________________________
 * PRE-CONDITIONS
 *	IOType			: A character representing the type of file, I or O
 *	defaultFile		: A name of a default file to use if user chooses
 *
 * POST-CONDITIONS
 *	returns filename as a string to the calling function
 *************************************************************************/
string GetIOFile(char IOType,			//IN - Type of IO file to get
				 string defaultFile)	//IN - default filename to use
{
	string input;		//IN - user filename input

	cout << "Which " << (toupper(IOType) == 'I' ? "input" : "output")
		 <<	" file would you like to use (type d for default file)? ";
	getline(cin, input);

	if(input == "d")
	{
		input = defaultFile;
	}

	return input;
}
