/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #5: Searching linked lists
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 19MAR2015
 ***********************************************************************/
#include "ASS_5.h"
/*************************************************************************
 * FUNCTION CenterStr
 *________________________________________________________________________
 * This function accepts a string and a width a field (setw(int)) as
 * 		an integer. The function then returns a string that is padded with
 * 		spaces to make the string centered in the given field width.
 * 		If the string length is odd, it will error to the right
 * 		- return -> a string padded with spaces on the left and right
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		text		: the text to be centered, must be a string
 * 		fieldWidth	: the width of the target field as an integer
 * 		
 * POST-CONDITIONS
 * 		This function will return a string padded with spaces left and right
 *************************************************************************/
string CenterStr(string text,		//IN - string to center
				 int fieldWidth)		//IN - width of target output
{
	int    length;		// CALC - length of the string to be centered
	int    position;	// CALC - the number of spaces to be padded
	string outString;	// OUT  - string for collecting the final return
	/*********************************************************************
	 * CALC - Calculates the number of spaces to pad with and pads both
	 *        sides of the string to make it centered in the desired width.
	 *********************************************************************/
	length = text.length();
	position = (fieldWidth - length)/2;
	for(int count=0; count < position; count++)
	{
		outString = outString + " ";
	}
	outString.append(text);
	for(int count=0; count < position; count++)
	{
		outString = outString + " ";
	}
	return outString;
}
