/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #5: Searching linked lists
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 19MAR2015
 ***********************************************************************/
#include "ASS_5.h"
/*************************************************************************
 * FUNCTION StringToUpper
 *________________________________________________________________________
 * This function receives a string and returns the same string
 * 		in all upper case characters.
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 		word	  : The string to convert
 * 		
 * POST-CONDITIONS
 * 		This function will return the original string in all upper case.
 *************************************************************************/
string StringToUpper(string word) //IN - String to convert to upper case
{
	string wordUpper;
	for(unsigned int index = 0; index < word.length(); index++)
	{
		wordUpper += (isalpha(word[index]) ? toupper(word[index])
										   : word[index]);
	}
	return wordUpper;
}
