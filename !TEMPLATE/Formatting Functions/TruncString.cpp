/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #5: Searching linked lists
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 19MAR2015
 ***********************************************************************/
#include "ASS_5.h"
/*************************************************************************
 * FUNCTION TruncString
 *________________________________________________________________________
 * 	This function takes a string and a field length, as an integer, and
 * 	checks to make sure the string fits in the given field. If not it will
 * 	truncate the string and add an ellipses on the end.
 * 	- returns the string, truncated if necessary
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 	text		: String that might needs truncating
 *	maxLength	: Field length where the string needs to fit
 *
 * POST-CONDITIONS
 * 	String is returned to calling function, truncated if necessary
 *************************************************************************/
string TruncString(string text,				//IN - string to modify
			       unsigned int maxLength)	//IN - field size of destination
{
	//CALC - if the string is longer than the field size, truncate and add
	//		 ellipses, otherwise do nothing
	if(text.length() > maxLength)
	{
		text = text.substr(0, maxLength-3) + "...";
	}

	//OUT - return the string, modified if necessary
	return text;
}
