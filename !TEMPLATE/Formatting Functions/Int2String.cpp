/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #5: Searching linked lists
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 19MAR2015
 ***********************************************************************/
#include "ASS_5.h"
/*************************************************************************
 * FUNCTION Int2String
 *________________________________________________________________________
 * 	This function receives an integer number and converts it to a string
 * 	using the stringstream method.
 * 	- returns the number as a string
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 	number		: an integer number to convert
 * 	SSTREAM must be included
 *
 * POST-CONDITIONS
 * 	number is returned as a string
 *************************************************************************/
string Int2String(int number)		//IN - number to convert to a string
{
	ostringstream ss;
	ss << number;
	return ss.str();
}
