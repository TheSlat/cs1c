/***********************************************************************
 * AUTHOR		: BEAK
 * ASSIGNMENT #5: Searching linked lists
 * CLASS		: CS1B
 * SECTION		: TTh 0800
 * DUE DATE		: 19MAR2015
 ***********************************************************************/
#include "ASS_5.h"
/*************************************************************************
 * FUNCTION WordWrap
 *________________________________________________________________________
 * 	This function takes a string and a field length (as an unsigned integer)
 * 	and checks to make sure the string fits in the given field word by word,
 * 	inserting newline characters between words as necessary.
 * 	- returns the string, wrapped to fit within the specified length
 *________________________________________________________________________
 * PRE-CONDITIONS
 * 	text		: the text that needs to be word wrapped
 * 	maxLength	: the field length that the text must fit in
 * 	SSTREAM must be included in pre-processors
 *
 * POST-CONDITIONS
 * 	Returns the string as multiple lines, each no wider than specified
 *************************************************************************/
string WordWrap(string text,			//IN - the text to wrap
			  	unsigned int maxLength)	//IN - the maximum size of a line
{
	string word;			//CALC - string to hold each word as it is parsed
	string line;			//CALC - string to hold each line as it is built
	ostringstream output;	//OUT  - stringstream to collect and output lines
	//Initialize strings for use
	line.clear();
	word.clear();

	//CALC - create lines word by word, and insert newlines as needed
	for(unsigned int count = 0; count < text.length(); count++)
	{
		if(!isspace(text[count]))
		{
			word += text[count];
		}
		else
		{
			//OUT - output the line to the stringstream once full
			if(line.length() + word.length() > maxLength)
			{
				output << line << endl;
				line.clear();
			}
			line += word + ' ';
			word.clear();
		}
	}
	//OUT - output the leftover lines once the primary loop is finished
	if(line.length() + word.length() > maxLength)
	{
		output << line << endl << word;
	}
	else
	{
		output << line << word;
	}
	return output.str();
}
