**** ADDING TO THE INTEGER QUEUE 6 TIMES ****
Adding 1
Adding 2
Adding 3
Adding 4
Adding 5
*** CAN'T ADD 6 THE QUEUE IS FULL ***
Adding 6

**** REMOVING FROM THE INTEGER QUEUE 4 TIMES ****
Removing 1 from the front
Removing 2 from the front
Removing 3 from the front
Removing 4 from the front

**** ADDING TO THE INTEGER QUEUE 5 TIMES ****
Adding 7
Adding 8
Adding 9
Adding 10
*** CAN'T ADD 11 THE QUEUE IS FULL ***
Adding 11

**** REMOVING FROM THE INTEGER QUEUE 3 TIMES ****
Removing 5 from the front
Removing 7 from the front
Removing 8 from the front
Removing 9 from the front

**** ADDING TO THE DOUBLE QUEUE 6 TIMES ****
Adding 1.1
Adding 1.2
Adding 1.3
Adding 1.4
Adding 1.5
*** CAN'T ADD 1.6 THE QUEUE IS FULL ***
Adding 1.6

**** REMOVING FROM THE DOUBLE QUEUE 4 TIMES ****
Removing 1.1 from the front
Removing 1.2 from the front
Removing 1.3 from the front
Removing 1.4 from the front

**** ADDING TO THE DOUBLE QUEUE 5 TIMES ****
Adding 1.7
Adding 1.8
Adding 1.9
Adding 2
*** CAN'T ADD 2.1 THE QUEUE IS FULL ***
Adding 2.1

**** REMOVING FROM THE DOUBLE QUEUE 3 TIMES ****
Removing 1.5 from the front
Removing 1.7 from the front
Removing 1.8 from the front
Removing 1.9 from the front

**** ADDING TO THE STRING QUEUE 6 TIMES ****
Adding one
Adding two
Adding three
Adding four
Adding five
*** CAN'T ADD six THE QUEUE IS FULL ***
Adding six

**** REMOVING FROM THE STRING QUEUE 4 TIMES ****
Removing one from the front
Removing two from the front
Removing three from the front
Removing four from the front

**** ADDING TO THE STRING QUEUE 5 TIMES ****
Adding seven
Adding eight
Adding nine
Adding ten
*** CAN'T ADD eleven THE QUEUE IS FULL ***
Adding eleven

**** REMOVING FROM THE STRING QUEUE 3 TIMES ****
Removing five from the front
Removing seven from the front
Removing eight from the front
Removing nine from the front
