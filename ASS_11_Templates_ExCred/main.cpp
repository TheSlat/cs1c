/**
 * @file	main.cpp
 * @brief	Assignment 11 - Templates Extra Credit
 *
 * Created a queue class using a template. Using the STD QUEUE container
 * for extra credit.
 *
 * @author	Ethan Slattery
 * @date	29SEP2015
 */
#include "queue.h"

int main()
{
	ASS11::MyQueue<int>    iQ;
	ASS11::MyQueue<double> dQ;
	ASS11::MyQueue<string> sQ;

	std::cout << "**** ADDING TO THE INTEGER QUEUE 6 TIMES ****\n";
	for(int i = 1; i <= 6; i++)
		std::cout << "Adding " << iQ.enqueue(i) << std::endl;

	std::cout << "\n**** REMOVING FROM THE INTEGER QUEUE 4 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
		std::cout << "Removing " << iQ.dequeue() << " from the front\n";

	std::cout << "\n**** ADDING TO THE INTEGER QUEUE 5 TIMES ****\n";
	for(int i = 7; i <= 11; i++)
		std::cout << "Adding " << iQ.enqueue(i) << std::endl;

	std::cout << "\n**** REMOVING FROM THE INTEGER QUEUE 3 TIMES ****\n";
	for(int i = 1; i <= 3; i++)
		std::cout << "Removing " << iQ.dequeue() << " from the front\n";

	std::cout << "\n**** ADDING TO THE DOUBLE QUEUE 6 TIMES ****\n";
	for(float i = 1.1; i < 1.7; i+=.1)
		std::cout << "Adding " << dQ.enqueue(i) << std::endl;

	std::cout << "\n**** REMOVING FROM THE DOUBLE QUEUE 4 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
		std::cout << "Removing " << dQ.dequeue() << " from the front\n";

	std::cout << "\n**** ADDING TO THE DOUBLE QUEUE 5 TIMES ****\n";
	for(float i = 1.7; i < 2.1; i+=.1)
		std::cout << "Adding " << dQ.enqueue(i) << std::endl;

	std::cout << "\n**** REMOVING FROM THE DOUBLE QUEUE 3 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
		std::cout << "Removing " << dQ.dequeue() << " from the front\n";

	std::cout << "\n**** ADDING TO THE STRING QUEUE 6 TIMES ****\n";
	std::cout << "Adding " << sQ.enqueue("one") << std::endl;
	std::cout << "Adding " << sQ.enqueue("two") << std::endl;
	std::cout << "Adding " << sQ.enqueue("three") << std::endl;
	std::cout << "Adding " << sQ.enqueue("four") << std::endl;
	std::cout << "Adding " << sQ.enqueue("five") << std::endl;
	std::cout << "Adding " << sQ.enqueue("six") << std::endl;

	std::cout << "\n**** REMOVING FROM THE STRING QUEUE 4 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
		std::cout << "Removing " << sQ.dequeue() << " from the front\n";

	std::cout << "\n**** ADDING TO THE STRING QUEUE 5 TIMES ****\n";
	std::cout << "Adding " << sQ.enqueue("seven") << std::endl;
	std::cout << "Adding " << sQ.enqueue("eight") << std::endl;
	std::cout << "Adding " << sQ.enqueue("nine") << std::endl;
	std::cout << "Adding " << sQ.enqueue("ten") << std::endl;
	std::cout << "Adding " << sQ.enqueue("eleven") << std::endl;

	std::cout << "\n**** REMOVING FROM THE STRING QUEUE 3 TIMES ****\n";
	for(int i = 1; i <= 3; i++)
		std::cout << "Removing " << sQ.dequeue() << " from the front\n";

	return 0;
}
