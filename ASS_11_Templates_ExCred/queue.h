/*
 *  Created on: Oct 28, 2015
 *      Author: Eslattery1
 */

#ifndef QUEUE_H_
#define QUEUE_H_
#include<iostream>
#include<iomanip>
#include<string>
#include<sstream>
#include<queue>
using std::string;

namespace ASS11
{

	template <class Type>
	class MyQueue
	{
		public:
			/* CONSTRUCTOR & DESTRUCTOR */
			MyQueue(int size = 5);
			~MyQueue();

			/* MUTATORS */
			Type enqueue(Type newObj);
			Type dequeue();

			/* ACCESSORS */
			Type   front() const;
			int    size() const;
			bool   isEmpty() const;
			bool   isFull() const;
		//string print() const;//STD-QUEUE does not allow access to elements!

		private:
			std::queue<Type> Q;
			int max;
	};

	/// The non-default constructor, creates a queue of specified size
	template <class Type>
	MyQueue<Type>::MyQueue(int size)
	{
		max = size;
	}

	/// The Destructor, does nothign in particular
	template <class Type>
	MyQueue<Type>::~MyQueue()
	{}

	/// The enqueue method, adds an item to the queue
	template <class Type>
	Type MyQueue<Type>::enqueue(Type newObj)
	{
		if(isFull())
			std::cout << "*** CAN'T ADD " << newObj
			          << " THE QUEUE IS FULL ***\n";
		else
			Q.push(newObj);
		return newObj;
	}

	/// The dequeue method, removes something from the queue and returns that item
	template <class Type>
	Type MyQueue<Type>::dequeue()
	{
		Type temp;
		if(isEmpty())
		{
			std::cout << "\n*** THE QUEUE IS EMPTY ***\n";
		}
		else
		{
			temp = front();
			Q.pop();
		}
		return temp;
	}

	/// Looks at the item in the front of the queue and returns that item
	template <class Type>
	Type MyQueue<Type>::front() const
	{
		return Q.front();
	}

	/// Returns the size of the queue
	template <class Type>
	int MyQueue<Type>::size() const
	{
		return Q.size();
	}

	/// checks to see if the queue is empty, TRUE if empty
	template <class Type>
	bool MyQueue<Type>::isEmpty() const
	{
		return Q.size() == 0;
	}

	/// checks if the queue is empty, TRUE if full
	template <class Type>
	bool MyQueue<Type>::isFull() const
	{
		return (Q.size() == max);
	}

} // END OF NAMESPACE ASS11
#endif /* QUEUE_H_ */
