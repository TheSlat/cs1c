// dynamic9.cpp
// client file
/*
 * Write a client to product the following output
 * output
 * 0 0
 * 1 10
 * 2 20
 * 3 30
 * 4 40
 * 5 50
 * 6 60
 * 7 70
 * 8 80
 * 9 90
 * copyfrom is called
 * (Note: print the new array)
 * 0 0
 * 1 10
 * 2 20
 * 3 30
 * 4 40
 * 5 50
 * 6 60
 * 7 70
 * 8 80
 * 9 90
 *
 * Store - invalid index : 11
 * ValueAt - invalid index : 11
 */

#include "dynamic9.h"
#include <iostream>
using namespace std;

int main()
{
	const int AR_SIZE = 10;
	DynArray myAR(AR_SIZE);
	DynArray copyAR(AR_SIZE);

	for(int count = 0; count < 10; count++)
	{
		myAR.Store(count*10, count);
	}

	cout << "Output:\n";
	for(int count = 0; count < 10; count++)
	{
		cout << count << " " << myAR.ValueAt(count) << endl;
	}

	copyAR.CopyFrom(myAR);

	cout << "\nOutput new array:\n";
	for(int count = 0; count < 10; count++)
	{
		cout << count << " " << copyAR.ValueAt(count) << endl;
	}

	copyAR.Store(110, 11);
	copyAR.ValueAt(11);

	return 0;
}

