/**
 *  Telemetry.h
 *  @brief  Class to collect and hold timing and access/comparison numbers
 *  @author Eslattery1
 *  @date   3DEC2015
 *  @notes  http://www.geisswerks.com/ryan/FAQS/timing.html
 */
#ifndef TELEMETRY_H_
#define TELEMETRY_H_

#include <iomanip>
#include <iostream>
#include <windows.h>	// gross!

using std::cout;
using std::endl;

/// class for returning the sort performance data
/// From gist.github.com/gongzhitaao/7062087
/// stackoverflow.com/questions/275004/
class Telemetry{
public:
    Telemetry();

    /* Mutators */
	/** Resets the start time
	 * if passed a bool equal to true it disables future resets
	 */
    void      Reset(bool recursive = false);
	/// Re-enables the reset method
    void      ReEnableReset() {disableReset = false;}
	/// returns the time elapsed since the last reset w/o stopping
    long long Elapsed() const;
	/// returns the time elapsed since the last reset and stops time tracking.
    long long RecordTime();
	/// increments the number of comparisons by the number passed (default 1)
    void      Compare(int num = 1);
	/// increments the number of assignments by the number passed (default 1)
    void      Assign (int num = 1);

    /* Accessors */
	/// Gets the number of comparisons
    unsigned long GetCompares();
	/// Gets the number of swap assignments
    unsigned long GetAssigns();
	/// Gets the number of seconds recorded
    double        GetSeconds();

private:

    LARGE_INTEGER freq;		    //< counter frequency in Hz
    LARGE_INTEGER startPoint;	//< counter value of start point
    long long     elapsed;		//< ticks from start to end point
    unsigned long comparisons;	//< counter for number of comparison operations
    unsigned long assignments;	//< counter for number of assignment operations
    bool          disableReset; //< disables resting time for use in recursive functions
};

#endif /* TELEMETRY_H_ */
