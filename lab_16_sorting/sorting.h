/**
 * @file	sorting.h
 * @brief	Assignment 16 - Sorting
 * @author	Ethan Slattery
 * @date	02DEC2015
 */
#ifndef LAB_16_SORTING_SORTING_H
#define LAB_16_SORTING_SORTING_H

#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>	// shuffle for random list
#include <math.h>       // log10
#include <stdlib.h>     // SRAND and RAND
#include <time.h>       // TIME
#include "Telemetry.h"
using namespace std;

/// Function to load the arrays with the initial values
void FillArrays(int ar1[], int ar2[], int ar3[], const int AR_SIZE);

/// Prints the array to STDOUT according to assignment guidelines
string PrintAR(int *ar, int numOut, const int AR_SIZE);

/// Sorts the array using the bubble sort
void BubbleSort(int* from, int* to, Telemetry *SortData);

/// Sorts the Array using the insertion sort
void InsertionSort(int* from, int* to, Telemetry *SortData);

/// Sorts the array using the Selection Sort
void SelectionSort(int* from, int* to, Telemetry *SortData);

/// Sorts the array using the Quick Sort
void QuickSort(int* from, int* to, Telemetry *SortData);

/// Sorts the array using a Merge sort algorithm
void mergeSort(int arr[], int l, int r, Telemetry *SortData);

/// Helper function for the merge sort that merges arrays together
void merge(int arr[], int l, int m, int r, Telemetry *SortData);

#endif //LAB_16_SORTING_SORTING_H

