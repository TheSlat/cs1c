/**
 * @file	main.cpp
 * @brief	Assignment 16 - Sorting
 * @author	Ethan Slattery
 * @date	02DEC2015
 *
 * Sort three arrays (list1, list 2, and list3) five times.
 *
 * list1 = {1, 2, 3, ...,98, 99, 100}
 * list2 = {100, 99, 98, ..., 3, 2, 1}
 * list3 = {100, 5, 92, ..., 98, 2, 88}
 *
 * Sort ascending (bubble, insertion, selection,
 * quick (use a random number between 1 and 100 as the pivot),
 * merge the arrays where
 * 1.	list1 is initially in ascending order
 * 2.	list2 is initially in descending order
 * 3.	list3 is initially in random order
 *
 * a.	Print out the first and last 10 array entries of list1, list2,
 *      and list3 before sorting.  Also print out the first and last 10 array
 *      entries after each sort (bubble, insertion, selection, quick, merge).
 *
 * b.	Output the number of comparisons and item assignments made during each
 * 	    sort.  There should be 15 entries.
 *
 * c.	Output the execution time in nanoseconds for each sort. There should
 *      be 15 entries.
 */
#include "sorting.h"

int main()
{
	/** @brief Refrence material for my own future reference
	 * Problems with large arrays due to stack overflows, description:
	 * stackoverflow.com/questions/1847789/
	 * http://stackoverflow.com/questions/79923/
	 */
	const int AR_SIZE   = 1000;     //< CALC - Size of the array to sort
	const int DISP_SIZE = 10;       //< CALC - # of elements to show start/end
	int *ar1 = new int[AR_SIZE];    //< Array of integers in order
	int *ar2 = new int[AR_SIZE];    //< Array of integers in reverse order
	int *ar3 = new int[AR_SIZE];    //< Array of randomly ordered unique values

    // Typedef of sort function signature
    typedef void (*sortFunctions) (int* from, int* to, Telemetry *SortData);

    // Array of sort functions minus the Merge sort that didn't play nice
    sortFunctions Sorts[] = { BubbleSort, SelectionSort, InsertionSort, QuickSort, };

    // Parallel array of names for each sort in the sort array
    string SortNames[]    = { "BUBBLE", "SELECTION", "INSERTION", "QUICK", "MERGE" };

    // Array to hold the telemetry data for each sort
    Telemetry arOut[5][3];

    srand(time(NULL));

    /***************************************************************************
     * ITERATE THROUGH THE SORTS IN THE SORT ARRAY
     ***************************************************************************/
	for(int i = 0; i <= sizeof(Sorts)/sizeof(Telemetry*); i++) {
        // Initialize the array to initial testing values
        FillArrays(ar1, ar2, ar3, AR_SIZE);

        cout << "***************************************************************\n"
        << "* ARRAYS BEFORE " << SortNames[i] << " SORT\n"
        << "***************************************************************\n";
        cout << PrintAR(ar1, DISP_SIZE, AR_SIZE);
        cout << PrintAR(ar2, DISP_SIZE, AR_SIZE);
        cout << PrintAR(ar3, DISP_SIZE, AR_SIZE) << endl;

        if(i < sizeof(Sorts)/sizeof(Telemetry*)) {
            // Perform the Sorting!
            Sorts[i](ar1, ar1 + (AR_SIZE - 1), &arOut[i][0]);
            Sorts[i](ar2, ar2 + (AR_SIZE - 1), &arOut[i][1]);
            Sorts[i](ar3, ar3 + (AR_SIZE - 1), &arOut[i][2]);
        }
        else {
            mergeSort(ar1, 0, AR_SIZE-1, &arOut[i][0]);
            mergeSort(ar2, 0, AR_SIZE-1, &arOut[i][1]);
            mergeSort(ar3, 0, AR_SIZE-1, &arOut[i][2]);
        }

        cout << "***************************************************************\n"
        << "* ARRAYS AFTER " << SortNames[i] << " SORT\n"
        << "***************************************************************\n";
        cout << PrintAR(ar1, DISP_SIZE, AR_SIZE);
        cout << PrintAR(ar2, DISP_SIZE, AR_SIZE);
        cout << PrintAR(ar3, DISP_SIZE, AR_SIZE) << endl;
    }


    /***************************************************************************
     * OUTPUT THE RESULTS OF THE SORT TESTS
     ***************************************************************************/
    for(int i = 0; i <= sizeof(Sorts)/sizeof(Telemetry *); i++) {
        if(i==0){
            cout << "\n\t\t*****************************************\n"
                 << "\t\t*****     TIME COMPARISONS (uS)     *****\n"
                 << "\t\t*****************************************\n"
                 << setw(12) << "" << setw(20) << "ORDERED ARRAY" << setw(20)
                 << "REVERSED ARRAY" << setw(20) << "RANDOM ARRAY" << endl;
        }
        cout << setw(12) << SortNames[i] << setprecision(3) << fixed
             << setw(20) << arOut[i][0].GetSeconds()*1000000
             << setw(20) << arOut[i][1].GetSeconds()*1000000
             << setw(20) << arOut[i][2].GetSeconds()*1000000 << endl;
    }


    for(int i = 0; i <= sizeof(Sorts)/sizeof(Telemetry *); i++) {
        if(i==0){
            cout << "\n\t\t**********************************************\n"
                 << "\t\t*****       NUMBER OF ASSIGNMENTS        *****\n"
                 << "\t\t**********************************************\n"
                 << setw(12) << "" << setw(20) << "ORDERED ARRAY" << setw(20)
                 << "REVERSED ARRAY" << setw(20) << "RANDOM ARRAY" << endl;
        }
        cout << setw(12) << SortNames[i]
        << setw(20) << arOut[i][0].GetAssigns()
        << setw(20) << arOut[i][1].GetAssigns()
        << setw(20) << arOut[i][2].GetAssigns() << endl;
    }


    for(int i = 0; i <= sizeof(Sorts)/sizeof(Telemetry *); i++) {
        if(i==0){
            cout << "\n\t\t*********************************************\n"
                 << "\t\t*****       NUMBER OF COMPARISONS       *****\n"
                 << "\t\t*********************************************\n"
                 << setw(12) << "" << setw(20) << "ORDERED ARRAY" << setw(20)
                 << "REVERSED ARRAY" << setw(20) << "RANDOM ARRAY" << endl;
        }
        cout << setw(12) << SortNames[i]
        << setw(20) << arOut[i][0].GetCompares()
        << setw(20) << arOut[i][1].GetCompares()
        << setw(20) << arOut[i][2].GetCompares() << endl;
    }
    return 0;
}
