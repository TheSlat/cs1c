/**
 *  Telemetry.cpp
 *  @brief  Class to collect and hold timing and access/comparison numbers
 *  @author Eslattery1
 *  @date   3DEC2015
 *  @notes  http://www.geisswerks.com/ryan/FAQS/timing.html
 */
#include "Telemetry.h"

Telemetry::Telemetry() {
	elapsed      = 0;
	comparisons  = 0;
	assignments  = 0;
	disableReset = false;

    if(!QueryPerformanceFrequency(&freq)) {
    	cout << "QueryPerformanceFrequency FAILED!\n";
    }

    if(!QueryPerformanceCounter(&startPoint)) {
    	cout << "QueryPerformanceCounter FAILED! " << endl;
    }
}

void Telemetry::Reset(bool recursive) {
	if(!disableReset) {
		QueryPerformanceCounter(&startPoint);
	}
	disableReset = recursive;
}

long long Telemetry::Elapsed() const {
	LARGE_INTEGER tempElapsed;
	QueryPerformanceCounter(&tempElapsed);

	return tempElapsed.QuadPart - startPoint.QuadPart;
}

long long Telemetry::RecordTime() {
	LARGE_INTEGER tempElapsed;
	QueryPerformanceCounter(&tempElapsed);
	elapsed = tempElapsed.QuadPart - startPoint.QuadPart;

	return elapsed;
}

void Telemetry::Compare(int num) {
	comparisons += num;
}

void Telemetry::Assign(int num) {
	assignments += num;
}

unsigned long Telemetry::GetCompares() {
	return comparisons;
}

unsigned long Telemetry::GetAssigns() {
	return assignments;
}

double Telemetry::GetSeconds() {
	return double(elapsed)/freq.QuadPart;
}
