/**
 * @file	sorting.cpp
 * @brief	Assignment 16 - Sorting
 * @author	Ethan Slattery
 * @date	02DEC2015
 */
#include "sorting.h"

void FillArrays(int ar1[], int ar2[], int ar3[], const int AR_SIZE) {
    for(int i = 0; i < AR_SIZE; i++) {
        // CALC - load the ordered arrays with numbers
        ar1[i] = (i+1);
        ar3[i] = (i+1);
        ar2[AR_SIZE-(i+1)] = (i+1);
    }
    std::random_shuffle(ar3, ar3 +  AR_SIZE);
}

string PrintAR(int *ar, int numOut, const int AR_SIZE) {
    ostringstream out;
    int colWidth = std::floor( std::log10( AR_SIZE ) ) + 2;

    for(int i = 0; i < numOut; i++) {
        out << setw(colWidth) << ar[i];
    }
    out << setw(colWidth) << "...";
    for(int i = AR_SIZE-numOut; i < AR_SIZE; i++) {
        out << setw(colWidth) << ar[i];
    }
    out << endl;

    return out.str();
}

void BubbleSort(int* from, int* to, Telemetry *SortData) {
    bool noChange; // stop when a pass causes no change
    SortData->Reset();

    for(int* i = to; i > from; i--) {
        noChange = true;
        for (int *j = (from + 1); j <= i; j++) {
            SortData->Compare();
            if (*j < *(j - 1)) {
                SortData->Assign();
                swap(*j, *(j - 1));
                noChange = false;
            } // end if
        } // end for(j)
        if (noChange) {
            break;
        }
    }// end(i)
    SortData->RecordTime();
}

void SelectionSort(int* from, int* to, Telemetry *SortData) {
    int* smallIndex = NULL;
    SortData->Reset();

    for(int* i = from; i <= to; i++) {
        // find smallest datapoint in unsorted data
        smallIndex = i;
        for(int* j = i + 1; j <= to; j++) {
            SortData->Compare();
            if (*j < *smallIndex) {
                smallIndex = j;
            }
        }
        if(smallIndex != i) {
            SortData->Assign();
            swap(*smallIndex, *i);
        }
    }
    SortData->RecordTime();
}

void InsertionSort(int* from, int* to, Telemetry *SortData) {
    int temp;
    SortData->Reset();

    for(int* i = from+1; i <= to; i++) {
        temp = *i;
        int* j = i-1;
        SortData->Compare();
        while(j >= from && *j > temp) {
            SortData->Compare();
            SortData->Assign();
            *(j+1) = *j;
            j--;
        }
        *(j+1) = temp;
    }
    SortData->RecordTime();
}

void QuickSort(int* from, int* to, Telemetry *SortData) {
    SortData->Reset(true);
    int* i = from;
    int* j = to;
    int pivot = *(from+( rand()%int(to-from)+1 ));

    /* partition */
    while (i <= j) {
        while (*i < pivot) {
            SortData->Compare();
            i++;
        }
        while (*j > pivot) {
            SortData->Compare();
            j--;
        }
        if (i <= j) {
            SortData->Assign();
            swap(*i, *j);
            i++;
            j--;
        }
    }

    /* recursion */
    if (from < j) {
        QuickSort(from, j, SortData);
    }
    if (i < to) {
        QuickSort(i, to, SortData);
    }
    SortData->RecordTime();
}

/* l is for left index and r is right index of the sub-array
  of arr to be sorted */
void mergeSort(int arr[], int l, int r, Telemetry *SortData) {
    SortData->Reset(true);
    if (l < r)
    {
        //Same as (l+r)/2, but avoids overflow for large l and h
        int m = l+(r-l)/2;
        mergeSort(arr, l, m, SortData);
        mergeSort(arr, m+1, r, SortData);
        merge(arr, l, m, r, SortData);
    }
    SortData->RecordTime();
}

/* Function to merge the two haves arr[l..m] and arr[m+1..r] of array arr[] */
void merge(int arr[], int l, int m, int r, Telemetry *SortData) {
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;

    /* create temp arrays */
    int L[n1], R[n2];

    /* Copy data to temp arrays L[] and R[] */
    for(i = 0; i < n1; i++) {
        SortData->Assign();
        L[i] = arr[l + i];
    }
    for(j = 0; j < n2; j++) {
        SortData->Assign();
        R[j] = arr[m + 1 + j];
    }

    /* Merge the temp arrays back into arr[l..r]*/
    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2) {
        SortData->Compare();
        if (L[i] <= R[j]) {
            SortData->Assign();
            arr[k] = L[i];
            i++;
        }
        else {
            SortData->Assign();
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    /* Copy the remaining elements of L[], if there are any */
    while (i < n1) {
        SortData->Assign();
        arr[k] = L[i];
        i++;
        k++;
    }

    /* Copy the remaining elements of R[], if there are any */
    while (j < n2) {
        SortData->Assign();
        arr[k] = R[j];
        j++;
        k++;
    }
}