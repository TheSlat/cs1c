/**
 * @file	main.cpp
 * @brief	Assignment 11 - Templates
 *
 * Created a queue class using a template. Used an array to make it more
 * challenging for myself since I have not used an array for an ADT in forever.
 * No exception usage in this assignment, just errors to STDOUT.
 *
 * @author	Ethan Slattery
 * @date	29SEP2015
 */
#include "queue.h"

int main()
{
	ASS11::queue<int>    iQ;
	ASS11::queue<double> dQ;
	ASS11::queue<string> sQ;

	std::cout << "**** ADDING TO THE INTEGER QUEUE 6 TIMES ****\n";
	for(int i = 1; i <= 6; i++)
	{
		iQ.enqueue(i);
		std::cout << iQ.print();
	}

	std::cout << "\n**** REMOVING FROM THE INTEGER QUEUE 4 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
	{
		std::cout << "Removing " << iQ.dequeue() << " from the front\n";
		std::cout << iQ.print();
	}

	std::cout << "\n**** ADDING TO THE INTEGER QUEUE 5 TIMES ****\n";
	for(int i = 7; i <= 11; i++)
	{
		iQ.enqueue(i);
		std::cout << iQ.print();
	}

	std::cout << "\n**** REMOVING FROM THE INTEGER QUEUE 3 TIMES ****\n";
	for(int i = 1; i <= 3; i++)
	{
		std::cout << "Removing " << iQ.dequeue() << " from the front\n";
		std::cout << iQ.print();
	}

	std::cout << "\n**** ADDING TO THE DOUBLE QUEUE 6 TIMES ****\n";
	for(float i = 1.1; i < 1.7; i+=.1)
	{
	dQ.enqueue(i);
	std::cout << dQ.print();
	}

	std::cout << "\n**** REMOVING FROM THE DOUBLE QUEUE 4 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
	{
		std::cout << "Removing " << dQ.dequeue() << " from the front\n";
		std::cout << dQ.print();
	}

	std::cout << "\n**** ADDING TO THE DOUBLE QUEUE 5 TIMES ****\n";
	for(float i = 1.7; i < 2.1; i+=.1)
	{
		dQ.enqueue(i);
		std::cout << dQ.print();
	}

	std::cout << "\n**** REMOVING FROM THE DOUBLE QUEUE 3 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
	{
		std::cout << "Removing " << dQ.dequeue() << " from the front\n";
		std::cout << dQ.print();
	}

	std::cout << "\n**** ADDING TO THE STRING QUEUE 6 TIMES ****\n";
	sQ.enqueue("one");
	std::cout << sQ.print();
	sQ.enqueue("two");
	std::cout << sQ.print();
	sQ.enqueue("three");
	std::cout << sQ.print();
	sQ.enqueue("four");
	std::cout << sQ.print();
	sQ.enqueue("five");
	std::cout << sQ.print();
	sQ.enqueue("six");
	std::cout << sQ.print();

	std::cout << "\n**** REMOVING FROM THE STRING QUEUE 4 TIMES ****\n";
	for(int i = 1; i <= 4; i++)
	{
		std::cout << "Removing " << sQ.dequeue() << " from the front\n";
		std::cout << sQ.print();
	}

	std::cout << "\n**** ADDING TO THE STRING QUEUE 5 TIMES ****\n";
	sQ.enqueue("seven");
	std::cout << sQ.print();
	sQ.enqueue("eight");
	std::cout << sQ.print();
	sQ.enqueue("nine");
	std::cout << sQ.print();
	sQ.enqueue("ten");
	std::cout << sQ.print();
	sQ.enqueue("eleven");
	std::cout << sQ.print();

	std::cout << "\n**** REMOVING FROM THE STRING QUEUE 3 TIMES ****\n";
	for(int i = 1; i <= 3; i++)
	{
		std::cout << "Removing " << sQ.dequeue() << " from the front\n";
		std::cout << sQ.print();
	}

	return 0;
}
