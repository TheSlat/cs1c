/**
 * @file	queue.h
 * @brief	Assignment 11 - Templates
 *
 * @author	Ethan Slattery
 * @date	29SEP2015
 */

#ifndef QUEUE_H_
#define QUEUE_H_
#include<iostream>
#include<iomanip>
#include<string>
#include<sstream>
using std::string;

namespace ASS11
{

	template <class Type>
	class queue
	{
		public:
			/* CONSTRUCTOR & DESTRUCTOR */
			queue(int size = 5);
			~queue();

			/* MUTATORS */
			void enqueue(Type newObj);
			Type dequeue();

			/* ACCESSORS */
			Type   front() const;
			int    size() const;
			bool   isEmpty() const;
			bool   isFull() const;
			string print() const;

		private:
			Type *AR;
			int head;
			int count;
			int max;
	};

	/// The non-default constructor, creates a queue of specified size
	template <class Type>
	queue<Type>::queue(int size)
	{
		max   = size;
		head  = 0;
		count = 0;
		AR = new Type[max];
	}

	/// The Destructor, does nothign in particular
	template <class Type>
	queue<Type>::~queue()
	{}

	/// The enqueue method, adds an item to the queue
	template <class Type>
	void queue<Type>::enqueue(Type newObj)
	{
		if(isFull())
			std::cout << "*** CAN'T ADD " << " THE QUEUE IS FULL ***\n";
		else
			AR[(head + count++) % max] = newObj;
	}

	/// The dequeue method, removes something from the queue and returns that item
	template <class Type>
	Type queue<Type>::dequeue()
	{
		Type temp;
		if(isEmpty())
		{
			std::cout << "\n*** THE QUEUE IS EMPTY ***\n";
		}
		else
		{
			temp = AR[head];
			count--;
			head = (head + 1) % max;
		}
		return temp;
	}

	/// Looks at the item in the front of the queue and returns that item
	template <class Type>
	Type queue<Type>::front() const
	{
		return AR[head];
	}

	/// Returns the size of the queue
	template <class Type>
	int queue<Type>::size() const
	{
		return count;
	}

	/// checks to see if the queue is empty, TRUE if empty
	template <class Type>
	bool queue<Type>::isEmpty() const
	{
		return count == 0;
	}

	/// checks if the queue is empty, TRUE if full
	template <class Type>
	bool queue<Type>::isFull() const
	{
		return (count == max);
	}

	/// Prints the queue so it can be displayed, returns a string
	template <class Type>
	string queue<Type>::print() const
	{
		std::ostringstream out;
		out << std::fixed << std::setprecision(2);
		out << "front --> ";
		for(int i = 0; i < count; i++)
		{
			out << (i > 0 ? " :: ": "");
			out << AR[(head+i)%max];
		}
		out << " <-- tail" << std::endl;
		return out.str();
	}

} // END OF NAMESPACE ASS11
#endif /* QUEUE_H_ */
